import cv2
import mediapipe as mp
import time
import os
import cvzone
from cvzone.FaceMeshModule import FaceMeshDetector





def main():
    capture = cv2.VideoCapture(0)
    
    
    # focal_len = (width_pixels * dist_cm )/width_cm
    # dist_cm = (width_cm * focal_len)
    face_detector = FaceMeshDetector(maxFaces=1)

    while True:
        success, img = capture.read()
        img, faces = face_detector.findFaceMesh(img, draw=False)
        
        if faces:
            face = faces[0]
            left_eye = face[145] #center position of eyes
            right_eye = face[374]
            cv2.circle(img, left_eye, 5, (255, 0, 255), cv2.FILLED)
            cv2.circle(img, right_eye, 5, (255, 0, 255), cv2.FILLED)
            cv2.line(img, left_eye, right_eye, (0,150,0),3)

            # finding the focal length of the camera
            # note- the ,_ just takes the first value
            width_pixels,_ = face_detector.findDistance(left_eye, right_eye)
            width_cm = 6.3 # average space between the eyes in cm
            # dist_cm = 50
            # focal_len = (width_pixels * dist_cm)/width_cm
            # print(focal_len)
            # 740

            # calculating depth
            focal_len = 740 # camera specific, tested using plug in webcam from home. possible to google for precision?
            dist = (width_cm * focal_len )/width_pixels
            print(dist)
        cv2.imshow("Image", img)
        cv2.waitKey(1)


if __name__ == "__main__":
    main()
