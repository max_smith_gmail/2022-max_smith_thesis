// https://chowdera.com/2022/117/202204270022047921.html

#include <iostream>
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h> 
#include <sensor_msgs/image_encodings.h> 
#include <image_transport/image_transport.h> 

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio.hpp>

int main(int argc, char** argv)
{
    
    ros::init(argc, argv, "imageGet_node");   
    ros::NodeHandle nh;                       
    image_transport::ImageTransport it(nh);   
    image_transport::Publisher image_pub = it.advertise("/cameraImage", 1);    

    ros::Rate loop_rate(200);    

    cv::Mat imageRaw;   
    cv::VideoCapture capture(0);    

    if(capture.isOpened() == 0)       
    {
    
        std::cout << "Read camera failed!" << std::endl;
        return -1;
    }

    while(nh.ok())
    {
    
        capture.read(imageRaw);           
        cv::imshow("viewer", imageRaw);   
        sensor_msgs::ImagePtr  msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", imageRaw).toImageMsg();   
        image_pub.publish(msg);          
        ros::spinOnce();                 
        loop_rate.sleep();               
        if(cv::waitKey(2) >= 0)          
            break;        
    }
}
