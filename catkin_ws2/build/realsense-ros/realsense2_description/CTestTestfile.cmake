# CMake generated Testfile for 
# Source directory: /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src/realsense-ros/realsense2_description
# Build directory: /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/realsense-ros/realsense2_description
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_realsense2_description_nosetests_tests "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/test_results/realsense2_description/nosetests-tests.xml" "--return-code" "\"/usr/bin/cmake\" -E make_directory /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/test_results/realsense2_description" "/usr/bin/nosetests-2.7 -P --process-timeout=60 --where=/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src/realsense-ros/realsense2_description/tests --with-xunit --xunit-file=/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/test_results/realsense2_description/nosetests-tests.xml")
