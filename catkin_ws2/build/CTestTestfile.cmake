# CMake generated Testfile for 
# Source directory: /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src
# Build directory: /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("realsense-ros/realsense2_description")
subdirs("camera_input")
subdirs("rplidar_ros")
subdirs("realsense-ros/realsense2_camera")
subdirs("tinkerforge_imu_ros")
subdirs("usb_cam")
subdirs("video_stream_opencv")
