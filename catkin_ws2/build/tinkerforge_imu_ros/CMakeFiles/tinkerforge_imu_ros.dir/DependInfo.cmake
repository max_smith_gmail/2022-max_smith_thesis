# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src/tinkerforge_imu_ros/src/brick_imu_v2.cpp" "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/tinkerforge_imu_ros/CMakeFiles/tinkerforge_imu_ros.dir/src/brick_imu_v2.cpp.o"
  "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src/tinkerforge_imu_ros/src/ip_connection.cpp" "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/tinkerforge_imu_ros/CMakeFiles/tinkerforge_imu_ros.dir/src/ip_connection.cpp.o"
  "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src/tinkerforge_imu_ros/src/tinkerforge_imu_ros.cpp" "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/build/tinkerforge_imu_ros/CMakeFiles/tinkerforge_imu_ros.dir/src/tinkerforge_imu_ros.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"tinkerforge_imu_ros\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/home/max-smith/Documents/2022-max_smith_thesis/catkin_ws2/src/tinkerforge_imu_ros/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
