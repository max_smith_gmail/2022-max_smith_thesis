set(_CATKIN_CURRENT_PACKAGE "tinkerforge_imu_ros")
set(tinkerforge_imu_ros_VERSION "1.0.0")
set(tinkerforge_imu_ros_MAINTAINER "steve macenski <stevenmacenski@gmail.com>")
set(tinkerforge_imu_ros_PACKAGE_FORMAT "3")
set(tinkerforge_imu_ros_BUILD_DEPENDS "roscpp" "sensor_msgs" "tf" "tf2")
set(tinkerforge_imu_ros_BUILD_EXPORT_DEPENDS "roscpp" "sensor_msgs" "tf" "tf2")
set(tinkerforge_imu_ros_BUILDTOOL_DEPENDS "catkin")
set(tinkerforge_imu_ros_BUILDTOOL_EXPORT_DEPENDS )
set(tinkerforge_imu_ros_EXEC_DEPENDS "roscpp" "sensor_msgs" "tf" "tf2")
set(tinkerforge_imu_ros_RUN_DEPENDS "roscpp" "sensor_msgs" "tf" "tf2")
set(tinkerforge_imu_ros_TEST_DEPENDS )
set(tinkerforge_imu_ros_DOC_DEPENDS )
set(tinkerforge_imu_ros_URL_WEBSITE "")
set(tinkerforge_imu_ros_URL_BUGTRACKER "")
set(tinkerforge_imu_ros_URL_REPOSITORY "")
set(tinkerforge_imu_ros_DEPRECATED "")