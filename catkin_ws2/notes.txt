Gaze Tracker Camera Packages
camera_input is the homemade attempt for the gaze tracker camera. https://chowdera.com/2022/117/202204270022047921.html
usb_cam is a package for using usb cameras in ros. Has error for mjpeg formats which can be worked around but dodgy
video_stream_opencv is another package for cameras in ros and this works.
NOTE: there is lots of noise on the image for some reason, may be that the requirements are too harsh currently (high res high freq), https://silkypix.isl.co.jp/en/how-to/useful/digital-camera-noise/
Alternatives: http://wiki.ros.org/cv_camera, https://dabit-industries.github.io/turtlebot2-tutorials/14b-OpenCV2_Python.html, https://msadowski.github.io/ros-web-tutorial-pt2-cameras/, https://github.com/PX4/uvc_ros_driver


Camera Calibration: http://wiki.ros.org/camera_calibration
