#SSH SETUP
ssh-keygen
sir .ssh
eval $ssh-agent
ssh-add ~/.ssh/id_rsa
git config --global user.name "Max-Smith"
git config --global user.email "thetrebuchetman@gmail.com"
ssh -T git@bitbucket.org
yes

# terminator
sudo apt-get install terminator
y

# gparted
sudo apt-get install gparted
y

# mypaint
sudo apt-get install mypaint
y

# basic software for other modules
sudo apt install software-properties-common

# for python 3.8
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.8
y
sudo apt install python3-opencv

# pip3
sudo apt install python3-pip 
y
sudo apt install python-pip
y

# ROS
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'/
sudo apt install curl
y
sudo apt update
sudo apt install python-rosdep
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
y
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt install curl
sudo apt update
sudo apt install ros-melodic-desktop-full
y
sudo rosdep init
rosdep update
sudo apt-get install python3-catkin-tools

# ROS OTHER
sudo snap install gh
sudo apt install v4l-utils
sudo apt-get install ros-melodic-realsense2-camera

#realsense SDK viewer
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE
sudo add-apt-repository "deb https://librealsense.intel.com/Debian/apt-repo $(lsb_release -cs) main" -u
sudo apt-get install librealsense2-dkms
sudo apt-get install librealsense2-utils

#rplidar: while lidar is plugged in. check port, give priveleges
ls -l /dev |grep ttyUSB
sudo chmod 666 /dev/ttyUSB0

# ubuntu cleaner
sudo add-apt-repository ppa:gerardpuig/ppa
sudo apt update
sudo apt install ubuntu-cleaner


#Unity Tweak tool, helpful for settings
sudo apt-get install notify-osd
sudo apt-get install unity-tweak-tool -y

# gnome settings
sudo apt install gnome-session gdm3
sudo tasksel install ubuntu-desktop 			(mistake- do not repeat, no harm though)

# settings manager
sudo apt-get install compizconfig-settings-manager

# for windows-like window management (V V GOOD)
sudo apt install gnome-shell-extensions chrome-gnome-shell
	#add to firefox- WinTile: Windows 10 window tiling for GNOME

# VLC media player
sudo snap install vlc

# screenshot tool
sudo apt update
sudo apt install flameshot
	#add shortcut on keyboard settings, command= /usr/bin/flameshot gui

# atom
sudo apt install software-properties-common apt-transport-https wget
y
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"
sudo apt install atom

# slack
https://slack.com/intl/en-au/downloads/linux download slack .deb file
sudo apt install ./slack-desktop-*.deb

# vs code
sudo apt update
sudo apt install gnupg2 software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
sudo apt update
sudo apt install code

# arandr
sudo apt update
sudo apt install arandr

# audacity (audio editor)
sudo apt update
sudo apt install audacity
y

# autotools (background helper for automake)
sudo apt-get update -y
sudo apt-get install -y autotools-dev

# snap installer
sudo apt update
sudo apt install snapd

# zoom
sudo snap install zoom-client

# IMU Bricklet 2.0 programs (developer, viewer, flasher)
wget https://download.tinkerforge.com/apt/$(lsb_release -is | tr [A-Z] [a-z])/archive.key -q -O - | sudo apt-key add -
sudo sh -c "echo 'deb https://download.tinkerforge.com/apt/$(lsb_release -is | tr [A-Z] [a-z]) $(lsb_release -cs) main' > /etc/apt/sources.list.d/tinkerforge.list"
sudo apt update
sudo apt install brickd
sudo apt install brickv
y
wget --backups=1 https://download.tinkerforge.com/tools/brick_flash/linux/brick-flash_linux_latest.deb
sudo dpkg -i brick-flash_linux_latest.deb
wget --backups=1 https://download.tinkerforge.com/firmwares/bricks/master/brick_master_firmware_latest.bin
#TO FLASH: brick-flash -p /dev/ttyACM0 -f brick_master_firmware_latest.bin

# qalculate- good calculator
sudo apt update
sudo apt install qalculate
y 

# spotify
sudo snap install spotify 

# bluetooth
sudo apt update
sudo apt install bluetooth
sudo apt install bluez* ofono ofono-phonesim
sudo  modprobe btusb
sudo systemctl restart bluetooth
sudo nano /etc/bluetooth/main.conf
	# enable ControllerMode = bredr
sudo /etc/init.d/bluetooth restart

# GIMP photo editor
sudo snap install gimp

# Blender 3D modeller
sudo snap install blender --classic

# download matlab from https://au.mathworks.com/downloads, tut: https://au.mathworks.com/matlabcentral/answers/1619455-matlab-unable-to-install-r2021b-unable-to-write-to-selected-folder-in-ubuntu-20-04
cd ~/Downloads/matlab_R2022a_glnxa64
xhost +SI:localuser:root
sudo ./install

# Mediapipe
sudo apt-get install nodejs-dev node-gyp libssl1.0-dev
sudo apt-get install npm
sudo npm install -g @bazel/bazelisk
cd ~/
git clone https://github.com/google/mediapipe.git
cd mediapipe
sudo apt-get install -y \
    libopencv-core-dev \
    libopencv-highgui-dev \
    libopencv-calib3d-dev \
    libopencv-features2d-dev \
    libopencv-imgproc-dev \
    libopencv-video-dev
sudo bash setup_opencv.sh
y
sudo apt-get install mesa-common-dev libegl1-mesa-dev libgles2-mesa-dev
pip3 install mediapipe
pip3 install protobuf_to_dict
pip install protobuf3-to-dict==0.1.5

# mediapipe iris model

# Dlib
pip3 install dlib



# etcher
curl -1sLf \
      'https://dl.cloudsmith.io/public/balena/etcher/setup.deb.sh' \
      | sudo -E bash
sudo apt install -y balena-etcher-electron

# Jetpack (needed for AI packages and also for Jetson Xavier setup
sudo apt-key adv --fetch-key http://repo.download.nvidia.com/jetson/jetson-ota-public.asc
sudo nano /etc/apt/sources.list
	# add this:
	# deb http://repo.download.nvidia.com/jetson/x86_64/bionic r34.1 main
sudo apt update
sudo apt-get install cuda-toolkit-11-4 cuda-cross-aarch64-11-4 libnvvpi2 vpi2-dev vpi2-cross-aarch64-l4t python3-vpi2 python3.9-vpi2 vpi2-samples nsight-systems-2021.5.4 nsight-graphics-for-embeddedlinux-2021.5.1

#QT cam for See3Cam camera module
sudo apt-add-repository ppa:qtcam/bionic
sudo apt-get update
sudo apt-get install qtcam

# PuTTY
sudo apt-get install -y putty
sudo apt-get install ssh

# Networking stuff
sudo apt install net-tools 
sudo apt install nmap

# for drivers
sudo ubuntu-drivers autoinstall


# view cam resolution 
sudo apt install uvcdynctrl

# yarn
sudo apt install cmdtest

#python 3.10 (MISTAKE!)
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
apt list | grep python3.10
sudo apt-get install python3.10
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.8 1
sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 2
sudo update-alternatives --config python3
	#pick number for python3.10, python3 -V for testing
sudo apt-get install --reinstall python3-apt

#python 3.9
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt install python3.9

#Python packages
pip3 install pandas
pip3 install scikit-image
pip3 install pyautogui
sudo apt-get install python3-tk python3-dev
python3.8 -m pip install pgi
sudo python3.8 -m pip install --ignore-installed PyGObject
pip install PyQt5
sudo apt-get install '^libxcb.*-dev' libx11-xcb-dev libglu1-mesa-dev libxrender-dev libxi-dev libxkbcommon-dev libxkbcommon-x11-dev

# for changing cursor
sudo apt install dconf-editor

sudo apt autoremove


#While USB devices are plugged in (e.g. ultrasonic sensor). go to /dev/
sudo adduser max-smith dialout
sudo chown max-smith ttyACM0 
ls -l tty* 
whoami
groups

# FROM UBUNTU SOFTWARE STORE:
# GStreamer Multimedia Codecs @ http://www.libav.org/ http://gstreamer.freedesktop.org/ http://gstreamer.freedesktop.org/modules/gst-libav.html


