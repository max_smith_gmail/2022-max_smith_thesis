# program to calculate required focal length of my camera based on required frame size at specific distances
# all distnace units are in meters, all angles are in degrees
import math

# dimensions of my own face
face_width = 0.17
face_height = 0.25

# Image sensor dimensions
# NOTE: the WIDEST (portrait=height, landscape=width) part of the image is produced using the SMALLEST (4.415) side of the image sensor.
# using the camera in portrait simply rotates the camera module, it does not swap this.
img_sens_width = 6.058e-3
img_sens_height = 4.415e-3

# range of distance from camera to face [0.5, 0.75]m
d1 = 0.5
d2 = 0.75

# minimum acceptable frame sizes in portrait mode at d1. 
# The face will take up the most room and the frame will be smallest at d1
# add 5cm margins to the face
w1_min = face_width + 2 * 0.05
h1_min = face_height + 2 * 0.05

# theta is accociated with facial width, alpha with height
theta_perf = 2 * math.atan(w1_min/(2*d1))
alpha_perf = 2 * math.atan(h1_min/(2*d1))

# calculate the focal length of these theoretically "perfect" view angles in each dimension
focal_len_ideal_width = img_sens_width/(2*math.tan(theta_perf/2))
focal_len_ideal_height = img_sens_height/(2*math.tan(alpha_perf/2))

# calculate the frame size at d1 and d2 for each of the "perfect" focal lengths (height-based or width-based)
# width focused
theta_width_focused = 2 * math.atan(img_sens_width/(2*focal_len_ideal_width))
w1_width_focused = 2 * d1 * math.tan(theta_width_focused/2)
w2_width_focused = 2 * d2 * math.tan(theta_width_focused/2)

alpha_width_focused = 2 * math.atan(img_sens_height/(2*focal_len_ideal_width))
h1_width_focused = 2 * d1 * math.tan(alpha_width_focused/2)
h2_width_focused = 2 * d2 * math.tan(alpha_width_focused/2)

print("\nFor an ideal frame width of {:.2f}m at a distance={:.2f}m, focal length should be {:.2f}mm.".format(w1_min, d1, focal_len_ideal_width*1000))
print("width viewing angle={:.2f} degrees, height viewing angle {:.2f} degrees.".format(math.degrees(theta_width_focused), math.degrees(alpha_width_focused)))
print("At distance = {:.2f}m, frame = {:.2f}m wide * {:.2f}m tall.".format(d1, w1_width_focused, h1_width_focused))
print("At distance = {:.2f}m, frame = {:.2f}m wide * {:.2f}m tall.\n".format(d2, w2_width_focused, h2_width_focused))

# height focused
theta_height_focused = 2 * math.atan(img_sens_width/(2*focal_len_ideal_height))
w1_height_focused = 2 * d1 * math.tan(theta_height_focused/2)
w2_height_focused = 2 * d2 * math.tan(theta_height_focused/2)

alpha_height_focused = 2 * math.atan(img_sens_height/(2*focal_len_ideal_height))
h1_height_focused = 2 * d1 * math.tan(alpha_height_focused/2)
h2_height_focused = 2 * d2 * math.tan(alpha_height_focused/2)

print("For an ideal frame height of {:.2f}m at a distance={:.2f}m, focal length should be {:.2f}mm.".format(h1_min, d1, focal_len_ideal_height*1000))
print("width viewing angle={:.2f} degrees, height viewing angle {:.2f} degrees.".format(math.degrees(theta_height_focused), math.degrees(alpha_height_focused)))
print("At distance = {:.2f}m, frame = {:.2f}m wide * {:.2f}m tall.".format(d1, w1_height_focused, h1_height_focused))
print("At distance = {:.2f}m, frame = {:.2f}m wide * {:.2f}m tall.".format(d2, w2_height_focused, h2_height_focused))

