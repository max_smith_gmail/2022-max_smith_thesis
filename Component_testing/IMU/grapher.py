# import pandas module
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
 
# creating a data frame
data = pd.read_csv("IMU_test_lunch.csv", delimiter=';')

# remove redundant cols
data.drop(['NAME','UID'], axis=1, inplace=True)

# make values numeric not strings
data['RAW'] = data['RAW'].astype(int)

# remove date from time
data.TIME = data.TIME.str.split(expand=True,)[1]

# make time into timedelta (diff in time type) and make it relative to the begining of the log
data['TIME'] = pd.to_timedelta(data['TIME'])
data['TIME'] = (data['TIME'] - data['TIME'][1]).astype('timedelta64[s]')

# Gather acceleration values, then dataframe with values
acc_X_vals = data.loc[(data.VAR == "Acceleration-X"), ['TIME', 'RAW']].rename(columns={"TIME":"Time","RAW":"Acc_X"}).reset_index(drop=True)
acc_Y_vals = data.loc[(data.VAR == "Acceleration-Y"), ['RAW']].rename(columns={"RAW":"Acc_Y"}).reset_index(drop=True)
acc_Z_vals = data.loc[(data.VAR == "Acceleration-Z"), ['RAW']].rename(columns={"RAW":"Acc_Z"}).reset_index(drop=True)
acceleration_data = pd.DataFrame()
acceleration_data = pd.concat([acc_X_vals,acc_Y_vals,acc_Z_vals], axis=1)

# Gather acceleration values, then dataframe with values
linear_acc_X_vals = data.loc[(data.VAR == "Linear Acceleration-X"), ['TIME', 'RAW']].rename(columns={"TIME":"Time","RAW":"Linear_acc_X"}).reset_index(drop=True)
linear_acc_Y_vals = data.loc[(data.VAR == "Linear Acceleration-Y"), ['RAW']].rename(columns={"RAW":"Linear_acc_Y"}).reset_index(drop=True)
linear_acc_Z_vals = data.loc[(data.VAR == "Linear Acceleration-Z"), ['RAW']].rename(columns={"RAW":"Linear_acc_Z"}).reset_index(drop=True)
linear_acceleration_data = pd.DataFrame()
linear_acceleration_data = pd.concat([linear_acc_X_vals,linear_acc_Y_vals,linear_acc_Z_vals], axis=1)

# Gather Orientation values, then dataframe with values
Orient_Heading_vals = data.loc[(data.VAR == "Orientation-Heading"), ['TIME', 'RAW']].rename(columns={"TIME":"Time","RAW":"Orient_Heading"}).reset_index(drop=True)
Orient_Roll_vals = data.loc[(data.VAR == "Orientation-Roll"), ['RAW']].rename(columns={"RAW":"Orient_Roll"}).reset_index(drop=True)
Orient_Pitch_vals = data.loc[(data.VAR == "Orientation-Pitch"), ['RAW']].rename(columns={"RAW":"Orient_Pitch"}).reset_index(drop=True)
Orientation_data = pd.DataFrame()
Orientation_data = pd.concat([Orient_Heading_vals, Orient_Roll_vals, Orient_Pitch_vals], axis=1)

# Gather Gravity Vector values, then dataframe with values
Grav_vector_X_vals = data.loc[(data.VAR == "Gravity Vector-X"), ['TIME', 'RAW']].rename(columns={"TIME":"Time","RAW":"Grav_vector_X"}).reset_index(drop=True)
Grav_vector_Y_vals = data.loc[(data.VAR == "Gravity Vector-Y"), ['RAW']].rename(columns={"RAW":"Grav_vector_Y"}).reset_index(drop=True)
Grav_vector_Z_vals = data.loc[(data.VAR == "Gravity Vector-Z"), ['RAW']].rename(columns={"RAW":"Grav_vector_Z"}).reset_index(drop=True)
Grav_vector_data = pd.DataFrame()
Grav_vector_data = pd.concat([Grav_vector_X_vals, Grav_vector_Y_vals, Grav_vector_Z_vals], axis=1)

# Gather magnetic field values, then dataframe with values
Mag_field_X_vals = data.loc[(data.VAR == "Magnetic Field-X"), ['TIME', 'RAW']].rename(columns={"TIME":"Time","RAW":"Mag_field_X"}).reset_index(drop=True)
Mag_field_Y_vals = data.loc[(data.VAR == "Magnetic Field-Y"), ['RAW']].rename(columns={"RAW":"Mag_field_Y"}).reset_index(drop=True)
Mag_field_Z_vals = data.loc[(data.VAR == "Magnetic Field-Z"), ['RAW']].rename(columns={"RAW":"Mag_field_Z"}).reset_index(drop=True)
Mag_field_data = pd.DataFrame()
Mag_field_data = pd.concat([Mag_field_X_vals, Mag_field_Y_vals, Mag_field_Z_vals], axis=1)

# Gather magnetic field values, then dataframe with values
Ang_vel_X_vals = data.loc[(data.VAR == "Angular Velocity-X"), ['TIME', 'RAW']].rename(columns={"TIME":"Time","RAW":"Ang_vel_X"}).reset_index(drop=True)
Ang_vel_Y_vals = data.loc[(data.VAR == "Angular Velocity-Y"), ['RAW']].rename(columns={"RAW":"Ang_vel_Y"}).reset_index(drop=True)
Ang_vel_Z_vals = data.loc[(data.VAR == "Angular Velocity-Z"), ['RAW']].rename(columns={"RAW":"Ang_vel_Z"}).reset_index(drop=True)
Ang_vel_data = pd.DataFrame()
Ang_vel_data = pd.concat([Ang_vel_X_vals, Ang_vel_Y_vals, Ang_vel_Z_vals], axis=1)

# Plot data on subfigures
fig, axes = plt.subplots(nrows=2, ncols=3)

acceleration_data.plot(ax=axes[0,0], x="Time", y=["Acc_X", "Acc_Y", "Acc_Z",], title="Acceleration Over Time",ylabel="Acceleration (0.01m/s^2)", xlabel="Time (s)")
Grav_vector_data.plot(ax=axes[0,1], x="Time", y=["Grav_vector_X", "Grav_vector_Y", "Grav_vector_Z",], title="Gravity Vector Over Time", ylabel="Acceleration (0.01m/s^2)", xlabel="Time (s)")
linear_acceleration_data.plot(ax=axes[0,2], x="Time", y=["Linear_acc_X", "Linear_acc_Y", "Linear_acc_Z",], title="Linear Acceleration Over Time", ylabel="Acceleration (0.01m/s^2)", xlabel="Time (s)")

Ang_vel_data.plot(ax=axes[1,0], x="Time", y=["Ang_vel_X", "Ang_vel_Y", "Ang_vel_Z",], title="Angular Velocity Over Time", ylabel="Angular Velocity (1/16 degrees/s)", xlabel="Time (s)")
Orientation_data.plot(ax=axes[1,1], x="Time", y=["Orient_Heading", "Orient_Roll", "Orient_Pitch",], title="Orientation Over Time", ylabel="Orientation (1/16 degrees)", xlabel="Time (s)")
Mag_field_data.plot(ax=axes[1,2], x="Time", y=["Mag_field_X", "Mag_field_Y", "Mag_field_X",], title="Magnetic Field Over Time", ylabel="Tesla 1/16 µT", xlabel="Time (s)")

plt.show()