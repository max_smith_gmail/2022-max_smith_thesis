import json, cv2, numpy as np, sys
import accessCameras

def main():
    # get possible options for video input
    # video_options = accessCameras.gather_JSON_data("cameraOptions.json")
    video_options = accessCameras.gather_JSON_data(sys.argv[1])

    # prompt user to select video input source/option
    selected_video = accessCameras.user_camera_selection(video_options)

    # open video stream and select chosen parameters
    cap = accessCameras.open_video(selected_video)

    # gather relevant settings from json file
    f = open("settings.json")
    settings = json.load(f)
    live_display_settings = settings["live_display"]


    # https://docs.opencv.org/4.x/dc/dbb/tutorial_py_calibration.html
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((6*7,3), np.float32)
    objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.


    # continue until calibration finishes in other thread or if not calibrating then go until time in settings is done
    while 1:

        # Capture frame
        success, frame = cap.read()

        # Stop if reading failed
        if success is False:
            raise Exception("Could not read image capture")
        if np.shape(frame) == [0,0,0]:
            raise Exception("Could not read image capture")

        # adjust colour and orientation of image
        frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)


        greyscale_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(greyscale_frame, (8,7), None)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv2.cornerSubPix(greyscale_frame,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners)
            # Draw and display the corners
            cv2.drawChessboardCorners(frame, (8,7), corners2, ret)
            # cv2.imshow('img', img)
            # cv2.waitKey(500)









        # resize the frame for viewing
        scale_percent = live_display_settings["live_display_shrink_percentage"]
        width = int(frame.shape[1] * scale_percent / 100)
        height = int(frame.shape[0] * scale_percent / 100)
        dim = (width, height)
        resized_img = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
        
        cv2.imshow('Resized Video Recording Preview', resized_img)


        # Waits for a user input to quit the application
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

main()