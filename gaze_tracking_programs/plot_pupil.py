import csv
from turtle import color
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# homography transform tutorial, for scaling eye images. (to be implemented in mediapipe module )
# https://towardsdatascience.com/image-processing-with-python-applying-homography-for-image-warping-84cd87d2108f


# heading names
'''
'Time', 'L_box_center', 'L_pupil_offset_xy', 'L_pupil_angle_deg', 'L_pupil_dist', 
        'R_box_center', 'R_pupil_offset_xy', 'R_pupil_angle_deg', 'R_pupil_dist'
'''

# read the csv input
# TODO refractor and make function for main program to use, 
# will give the data folder to this program to locate csv
df = pd.read_csv('eye_pos.csv')

# split the offset data to access x and y
df[['left_x_offset', 'left_y_offset']] = df['L_pupil_offset_xy'].str.split(',', expand=True)
df[['right_x_offset', 'right_y_offset']] = df['L_pupil_offset_xy'].str.split(',', expand=True)

# convert data to correct type format
df['Time'].index = pd.to_timedelta(df['Time'].index, unit='seconds')

# cartesian data
df['left_x_offset'] = pd.to_numeric(df['left_x_offset'])
df['left_y_offset'] = pd.to_numeric(df['left_y_offset'])
df['right_x_offset'] = pd.to_numeric(df['right_x_offset'])
df['right_y_offset'] = pd.to_numeric(df['right_y_offset'])

# Polar coordinate data
df['L_pupil_angle_deg'] = pd.to_numeric(df['L_pupil_angle_deg'])
df['L_pupil_dist'] = pd.to_numeric(df['L_pupil_dist'])
df['R_pupil_angle_deg'] = pd.to_numeric(df['R_pupil_angle_deg'])
df['R_pupil_dist'] = pd.to_numeric(df['R_pupil_dist'])


# # Plot cartesian
# fig, axs = plt.subplots(2, sharex=True, sharey=True)

# axs[0].plot(df['Time'], df['left_x_offset'], color='red')
# axs[0].plot(df['Time'], df['left_y_offset'], color='green')
# axs[0].set(title='Left', xlabel='Time(s)', ylabel='Offset (pixels)')

# axs[1].plot(df['Time'], df['right_x_offset'], color='red')
# axs[1].plot(df['Time'], df['right_y_offset'], color='green')
# axs[1].set(title='Right', xlabel='Time(s)', ylabel='Offset (pixels)')

# fig.suptitle('Pupil offsets over time in pixels')
# fig.legend(['x_offset', 'y_offset'])
# for ax in axs.flat:
#     ax.label_outer()

# plt.show()

# Plot polar
fig, axs = plt.subplots(2, sharex=True)
axs2 = (axs[0].twinx(), axs[1].twinx())

# degrees y axis
axs[0].plot(df['Time'], df['L_pupil_angle_deg'], color='blue')
axs[0].set_ylabel('Pupil vector angle (degrees)', color='blue')
axs[0].set_ylim([-180,180])

# distance y axis
axs2[0].plot(df['Time'], df['L_pupil_dist'], color='#f527c8')
axs2[0].set_ylabel('Pupil displacement (pixels)', color='#f527c8')

axs[0].set(title='Left')
axs[0].set_xlabel('Time(s)')


# degrees y axis
axs[1].plot(df['Time'], df['R_pupil_angle_deg'], color='blue')
axs[1].set_ylabel('Pupil vector angle (degrees)', color='blue')
axs[1].set_ylim([-180,180])

# distance y axis
axs2[1].plot(df['Time'], df['R_pupil_dist'], color='#f527c8')
axs2[1].set_ylabel('Pupil displacement (pixels)', color='#f527c8')

axs[1].set(title='Right')
axs[1].set_xlabel('Time(s)')


fig.suptitle('Pupil offsets over time in pixels')
# fig.legend(['Theta (degrees)', 'displacement (pixels)'])
for ax in axs.flat:
    ax.label_outer()

plt.show()

