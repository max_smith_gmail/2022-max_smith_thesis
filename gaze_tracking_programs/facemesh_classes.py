
from logging import raiseExceptions
import math
from math import hypot
from cmath import pi
import numpy as np
import cv2
from skimage import transform


# CONVENTIONS
# all sets of four coordinates are in the order left, right, top, bottom
# all pixel coordinates are stored as integers

# ROTATION
# x looking up or down
# y looking left or right
# z camera orientation/turn, are they upsidedown or sideways?


class glint():

    def __init__(self, rel_x, rel_y, area):
        self.area = area
        self.rel_x = rel_x
        self.rel_y = rel_y
        self.abs_x = None
        self.abs_y = None

    def calc_abs_xy_str(self, img_offset_x, img_offset_y, dp=2):
        self.abs_x = img_offset_x + self.rel_x
        self.abs_y = img_offset_y + self.rel_y
        return self.get_abs_xy_str(dp=dp)

    def get_rel_xy_str(self, dp=2):
        return "{:.{dp}f},{:.{dp}f}".format(self.rel_x, self.rel_y, dp=dp)

    def get_abs_xy_str(self, dp=2):
        if self.abs_x == None or self.abs_y == None:
            raise ValueError("Glint absolute coordinates not yet calculated: cannot provide string form")
        return "{:.{dp}f}, {:.{dp}f}".format(self.abs_x, self.abs_y, dp=dp)

class glint_pair():
    def __init__(self, blob_1, blob_2):
        glints = [glint(blob_1[0], blob_1[1], blob_1[2]), 
                  glint(blob_2[0], blob_2[1], blob_2[2])]

        # check left and right of glints in this eye(from the user's perspective)
        if glints[0].rel_x > glints[0].rel_x:
            self.left = glints[0]
            self.right = glints[1]
        else:
            self.left = glints[1]
            self.right = glints[0]

        self.ave_rel_x = (self.left.rel_x + self.right.rel_x)/2
        self.ave_rel_y = (self.left.rel_y + self.right.rel_y)/2

        self.ave_abs_x = None
        self.ave_abs_y = None

    # add coordinates relative to the main image (instead of relative to the eye sub-images)
    def add_absolute_coords(self, img_offset_x, img_offset_y):

        self.left.calc_abs_xy_str(img_offset_x, img_offset_y)
        self.right.calc_abs_xy_str(img_offset_x, img_offset_y)

        self.ave_abs_x = (self.left.abs_x + self.right.abs_x)/2
        self.ave_abs_y = (self.left.abs_y + self.right.abs_y)/2

        return self.get_ave_abs_xy()

    def get_ave_abs_xy(self):
        if self.left.abs_x == None:
            raise ValueError("Absolute glint coords not yet added, cannot return values.")
        return (self.ave_abs_x,self.ave_abs_y)

    def get_ave_rel_xy(self):
        return (self.ave_rel_x,self.ave_rel_y)

    def get_csv_str(self, dp=2):
        ret_str = [ 'L:{}'.format(self.left.get_abs_xy_str(dp=dp)),
                    'R:{}'.format(self.right.get_abs_xy_str(dp=dp))]
        return ret_str

    def get_dist_between_glints(self):
        return hypot(abs(self.left.x - self.right.x), abs(self.left.y - self.right.y))

# orientation storage in 3 dim, radians
class orientation():
    def __init__(self, x, y, z):
        self.x = x # looking up or down
        self.y = y # looking left or right
        self.z = z # camera orientation/turn, are they upsidedown or sideways?

    def get_str(self, dp=2):
        
        return "[{:.{dp}f}, {:.{dp}f}, {:.{dp}f}]".format(self.x, self.y, self.z ,dp=dp)

# basic class to store pixel coordinates (estimated z is given by mediapipe)
class pixel_coords():
    def __init__(self, *args):
        self.x = int(args[0])
        self.y = int(args[1])
        self.all = [self.x, self.y]

        if len(args) == 3: 
            self.z = int(args[2])
            self.all = [self.x, self.y, self.z]

    # def get_xy_str(self):
    #     return str(self.x) + "," + str(self.y)

    # get the x and y distance to the given point. Normal coords apply (larger y is higher)
    def get_xy_dist_to_coord(self,end_coord):
        return pixel_coords((end_coord.x - self.x), (self.y - end_coord.y))
    
    # get the diagonal distance to a given point
    def get_diag_dist_to_coord(self,end_coord):
        return math.sqrt((end_coord.x - self.x)**2 + (self.y - end_coord.y)**2)

    # create the end point of a vector starting at this pixel given an angle and length
    def calc_vector_end_coords(self, len, angle_rad, degrees=False):

        if degrees == True:
            angle_rad = angle_rad * pi/180

        return pixel_coords(
                self.x + len * math.cos(angle_rad), 
                self.y + len * math.sin(angle_rad)
                )

    # return the angle from the current point to the given point
    def get_angle_to_pixel_coord(self, end_pixel_coords, degrees=False):

        rise = self.y - end_pixel_coords.y
        run = end_pixel_coords.x - self.x        

        # account for division by zero errors and edge cases
        if run >= 0 and rise == 0: # if dead center or pointing right
            angle = 0
        elif run == 0 and rise > 0: # if line it vertically up
            angle = pi/2
        elif run == 0 and rise < 0: # if line is vertically down
            angle = -pi/2
        elif run < 0 and rise == 0: # line is left
            angle = pi
        else: # normal maths, but account for > 90 degor < 90 deg
            if run < 0 and rise > 0: # top left quadrant of unit circle
                angle = pi + math.atan(rise/run)
            elif run < 0 and rise < 0: # bottom left quadrant of unit circle
                angle = math.atan(rise/run) - pi
            else:
                angle = math.atan(rise/run)

        if degrees == True:
            angle =  angle * 180/pi

        return angle

# class to take 4 landmark coordinate and store their pixel locations
class landmark_set():
    def __init__(self, left, right, top, bottom, landmarks, im_width, im_height):
        self.landmarks = landmarks

        # TODO check for invalid args
        self.left_id = left
        self.right_id = right
        self.top_id = top
        self.bottom_id = bottom

        self.im_width = im_width
        self.im_height = im_height

        self.left_pixel_coords = pixel_coords(
            landmarks[left]['x'] * self.im_width, 
            landmarks[left]['y'] * self.im_height, 
            landmarks[left]['z'] * self.im_width)
        self.right_pixel_coords = pixel_coords(
            landmarks[right]['x'] * self.im_width, 
            landmarks[right]['y'] * self.im_height, 
            landmarks[right]['z'] * self.im_width)
        self.top_pixel_coords = pixel_coords(
            landmarks[top]['x'] * self.im_width, 
            landmarks[top]['y'] * self.im_height, 
            landmarks[top]['z'] * self.im_width)
        self.bottom_pixel_coords = pixel_coords(
            landmarks[bottom]['x'] * self.im_width, 
            landmarks[bottom]['y'] * self.im_height, 
            landmarks[bottom]['z'] * self.im_width)

        self.width = abs(self.left_pixel_coords.x - self.right_pixel_coords.x)
        self.height = abs(self.top_pixel_coords.y - self.bottom_pixel_coords.y)

    def get_pixel_coords(self):
        return [self.left_pixel_coords, self.right_pixel_coords, self.top_pixel_coords, self.bottom_pixel_coords]

    def get_string_display(self):
        return "L {}.({},{})\t R {}.({},{})\t T {}.({},{})\t B {}.({},{})".format(
            self.left_id, self.left_pixel_coords.x, self.left_pixel_coords.y,
            self.right_id, self.right_pixel_coords.x, self.right_pixel_coords.y,
            self.top_id, self.top_pixel_coords.x, self.top_pixel_coords.y,
            self.bottom_id, self.bottom_pixel_coords.x, self.bottom_pixel_coords.y,
        )

    def get_landmarks(self):
        return [self.left_id, self.right_id, self.top_id, self.bottom_id]
    
    def replace_landmark(self, label, new_landmark_id, new_landmark_data):
        # check landmark types to make sure the ID and the landmark are not swapped
        if (type(new_landmark_id) != int):
            raiseExceptions("Error replacing landmark: landmark ID must be an integer")
        if (type(new_landmark_data) != dict):
            raiseExceptions("Error replacing landmark: landmark data must be a dict")

        new_pixel_coords = pixel_coords(
            new_landmark_data['x'] * self.im_width,
            new_landmark_data['y'] * self.im_height,
            new_landmark_data['z'] * self.im_width)

        if 'l' in label.lower():
            self.left_id = new_landmark_id
            self.left_pixel_coords = new_pixel_coords
        
        if 'r' in label.lower():
            self.right_id = new_landmark_id
            self.right_pixel_coords = new_pixel_coords
        
        if 't' in label.lower():
            self.top_id = new_landmark_id
            self.top_pixel_coords = new_pixel_coords
        
        if 'b' in label.lower():
            self.bottom_id = new_landmark_id
            self.bottom_pixel_coords = new_pixel_coords
        
# class to create an angled box given four pixel coordinates (ont on each edge) and an angle which defaults to zero.
# If given a ratio then use the width as fixed and calculate the height
class angled_box():
    def __init__(self, left, right, top, bottom, angle_rad=0, height2width_ratio=None, pixel_of_interest=None):

        # sort through inputs, if they are integers then the user has supplied bounds instead of pixel coordinates
        input_coords = [left, right, top, bottom]
        if all(isinstance(input, int) for input in input_coords):
            self.centre = pixel_coords(
                int((left + right)/2),
                int((top + bottom)/2),
                )
            left = pixel_coords(left, self.centre.y)
            right = pixel_coords(right, self.centre.y)
            top = pixel_coords(self.centre.x, top)
            bottom = pixel_coords(self.centre.x, bottom)

        elif all(isinstance(input, pixel_coords) for input in input_coords):
            self.centre = pixel_coords(
                (left.x + right.x)/2,
                (top.y + bottom.y)/2
                )
        else:
            raiseExceptions("angled_box constructor parameter error: inputs must be ints or pixel_coords")
        
        # determine the dimensions and angle of the new box
        self.angle_rad = angle_rad
        self.width_px = int(abs(left.x - right.x))
        if height2width_ratio is None:
            self.height_px = int(abs(top.y - bottom.y))
        else:
            self.height_px = int(self.width_px * height2width_ratio)

        # the initial points/corners of the non-tilted rectangle
        self.corners = np.array([
            [self.centre.x - self.width_px/2, self.centre.y + self.height_px/2], # bottom left
            [self.centre.x + self.width_px/2, self.centre.y + self.height_px/2], # bottom right
            [self.centre.x + self.width_px/2, self.centre.y - self.height_px/2], # top right
            [self.centre.x - self.width_px/2, self.centre.y - self.height_px/2], # top left
            ]).astype(int)

        # rotate the rectangle about its center if an angle was specified
        if self.angle_rad is not None:
            angled_corners = np.array(
                [
                    [
                        self.centre.x + math.cos(self.angle_rad) * (px - self.centre.x) + math.sin(self.angle_rad) * (py - self.centre.y),
                        self.centre.y - math.sin(self.angle_rad) * (px - self.centre.x) + math.cos(self.angle_rad) * (py - self.centre.y),
                    ]
                    for px, py in self.corners
                ]
            ).astype(int)
            self.corners = angled_corners

        if pixel_of_interest is not None and type(pixel_of_interest) == pixel_coords:
            

            # make offset a vector from the center of the box so that we can rotate it to the new axis (when tilted)
            self.dist2POI = self.centre.get_diag_dist_to_coord(pixel_of_interest)
            self.absolute_angle2POI = self.centre.get_angle_to_pixel_coord(pixel_of_interest) # relative to image x,y axis (absolute)

            # this angle is relative to the angle box (not absolute image coords)
            self.relative_angle2POI = self.absolute_angle2POI - self.angle_rad

            self.absolute_POI_coords = pixel_of_interest
            self.relative_POI_coords = pixel_coords(self.dist2POI * math.sin(self.relative_angle2POI), self.dist2POI * math.cos(self.relative_angle2POI))

            # TODO: check what was being worked on here
            # print("dist2POI = {:.2f}, angle2POI = {:.2f}".format(self.dist2POI, self.angle2POI))
            # print("Tilt = {:.2f} radians = {:.2f} degrees".format(self.angle_rad, self.angle_rad * 180/pi))
            # print("relative angle = {:.2f} (rad) = {:.2f} degrees\n".format(self.relative_angle2POI, self.relative_angle2POI * 180/pi))
           
        else:
            self.POI_offset = None


        # low pixel values are at the top left of the image, so large values are on the bottom right of the screen
        self.img_rightmost_val = max(self.corners[:, 0])
        self.img_leftmost_val = min(self.corners[:, 0])       
        self.img_lowest_val = max(self.corners[:, 1])
        self.img_highest_val = min(self.corners[:, 1])
        self.origin = pixel_coords(list(self.corners[3])[0], list(self.corners[3])[1])

        self.rect = cv2.minAreaRect(self.corners)
        self.box_points = np.int0(cv2.boxPoints(self.rect))

    def get_homography_matrix(self, new_width=None, new_height=None):

        # get dimensions if not given
        if new_width is None:
            new_width = self.width_px
        if new_height is None:
            new_height = self.height_px

        # gather source and destination coordinates, top left = (0,0)
        start_points = self.corners.reshape((4, 2))
        projection_points = np.array([
            [0, new_height], # left bottom
            [new_width, new_height], # right bottom
            [new_width, 0], # right top
            [0, 0] # left top
            ]).astype(int).reshape((4, 2))

        # calcuate transform 
        trans = transform.estimate_transform('projective', start_points, projection_points)
        # theory is projective transform given here: https://au.mathworks.com/help/images/matrix-representation-of-geometric-transformations.html
        '''
        T = [[1 0 E],
             [0 1 F],
             [0 0 1]]
        E and F influence tha vanishing point. Large values -> parallel lines converge quickly (more warping)

        image = 1920 * 1080 * 3, T = 3 * 3
        new_image = imwarp(image, T)

        single pixel = 1 * 1 * 3, T = 3*3
        updated_pixel_coords = 
        '''
        return trans

# class to store all of the information related to a single eye
class eye():
    def __init__(self, eye_bounds, eye_iris, pupil, eye_corners, tilt_rad = 0):

        self.bounds = eye_bounds
        self.iris = eye_iris
        self.pupil = pupil #this is a single pixel coordinate, not a landmark set
        self.centre = pixel_coords(
            (eye_bounds.left_pixel_coords.x + eye_bounds.right_pixel_coords.x)/2,
            (eye_bounds.top_pixel_coords.y + eye_bounds.bottom_pixel_coords.y)/2
            )
        self.tilt_rad = tilt_rad

        self.eye_corner_left = eye_corners[0]
        self.eye_corner_right = eye_corners[1]
        self.eye_corner_dist = hypot(abs(self.eye_corner_left.x + self.eye_corner_right.x), abs(self.eye_corner_left.y + self.eye_corner_right.y))
        self.image_box = None
        self.glint_pair = None
        self.glint_pair_ave_abs_x = None
        self.glint_pair_ave_abs_y = None
        self.glint2pupil_V = None


    # create an image box which incorporates the tilt of the head
    def make_image_box(self, tilt_rad, height2width_ratio = 0.5):
        self.tilt_rad = tilt_rad
        # self.image_box = eye_image_box(self, height2width_ratio)
        self.image_box = angled_box(
            self.bounds.left_pixel_coords,
            self.bounds.right_pixel_coords,
            self.bounds.top_pixel_coords,
            self.bounds.bottom_pixel_coords,
            angle_rad = self.tilt_rad,
            height2width_ratio = height2width_ratio, 
            pixel_of_interest = self.pupil
            )

    # add the given glint pair to the eye, then give it the eye sub-image coordinates to add absolute coordinates to the glint pair
    def add_glint_pair(self, glint_pair):
        self.glint_pair = glint_pair
        self.glint_pair.add_absolute_coords(self.image_box.origin.x, self.image_box.origin.y)
        (self.glint_pair_ave_abs_x, self.glint_pair_ave_abs_y) = self.glint_pair.get_ave_abs_xy()

    # get the glint pair average x and y positions in absolute coordinates
    def get_glint_pair_ave_xy(self):

        # if the absolute coordinates have not yet been added, add them now
        if self.glint_pair_ave_abs_x == None:
            self.glint_pair.add_absolute_coords(self.image_box.origin.x, self.image_box.origin.y)
            (self.glint_pair_ave_abs_x, self.glint_pair_ave_abs_y) = self.glint_pair.get_ave_abs_xy()

        return (self.glint_pair_ave_abs_x, self.glint_pair_ave_abs_y)

    # return a string format of the absolute positions of the glint (for the csv file)
    def get_glint_pair_str(self, dp=2):
        if self.glint_pair == None:
            return ""
        elif self.glint_pair.ave_abs_x == None:
            self.glint_pair.add_absolute_coords(self.image_box.origin.x, self.image_box.origin.y)
        else:
            return "[{}, {}]".format(self.glint_pair.left.get_abs_xy_str(dp), self.glint_pair.right.get_abs_xy_str(dp))

    def get_eye_corner_pair_str(self, dp=2):

        return "[{:.{dp}f}, {:.{dp}f}, {:.{dp}f}, {:.{dp}f}]".format(self.eye_corner_left.x, self.eye_corner_left.y, self.eye_corner_right.x, self.eye_corner_right.y, dp=dp)

    # calculate the vector from the average glint position to the pupil center, then normalise by the distnace between the glint pair averages (between eyes)
    def get_normalized_ave_glint2_pupil_vector(self):
        # TODO: change normalising dist to distance between glints in each eye
        # if the abs coords have not yet been added (somehow), add them now
        if self.glint_pair.ave_abs_x == None:
            self.glint_pair.add_absolute_coords(self.image_box.origin.x, self.image_box.origin.y)

        # average glint pair and pupil locations
        (ave_glint_x, ave_glint_y) = self.glint_pair.get_ave_abs_xy()
        (pupil_x, pupil_y) = (self.image_box.absolute_POI_coords.x, self.image_box.absolute_POI_coords.x)

        # calc vector from glint to pupil, then normalize
        normal_dist = self.glint_pair.get_dist_between_glints()
        print("Glint method: normal dist = ",normal_dist)
        V_x = (pupil_x - ave_glint_x)/normal_dist
        V_y = (pupil_y - ave_glint_y)/normal_dist
        self.glint2pupil_V = [V_x, V_y]

        return (V_x, V_y)

    def get_normalized_corner2pupil_vector(self):
        # https://sci-hub.mksa.top/10.1145/2168556.2168598
        # NOTE: paper compares performance of different vectors/features: 1. ave_eye_corner_pos2pupil or 2. ave(both ave_eye_corner_pos2pupil)
        print("TODO: glint-less gaze tracking formula, dummy data returned")
        
        # V_N = (V_Nx, V_Ny), normalised pupil-corner vectors
        V_N_left_eye_corner_x = (self.pupil.x - self.eye_corner_left.x)/self.eye_corner_dist
        V_N_left_eye_corner_l = (self.pupil.y - self.eye_corner_left.y)/self.eye_corner_dist
        V_N_right_eye_corner_x = (self.pupil.x - self.eye_corner_right.x)/self.eye_corner_dist
        V_N_right_eye_corner_l = (self.pupil.y - self.eye_corner_right.y)/self.eye_corner_dist

        # average vectors
        V_N_ave_x = (V_N_left_eye_corner_x + V_N_right_eye_corner_x)/2
        V_N_ave_y = (V_N_left_eye_corner_l + V_N_right_eye_corner_l)/2

        return (V_N_ave_x, V_N_ave_y)

# class to store both eyes conveniently 
class eye_pair():
    def __init__(self, left_eye, right_eye):
        self.left = left_eye
        self.right = right_eye
        self.glint_distance = None

    def make_image_boxes(self, tilt_rad, height2width_ratio = 0.5):
        self.left.make_image_box(tilt_rad, height2width_ratio)
        self.right.make_image_box(tilt_rad, height2width_ratio)

    def get_iris_pixels(self):
        return self.left.iris.get_pixel_coords() + self.right.iris.get_pixel_coords()
    
    def get_pupil_pixels(self):
        return (self.left.pupil,self.right.pupil)

    def calc_glint_ave_eye_dist(self, dp=2):
        # this is the average distance between the corresponding glints in each eye.
        # i.e ave(left2left_dist, right2right_dist)
        # Equivalent to dist(ave_left_eye_glints, ave_right_eye_glints)
        if self.left.glint_pair is None or self.right.glint_pair is None:
            print("Glint pairs are none, yet asking for glint distance")
            return "" #TODO: shouldn't bother recording this line in the csv anyway, same with the non-fixation points
            # raise ValueError('Glint pairs are none, yet asking for glint distance')

        self.glint_distance = hypot(self.left.glint_pair_ave_abs_x - self.right.glint_pair_ave_abs_x, 
                                    self.left.glint_pair_ave_abs_y- self.right.glint_pair_ave_abs_y)

        return "{:.{dp}f}".format(self.glint_distance, dp=dp)

    def get_normalized_ave_glint2pupil_vector_pair(self):

        if self.glint_distance is None:
            self.calc_glint_ave_eye_dist()

        (l_e_x, l_e_y) = self.left.get_normalized_ave_glint2_pupil_vector()
        (r_e_x, r_e_y) = self.right.get_normalized_ave_glint2_pupil_vector()

        return [l_e_x, l_e_y, r_e_x, r_e_y]

    def get_normalized_corner2pupil_vector_pair(self):

        (l_e_x, l_e_y) = self.left.get_normalized_corner2pupil_vector()
        (r_e_x, r_e_y) = self.right.get_normalized_corner2pupil_vector()

        return [l_e_x, l_e_y, r_e_x, r_e_y]

# class to store the calculated head pose for the current frame
class head_pose():
    def __init__(self, rot_vec, trans_vec, head_orientation, nose_line_points):
        self.rot_vec = rot_vec
        self.trans_vec = trans_vec
        self.orientation = head_orientation
        self.nose_line_points = nose_line_points
        # TODO atm these values seem garbage

    def add_centroid(self, centre_point_pixel_coord):
        self.centre = centre_point_pixel_coord

    def get_orientation_xyz_str(self, dp=2):
        return self.orientation.get_str()

    def get_dist_xyz_str(self, dp=2):
        x = float(np.round(self.trans_vec[0],dp))
        y = float(np.round(self.trans_vec[1],dp))
        z = float(np.round(self.trans_vec[2],dp))
        return "[{}, {}, {}]".format(x,y,z)

