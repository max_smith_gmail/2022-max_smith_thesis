from sqlite3 import Time
import threading
from tkinter import colorchooser
import pyautogui # Cheatsheet: https://pyautogui.readthedocs.io/en/latest/quickstart.html
import time
import numpy as np
from tkinter import *
import json
import os



# object for circles
class circle_widget:
    def __init__(self, canvas, r, center, col):

        # create circle based on center and radius then store id
        x0,y0,x1,y1 = center[0]-r, center[1]-r, center[1]+r,center[1]+r
        self.canvas = canvas
        self.id = canvas.create_oval(x0,y0,x1,y1, fill=col)

        # store relevant variables/properties for later
        self.col = col
        self.r = r
        self.coords = center

    # # update the movement coordinates for later access
    # def update_coords(self,coords):
    #     self.coords = coords
        
    def update_colour(self, new_col):
        self.canvas.itemconfig(self.id, fill=new_col)

    def move_center(self, center_x, center_y):
        self.canvas.coords(self.id, center_x-self.r, center_y-self.r, center_x+self.r, center_y+self.r)
        self.coords = [center_x, center_y]

    def update_radius(self, new_rad):
        x0,y0,x1,y1 = self.coords[0]-new_rad, self.coords[1]-new_rad, self.coords[0]+new_rad, self.coords[1]+new_rad
        self.canvas.coords(self.id, x0, y0, x1, y1)
        self.r = new_rad



class calib_window:
    def __init__(self, gaze_calibration_settings):

        # store window parameters
        self.fixation_object_details = gaze_calibration_settings["fixation_object_details"]
        self.calib_pattern_dets = gaze_calibration_settings["calib_pattern_details"]
        self.background_col = gaze_calibration_settings["background_col"]
        # get screen resolution/size in pixels
        [self.screen_w_px, self.screen_h_px] = pyautogui.size()

        self.create_root()
        self.get_window_dimensions_mm()
        self.create_canvas()
        self.create_fixation_obj()

    def destroy(self):
        self.root.destroy()

    # Create window (root) for calibration then set parameters
    def create_root(self):
        self.root = Tk()
        self.root.attributes("-fullscreen", True)
        self.root.configure(width=self.screen_w_px, height=self.screen_h_px)
        self.root.configure(bg=self.background_col)

    # get the dpi to determine the physical size of the monitors, 1 inch = 25.4mm
    def get_window_dimensions_mm(self):
        self.dpi = self.root.winfo_fpixels('1i')
        self.screen_w_mm = self.screen_w_px/ self.dpi * 25.4
        self.screen_h_mm = self.screen_h_px/ self.dpi * 25.4

    # create canvas and circles for movement. pack makes canvas visible in window
    def create_canvas(self): #TODO make colour settings in settings.json
        self.my_canvas = Canvas(self.root, width=self.screen_w_px, height=self.screen_h_px, bg=self.background_col)
        self.my_canvas.pack()

    # create inner and outer circles for fixation object
    def create_fixation_obj(self):
        
        # figure out the largest radius to determine the centering of the inner circle
        largest_radius = 0
        for obj_no in self.fixation_object_details:
            if self.fixation_object_details[obj_no]["radius_px"] > largest_radius:
                largest_radius = self.fixation_object_details[obj_no]["radius_px"]

        # create list of the fixation objects
        origin_coords = [int(largest_radius), int(largest_radius)]
        self.circles = []
        self.max_fixation_obj_radius = largest_radius

        for obj_no in self.fixation_object_details:

            # get circle details and create new obj
            circ = self.fixation_object_details[obj_no]
            new_circle = circle_widget(self.my_canvas, r = circ["radius_px"], center = origin_coords, col = circ["col"])
            self.circles.append(new_circle)

    def shrink_to_min(self, duration_ms):
        inner_circle = self.circles[1]
        dt_ms = 10
        num_of_shrinks = int(duration_ms/dt_ms)
        change_in_rad = (inner_circle.r-1)
        shrink_per_step = change_in_rad/num_of_shrinks

        current_rad = inner_circle.r
        for i in range(0,num_of_shrinks):
            # print("shrink to {} pixels".format(int(current_rad)))
            current_rad -= shrink_per_step
            inner_circle.update_radius(int(current_rad))
            # inner_circle.update_radius(20)
            self.my_canvas.update()
            time.sleep(dt_ms/1000)

        # reset to size of outer circle
        inner_circle.update_radius(self.circles[0].r)
        self.my_canvas.update()

    # determine vector for movement between points
    def make_path(self, origin, dest, duration_ms):

        # move by up to 1 pixel at a time, duration determines stop time between moves
        longest_dim_pixels = max(abs(dest[0] - origin[0]), abs(dest[1] - origin[1]))

        x_path = np.linspace(origin[0],dest[0], longest_dim_pixels, dtype=int)
        y_path = np.linspace(origin[1],dest[1], longest_dim_pixels, dtype=int)
        delay_between_moves = int((duration_ms/longest_dim_pixels))

        return [x_path, y_path, delay_between_moves]

    # moves circles along a given vector, waiting between pixel movements for delay_ms
    def move_circles(self, vector, idx, delay_ms):

        # move each circle and update their known coords (for use later)
        for circle in self.circles:
            circle.move_center(vector[0][idx], vector[1][idx])
        
        # update display to show new circle positions
        self.my_canvas.update()
        if (idx == len(vector[0]) - 1):
            return

        # wait specified delay
        time.sleep(delay_ms/1000)

        # recurse function
        self.move_circles(vector, idx+1, delay_ms)

    def run_calib_sequence(self, frame_start_time, calib_fixation_freq, thread_events):
        # adjust screen size in all directions so cursor is always visible
        edge_offset = (self.max_fixation_obj_radius)
        screen_w = self.screen_w_px - int(edge_offset*2)
        screen_h = self.screen_h_px - int(edge_offset*2)

        # gather pattern m*n dets from config file
        x_calib_points = self.calib_pattern_dets["cols"]
        y_calib_points = self.calib_pattern_dets["rows"]
        fixation_duration_ms = self.calib_pattern_dets["fixation_duration_ms"]
        movement_duration_ms = self.calib_pattern_dets["movement_duration_ms"]

        # update visual for first fixation before starting pattern
        self.my_canvas.update()

        # delay before start to get initial fixation
        time.sleep(fixation_duration_ms/1000)

        # loop through grid pattern, recording the previous location for each point.
        # Previous and current point then used to make a path for fixation objects to follow
        first = True
        for calib_row in range(y_calib_points):
            for calib_col in range(x_calib_points):

                # move right vs move left for snake pattern
                if (calib_row%2 == 1):
                    calib_col = (x_calib_points-1) - calib_col

                # account for edge offsets
                x_pix = int(calib_col *(screen_w/(x_calib_points-1))) + int(edge_offset)
                y_pix = int(calib_row *(screen_h/(y_calib_points-1))) + int(edge_offset)
                
                # move fixation objects
                if not first:
                    path = self.make_path(origin=[x_prev_pix,y_prev_pix], dest=[x_pix,y_pix], duration_ms=movement_duration_ms)
                    vector = path[0:2]
                    delay_ms = path[2]
                    
                    self.move_circles(vector=vector, idx=0, delay_ms=delay_ms)
                else:
                    first = False

                # once a new fixation point has been reached, set event to inform the recording thread
                thread_events.global_fixation_coords = [x_pix, y_pix]
                thread_events.fixation_event.set()

                # wait for given duration at the fixation point

                self.shrink_to_min(fixation_duration_ms)

                # when moving once again, clear event flag so that the recording thread can stop logging data
                thread_events.fixation_event.clear()

                x_prev_pix = x_pix
                y_prev_pix = y_pix
        self.destroy()



def main():

    # confirmation screen (waiting for user ready)
    if pyautogui.confirm(text="Start Calibration Sequence?", title="Confirmation", buttons=["Start", "Cancel"]) == "Cancel":
        exit()

    # gather config info from file
    config_file = open("settings.json")
    settings = json.load(config_file)
    config_data = settings["gaze_calibration"]

    test_calib_window = calib_window(config_data) 
    test_calib_window.run_calib_sequence()



# main()