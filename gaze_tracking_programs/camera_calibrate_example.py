import accessCameras
import numpy as np
import cv2
import sys

# https://github.com/niconielsen32/ComputerVision/blob/master/cameraCalibration.py

################ FIND CHESSBOARD CORNERS - OBJECT POINTS AND IMAGE POINTS #############################

chessboardSize = (8,6)
frameSize = (1920,1080)


# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 20, 0.001)


# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((chessboardSize[0] * chessboardSize[1], 3), np.float32)
objp[:,:2] = np.mgrid[0:chessboardSize[0],0:chessboardSize[1]].T.reshape(-1,2)

size_of_chessboard_squares_mm = 12.5
objp = objp * size_of_chessboard_squares_mm


# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

# # get possible options for video input
# # video_options = accessCameras.gather_JSON_data("cameraOptions.json")
# video_options = accessCameras.gather_JSON_data(sys.argv[1])

# # prompt user to select video input source/option
# selected_vid = accessCameras.user_camera_selection(video_options)

# open video stream and select chosen parameters
# cap = accessCameras.open_video(selected_vid)

# from recordingAug_18_11:46:51calibration_rec
vid_location = "Recorded_data/Aug_18_11:56:40_pnr_calib_rec/Aug_18_11:56:40calibration_rec.avi"
# vid_location = "Recorded_data/Aug_18_10:22:13_pnr_calib_rec/Aug_18_10:22:13calibration_rec.avi"
cap = cv2.VideoCapture(vid_location)

# continue until cancelled by user
while 1:

    # Capture frame
    success, frame = cap.read()
    if success == False:
        break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, corners = cv2.findChessboardCorners(gray, chessboardSize, None)

    # If found, add object points, image points (after refining them)
    if ret == True:

        objpoints.append(objp)
        corners2 = cv2.cornerSubPix(gray, corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)

        # Draw and display the corners
        cv2.drawChessboardCorners(frame, chessboardSize, corners2, ret)
        print("found board")

    
    print("done frame")
    # resize the frame for viewing
    scale_percent = 50
    width = int(frame.shape[1] * scale_percent / 100)
    height = int(frame.shape[0] * scale_percent / 100)
    dim = (width, height)
    frame = cv2.resize(frame, dim, interpolation = cv2.INTER_AREA)
    cv2.imshow('img', frame)
    k = cv2.waitKey(1)

    


cv2.destroyAllWindows()




############## CALIBRATION #######################################################
ret, cameraMatrix, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, frameSize, None, None)


############## UNDISTORTION #####################################################
# img = cv2.imread('cali5.png')
# h,  w = img.shape[:2]
h = 1920
w = 1080
newCameraMatrix, roi = cv2.getOptimalNewCameraMatrix(cameraMatrix, dist, (w,h), 1, (w,h))

print(newCameraMatrix)
print(roi)

# # Undistort
# dst = cv2.undistort(img, cameraMatrix, dist, None, newCameraMatrix)

# # crop the image
# x, y, w, h = roi
# dst = dst[y:y+h, x:x+w]
# cv2.imwrite('caliResult1.png', dst)


# # Undistort with Remapping
# mapx, mapy = cv2.initUndistortRectifyMap(cameraMatrix, dist, None, newCameraMatrix, (w,h), 5)
# dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)

# # crop the image
# x, y, w, h = roi
# dst = dst[y:y+h, x:x+w]
# cv2.imwrite('caliResult2.png', dst)


# # Reprojection Error
# mean_error = 0

# for i in range(len(objpoints)):
#     imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], cameraMatrix, dist)
#     error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2)/len(imgpoints2)
#     mean_error += error

# print( "total error: {}".format(mean_error/len(objpoints)) )