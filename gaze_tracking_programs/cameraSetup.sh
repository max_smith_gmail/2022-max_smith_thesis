#! /bin/bash
# v4l2-ctl -d /dev/video0 --list-formats-ext
tempFilename="availableCamerasInfo.txt"
outputJSONFilename="cameraOptions.json"

echo '' > $tempFilename
echo '' > $outputJSONFilename
for d in /dev/video*
    do echo $d >> $tempFilename
    v4l2-ctl --list-formats-ext -d $d  >> $tempFilename
done

python3 cameraInfo2JSON.py $tempFilename $outputJSONFilename
rm $tempFilename
# v4l2-ctl --list-formats-ext -d /dev/video0 
python3 face_analysis.py $outputJSONFilename