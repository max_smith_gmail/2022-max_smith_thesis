from signal import pause
import sys
import cv2
import time
import numpy as np
import csv
import json
import os
import pyautogui
import threading
from calib_pattern import calib_window, circle_widget
from threading_events_class import threading_events
import shutil
import accessCameras
import mediapipe_facemesh_module as mp_mod
from facemesh_classes import *
# import detect_glints
import regression

# Priority list
# 1. update chapter 1 thesis
    # if no figures made then do quick sketch to for placeholder on what to talk about
# 2. chapter 2 thesis
    # note main research objectives/contributions, copy into power point

# For Next week:
# chapter 1 done
# chapter 2 almost entirely done
    # power point presentation start



# TODO CRITICAL PATH
# 1. annotate saved eye images- check glint functions as they has a live annotation image.

# get figures for thesis to define vectors etc also head pos feedback
# check accuracy of pupil location (highlight 4* glints and 2* pupils on main image for clarity)
# check vector normalization process is identical for regression and manual calculation during gaze
# check if glints are being accurately detected during both
# make blink counter
# may need to create a big database for the regression model
# plot V_x and V_y against gaze vector on 3d scatter plot (1 plot for each eye and direction)

# TODO PERIPHERY
# camera calibration using small board
# fix left lean in head pose calc (1. try adding calibration matrix, 3. read cv2.solvePnP 2. different method (EPNP))
# change naming convention for eye fixations- atm they do left/right_top/down which is unintuitive
# make eye boxes based on eye corners only (to avoid up/down issue with eye boxes)
# FPS issue with video recording
# investigate previous transform work- made program v slow if fixed would be v helpful
# fix bug when pattern coordinates are set to 2 rows * 3 cols
# head positioning feedback section
# deal with using recorded vid instead of live (cmd line or in settings.json)
# make colours of annotations and window background part of settings.json
# make program installable (convert python to .exe file, maybe zip and unzip)
# make sure face mesh is only doing essential processes (e.g. not finding head border or margins if no feedback needed)
# make figure to explain calib_pattern


# TODO notes for thesis
# chapter 2
    # comparison table of gaze tracking techniques
# chapter 3
    # camera calibration -> get both extrinsic and intrinsic parameters 
    # gaze calibration -> user eyes/pupil vs fixation point on screen minimise error
# chapter 4
    # mention hough transform slow speed -> why this was discarded

# use checkerboard pattern to remove distortion from image
def calibrate_camera(cap, settings, face_detector, data_folder):
    cam_calib = settings["camera_calibration"]
    chessboardSize = (cam_calib["checkerboard_dimensions"]["w"],cam_calib["checkerboard_dimensions"]["h"])
    
    # get vcap property 
    width  = 1080#int(cap.get(cv2.CV_CAP_PROP_FRAME_WIDTH))
    height = 1920#int(cap.get(cv2.CV_CAP_PROP_FRAME_HEIGHT))  
    frameSize = (width,height)

    # display live video and take screenshots for calibration using the space bar
    # TODO this does not work/save value for next function
    settings["live_debug"]["space_save_imgs"] = True
    record_gaze_calib_data(cap, settings, face_detector, data_folder)
    # termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((chessboardSize[0] * chessboardSize[1], 3), np.float32)
    objp[:,:2] = np.mgrid[0:chessboardSize[0],0:chessboardSize[1]].T.reshape(-1,2)

    size_of_chessboard_squares_mm = cam_calib["checker_size_mm"]
    objp = objp * size_of_chessboard_squares_mm

    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.


    # TODO: up to here, make function to read folder, run analysis then return calibration matricies (or object of them?)
    img_folder = os.path.join(data_folder,"screenshots")
    for img_name in os.listdir(img_folder):
        print(img_name)
        calib_img = cv2.imread(os.path.join(img_folder, img_name))
        calib_img = cv2.cvtColor(calib_img, cv2.COLOR_BGR2GRAY)

        # Find the chess board corners
        ret, corners = cv2.findChessboardCorners(calib_img, chessboardSize, None)

        # If found, add object points, image points (after refining them)
        if ret == True:

            objpoints.append(objp)
            corners2 = cv2.cornerSubPix(calib_img, corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners)
            cv2.drawChessboardCorners(calib_img, chessboardSize, corners2, ret)
            print("successly identified calibration board")
        else:
            print("couldn't find board")


        # resize the frame for viewing
        scale_percent = 50
        width = int(calib_img.shape[1] * scale_percent / 100)
        height = int(calib_img.shape[0] * scale_percent / 100)
        dim = (width, height)
        calib_img = cv2.resize(calib_img, dim, interpolation = cv2.INTER_AREA)
        
        # Draw and display the corners
        cv2.imshow('img', calib_img)
        cv2.waitKey(1)
        
    ############## CALIBRATION #######################################################
    ret, cameraMatrix, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, frameSize, None, None)


    ############## UNDISTORTION #####################################################
    img = cv2.imread(os.path.join(img_folder,os.listdir(img_folder)[0]) )
    h,  w = img.shape[:2]
    newCameraMatrix, roi = cv2.getOptimalNewCameraMatrix(cameraMatrix, dist, (w,h), 1, (w,h))

    # Undistort
    dst = cv2.undistort(img, cameraMatrix, dist, None, newCameraMatrix)

    # crop the image
    x, y, w, h = roi
    dst = dst[y:y+h, x:x+w]
    # cv2.imwrite('caliResult1.png', dst)


    # Undistort with Remapping
    mapx, mapy = cv2.initUndistortRectifyMap(cameraMatrix, dist, None, newCameraMatrix, (w,h), 5)
    dst = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)

    # crop the image
    x, y, w, h = roi
    dst = dst[y:y+h, x:x+w]
    # cv2.imwrite('caliResult2.png', dst)

    # Reprojection Error
    mean_error = 0

    for i in range(len(objpoints)):
        imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], cameraMatrix, dist)
        error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2)/len(imgpoints2)
        mean_error += error

    print("total error: {}".format(mean_error/len(objpoints)))

    # TODO: add calibration matrix to settings.json
    print("calibrate camera")

# use known distance between camera and face to calculate the focal length
def update_focal_length(cap, settings, face_detector):

    gaze_tracking = settings["gaze_tracking"]
    
    # run until face is found in image
    annotated_frame = None
    while annotated_frame is None:

        # get image
        success, frame = cap.read()

        # Stop if reading image failed
        if success is False:
            raise Exception("Could not read image capture")
        if np.shape(frame) == [0,0,0]:
            raise Exception("Could not read image capture")

        # run frame to determine pupil width in pixels
        annotated_frame = face_detector.process_frame(
                    frame = frame, 
                    landmark_annotate = False, 
                    eye_annotate = False,
                    eye_corner_vectors_annotate = False,
                    pose_annotate = False, 
                    show_eyes = False,
                    save_eyes = False,
                    time_count = 0,
                    thread_events = None
                    )

    left = face_detector.eyes.left.pupil
    right = face_detector.eyes.right.pupil
    pupil_width_pixels = left.get_diag_dist_to_coord(right)

    # get required input from user and calculate new focal length
    face_distance_cm = input("Enter distance (cm) from camera to bridge of nose:")
    pupil_width_input = input("Enter pupilary distance of user or hit enter to use default of 6.3cm")
    if pupil_width_input == '':
        pupil_width_cm = 6.3
    else:
        pupil_width_cm = float(pupil_width_input)

    # update focal length measurement in settings.json
    settings["hardware_setup"]["focal_length"] = (pupil_width_pixels * float(face_distance_cm))/pupil_width_cm
    print("focal length = {}".format(settings["hardware_setup"]["focal_length"]))

    # dist = (width_cm * focal_len )/width_pixels
    return settings

# Provide positioning feedback to the user
def run_camera_positioning_feedback(cap, settings, face_detector):
    """
        1. process frame
        2. run positioning feedback function
        3. display annotation
    """
    # annotated_frame = face_detector.process_frame(
    #                 frame = frame, 
    #                 landmark_annotate = False, 
    #                 eye_annotate = False, 
                    # eye_corner_vectors_annotate = False,
    #                 pose_annotate = False, 
    #                 positioning_feelback = False,
    #                 frame_head_margins = frame_head_margins,
    #                 show_eyes = False,
    #                 save_eyes = False,
    #                 time_count = 0,
    #                 thread_events = None
    #                 )
    # frame_head_margins = [settings["head_position_feedback"]["frame_margin_inner"], 
    #                         settings["head_position_feedback"]["frame_margin_outer"]]

    # while 1:
    #     if face_detector.check_head_positioning( frame_head_margins) is not None:
    #         # show img

    # TODO will just show the head positioning feedback to the user.
    print("run camera positioning feedback")

# show the live image with any annotations requested in settings
def run_live_debug(cap, settings, face_detector, data_folder):

    # gather settings
    gaze_calibration = settings["gaze_calibration"]
    live_debug = settings["live_debug"]
    hardware_setup = settings["hardware_setup"]

    if live_debug["record_video"]:
        vid_writer = accessCameras.create_video_writer(
                    cap, 
                    file_location = data_folder,
                    filename_tag = live_debug["video_tag"],
                    turn_angle = hardware_setup["camera_orientation"]
                    )

    # if fps or time will be annotated onto live or recorded frame 
    if (live_debug["fps_annotate"] or live_debug["time_annotate"]):
        frame_start_time = time.time()
        time_total = 0
        
    # continue until cancelled by user
    while 1:
        annotated_frame = None
        # Capture frame
        success, frame = cap.read()
        
        # rotate capture if needed. turn clockwise up to 3 times
        if settings["hardware_setup"]["camera_orientation"] %360 != 0:
            for i in range(int(settings["hardware_setup"]["camera_orientation"]/90)):
                frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)

        # Stop if reading failed
        if success is False:
            raise Exception("Could not read image capture")
        if np.shape(frame) == [0,0,0]:
            raise Exception("Could not read image capture")

        # adjust colour and orientation of image
        frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        # update time if the live time will be annotated onto the live frame or the recorded frame
        if live_debug["time_annotate"]:
            time_count = time_total
        else:
            time_count = None

        # run facemesh detector and the settings for analysis
        annotated_frame = face_detector.process_frame(
                    frame = frame, 
                    landmark_annotate = live_debug["landmark_annotate"], 
                    eye_annotate = live_debug["eye_annotate"],
                    eye_corner_vectors_annotate = live_debug["eye_corner_vectors_annotate"],
                    pose_annotate = live_debug["pose_annotate"],
                    show_eyes = live_debug["show_eye_imgs"],
                    save_eyes = live_debug["save_eye_imgs"],
                    time_count = time_count,
                    thread_events = None
                    )

        # skip if failed
        if annotated_frame is None:
            print("frame annotation failed")
            annotated_frame = frame
            


        # TODO: Show glint2pupil vectors in live time so that glint finding functions can be improved




        # calculate fps and update time total if needed
        if (live_debug["fps_annotate"] or live_debug["time_annotate"]):

            # calcuate FPS and update time_total
            current_time = time.time()
            fps = 1 / (current_time - frame_start_time)
            time_total += current_time - frame_start_time
            frame_start_time = current_time


        # if recording the video
        if gaze_calibration["record_video"]:

            # annotate time and FPS onto recorded vid if needed
            if live_debug["fps_annotate"]:
                cv2.putText(annotated_frame, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)           
            if live_debug["time_annotate"]:
                cv2.putText(annotated_frame, "T={:.2f} (s)".format(time_count), (int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) - 200, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
            
            # record the annotated or plain frame depending on settings
            if gaze_calibration["annotate_video"]:
                vid_writer.write(annotated_frame)
            else:
                vid_writer.write(frame)

        # resize the frame for viewing/display
        scale_percent = live_debug["live_display_shrink_percentage"]
        width = int(annotated_frame.shape[1] * scale_percent / 100)
        height = int(annotated_frame.shape[0] * scale_percent / 100)
        dim = (width, height)
        resized_frame = cv2.resize(annotated_frame, dim, interpolation=cv2.INTER_AREA)
        
        # if being recorded note on live display, annotate time and FPS
        # if live_debug["record_video"]:
        #     cv2.putText(annotated_frame, "Recording...", (20, 110), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)     
        # if live_debug["time_annotate"]:
        #     cv2.putText(annotated_frame, "Time: {:.2}".format(float(time_total)), (20, 110), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)     
        # if live_debug["fps_annotate"]:
        #     cv2.putText(annotated_frame, "T={:.2f} (s)".format(time_count), (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)) - 200, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

        cv2.imshow('Resized Video Recording Preview', resized_frame)

        # Waits for a user input to quit the application #TODO this doesn't work
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    # close the video and writer then  destroy all windows
    if live_debug["record_video"]:
        vid_writer.release()

# Run the calibration pattern and analyse the frames concurrently using threads
def run_calib_process(settings, thread_events):
    calib_fixation_freq = settings["gaze_calibration"]["calib_fixation_freq"]

    # create window for calibration
    test_calib_window = calib_window(settings["gaze_calibration"]) 
    
    # let recording thread know they can start
    thread_events.record_ready_event.wait() #TODO make mutual, so both need to wait

    # record the start time
    frame_start_time = time.time()

    # run calibration sequence
    test_calib_window.run_calib_sequence(frame_start_time, calib_fixation_freq, thread_events)

    # once finished, set the event so that the recording can stop
    thread_events.finished_calib_event.set()

# Record data for gaze calibration
def record_gaze_calib_data(cap, settings, face_detector, data_folder, thread_events=None):

    # gather settings
    general = settings["general"]
    gaze_calibration = settings["gaze_calibration"]
    live_debug = settings["live_debug"]
    gaze_tracking = settings["gaze_tracking"]
    hardware_setup = settings["hardware_setup"]

    # if program will record video, make object for video writing
    if gaze_calibration["record_video"]:
        vid_writer = accessCameras.create_video_writer(
                    cap, 
                    file_location = data_folder,
                    filename_tag = live_debug["video_tag"],
                    turn_angle = hardware_setup["camera_orientation"]
                    )
    
    # if saving screenshots triggered from user input
    if live_debug["space_save_imgs"]:
        # create folder to store all recorded data
        screenshot_folder = os.path.join(data_folder, "screenshots")
        os.mkdir(screenshot_folder)


    # create a csv file for recording eye position and write header row
    eye_pos_file = open(data_folder + "/" + "eye_pos.csv","w")
    eye_pos_file_writer = csv.writer(eye_pos_file)
    eye_pos_file_writer.writerow(face_detector.get_eye_data_header())

    # let the calibration thread know that recording is ready to start
    if thread_events is not None:
        thread_events.record_ready_event.set()

    # if fps or time will be annotated onto live or recorded frame 
    frame_start_time = time.time()
    time_total = 0

    # store initial status of fixation event flag
    if thread_events is not None:
        prev_fixation_status = thread_events.fixation_event.is_set()

    # record previous glint locations for glint isolation assistance # TODO may be redundant, just use glint_pairs (the current one)
    prev_glint_pairs = [None,None]

    # continue until calibration finishes in other thread
    while 1:

        # Capture frame
        success, frame = cap.read()

        # Stop if reading failed
        if success is False:
            raise Exception("Could not read image capture")
        if np.shape(frame) == [0,0,0]:
            raise Exception("Could not read image capture")

        # adjust colour and orientation of image
        frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        
        # update time if the live time will be annotated onto the live frame or the recorded frame
        time_count = time_total

        # Specify the margins for the head positioning, and the settings for analysis   
        annotated_frame = face_detector.process_frame(
                    frame = frame, 
                    landmark_annotate = live_debug["landmark_annotate"], 
                    eye_annotate = live_debug["eye_annotate"], 
                    eye_corner_vectors_annotate = live_debug["eye_corner_vectors_annotate"],
                    pose_annotate = live_debug["pose_annotate"],
                    show_eyes = settings["live_debug"]["show_eye_imgs"],
                    save_eyes = (general["calibrate_gaze"] and gaze_calibration["save_eye_imgs"]),
                    time_count = time_count,
                    thread_events = thread_events
                    )

        # if tried and failed to process frame, then go to next frame
        if annotated_frame is None:
            continue

        # if analysis was successful, isolate the glints within the image
        elif gaze_tracking["no_glints"] == False:
            glint_pairs = face_detector.get_glint_pairs(
                    glint_detect_params = gaze_tracking["glint_detection_parameters"],
                    prev_glint_pairs = prev_glint_pairs)

            # update the previous glints for next iteration, may be better to store old location for set duration 
            # (after which the old glint locations could be considered irrelevant?)
            prev_glint_pairs = glint_pairs
            if glint_pairs is None:
                continue

        
        # if saving eye images and a new fixation point is reached, create new folders for fixation
        if thread_events is not None:
            current_fixation_status = thread_events.fixation_event.is_set()
            if gaze_calibration["save_eye_imgs"]:

                # previously flag was off (not at fixation) but recently was set (arrived)
                if prev_fixation_status == False and current_fixation_status == True:
                    # TODO check that the calib thread has enough time to update the coords before they are checked here (unlikely but potentially hard bug later)
                    new_eye_img_folder_name = "fixation_at_" + "_".join([str(elem) for elem in thread_events.global_fixation_coords])
                    face_detector.new_eye_fixation_img_folder(new_eye_img_folder_name)

            # update old fixation status
            prev_fixation_status = current_fixation_status

        # add data to csv file
        if thread_events is not None:

            # if we are currently paused on a fixation point then we should record it
            if thread_events.fixation_event.is_set():
                fixation_coords_str = ["[{},{}]".format(thread_events.global_fixation_coords[0], thread_events.global_fixation_coords[1])]
                eye_pos_file_writer.writerow(face_detector.get_eye_data() + fixation_coords_str)
            
            # NOTE: could just ignore data which is not at a fixation
            # otherwise still record the data, but omit the fixation coordinate
            else:
                eye_pos_file_writer.writerow(face_detector.get_eye_data())

        # calcuate FPS and update time_total
        current_time = time.time()
        fps = 1 / (current_time - frame_start_time)
        time_total += current_time - frame_start_time
        frame_start_time = current_time

        # if recording the video
        if gaze_calibration["record_video"]:

            # annotate time and FPS onto recorded vid if needed
            if live_debug["fps_annotate"]:
                cv2.putText(annotated_frame, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)           
            if live_debug["time_annotate"]:
                cv2.putText(annotated_frame, "T={:.2f} (s)".format(time_count), (int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)) - 200, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
            
            # record the annotated or plain frame depending on settings
            if gaze_calibration["annotate_video"]:
                vid_writer.write(annotated_frame)
            else:
                vid_writer.write(frame)

        # check if the user is entering a key instruction (ESC or Space bar)
        k = cv2.waitKey(1)

        # ESC pressed
        if k == 27:
            print("Escape hit, closing...")
            break

        # save image if hits space
        if live_debug["space_save_imgs"] and  k == 32:
            img_name = os.path.join(data_folder, "screenshots", (("{:.2f}".format(round(time_count,3))).replace(':', '_')).replace('.', '_') + ".jpg")
            cv2.imwrite(img_name, annotated_frame)

        # if the calibration has finished (in other thread), then stop and save data
        if thread_events is not None:
            if thread_events.finished_calib_event.is_set():
                break
        else:
            # if not threading (not calibrating), then wait until time has elapsed
            if time_total >= live_debug["record_duration"]:
                break

    # close the video and writer then  destroy all windows
    if gaze_calibration["record_video"]:
        vid_writer.release()

    eye_pos_file.close()

# Ask the user to start the gaze calibration process and organise threads for it
def calibrate_gaze(cap, settings, face_detector, data_folder):

    # confirmation screen (waiting for user ready)
    if pyautogui.confirm(text="Start Calibration Sequence?", title="Confirmation", buttons=["Start", "Cancel"]) == "Cancel":
        exit()

    calib_pattern_dets = settings["gaze_calibration"]["calib_pattern_details"]
    fixation_object_details = settings["gaze_calibration"]["fixation_object_details"]

    # create object to store all threading events
    thread_events = threading_events()

    # create a thread for the calib window program and one for the video recording one
    threads = [
        threading.Thread(target=run_calib_process, args=(settings, thread_events)), 
        threading.Thread(target=record_gaze_calib_data, args=(cap, settings, face_detector, data_folder, thread_events))
        ]

    # start the threads
    for index, thread in enumerate(threads):
        thread.start()

    # wait for both to end
    for index, thread in enumerate(threads):
        thread.join()
    
    # calculate the polynomial coefficients for the current setup + user via regression
    updated_settings = run_calibration_calculation(cap, settings, data_folder)

    return updated_settings

# calculate the gaze coefficients for gaze estimation using previously recorded data
def run_calibration_calculation(cap, settings, data_folder):
    """
        use data collected in the csv file in the data folder to run regression to fit polynomial expression to fixation points
        1. normalise glint-pupil vectors with respect to one of the distances below. 
            e.g. V_left_eye_left_glint = (pupil - left_glint) /  distance
            i. dist between glints in eye
            ii. dist between glints in each eye (average pair location)
            iii. pupil distance
        2. average vectors: i.e. V_left_eye = 0.5(V_left_eye_left_glint + V_left_eye_right_glint)
        3. feed in vector to chosen equation e.g. https://sci-hub.mksa.top/10.1109/tbme.2012.2201716 page 3
            PoR_eye_x= Cx0 + Cx1*Vect_x + Cx2*Vect_x^3 + Cx3*Vect_y^2
            PoR_eye_y= Cy0 + Cy1*Vect_y + Cy2*Vect_y + Cy3*Vect_x^2*Vect_y^2
        4. run regression for each data point using pandas (similar to plot_pupil.py)


        ALTERNATIVE: machine learning to replace regression model
        TODO: after regression works try again with teachable machines (to show appearance based approach)

        NOTE: may need to stop running mediapipe while recording calib data, then run mediapipe on video afterwards

    """
    print("time to derive coeffs of polynomial expression!")

    # get and sort recorded data in csv file
    vectors, fixation_points = regression.gather_and_normalize_data(data_folder)
    
    if vectors[0] == None:
        no_glints = True
    g_vectors = vectors[0]
    ec_vectors = vectors[1]
    

    # gather data and calculate coefficients for either glint or eye corner vectors
    if (settings["gaze_tracking"]["no_glints"]):
        coeff_storage = settings["gaze_tracking"]["eye_corner_gaze_coeffs"]
        csv_data_file_name = data_folder + "/" + "eye_corner_vector_and_PoR_data.csv"
        V_lx = ec_vectors[0]
        V_ly = ec_vectors[1]
        V_rx = ec_vectors[2]
        V_ry = ec_vectors[3]
    else:
        coeff_storage = settings["gaze_tracking"]["glint_gaze_coeffs"]
        csv_data_file_name = data_folder + "/" + "glint_vector_and_PoR_data.csv"
        V_lx = g_vectors[0]
        V_ly = g_vectors[1]
        V_rx = g_vectors[2]
        V_ry = g_vectors[3]

    # calculate coeffs
    (left_eye_x_coeffs, left_eye_y_coeffs) = regression.optimise_coeffs2data(V_lx, V_ly, fixation_points)
    (right_eye_x_coeffs, right_eye_y_coeffs) = regression.optimise_coeffs2data(V_rx, V_ry, fixation_points)

    # Store gaze coefficients
    coeff_storage["L_e_x_coeffs"] = left_eye_x_coeffs.tolist()
    coeff_storage["L_e_y_coeffs"] = left_eye_y_coeffs.tolist()
    coeff_storage["R_e_x_coeffs"] = right_eye_x_coeffs.tolist()
    coeff_storage["R_e_y_coeffs"] = right_eye_y_coeffs.tolist()

    # caluculate the gaze estimations via glints and gaze for each eye 
    L_eye_PoR_x = (left_eye_x_coeffs[0] + left_eye_x_coeffs[1]*V_lx + left_eye_x_coeffs[2]*(V_lx**3) + left_eye_x_coeffs[3]*(V_ly**2)).astype(int)
    L_eye_PoR_y = (left_eye_y_coeffs[0] + left_eye_y_coeffs[1]*V_lx + left_eye_y_coeffs[2]*V_ly + left_eye_y_coeffs[3]*(V_lx**2)*V_ly).astype(int)
    R_eye_PoR_x = (right_eye_x_coeffs[0] + right_eye_x_coeffs[1]*V_rx + right_eye_x_coeffs[2]*(V_rx**3) + right_eye_x_coeffs[3]*(V_ry**2)).astype(int)
    R_eye_PoR_y = (right_eye_y_coeffs[0] + right_eye_y_coeffs[1]*V_rx + right_eye_y_coeffs[2]*V_ry + right_eye_y_coeffs[3]*(V_rx**2)*V_ry).astype(int)
    
    L_PoR = zip(*[L_eye_PoR_x, L_eye_PoR_y])
    R_PoR = zip(*[R_eye_PoR_x, R_eye_PoR_y])
    
    # create csv file 
    eye_pos_file = open(csv_data_file_name,"w")
    eye_pos_file_writer = csv.writer(eye_pos_file)
        
    # write data to csv file
    eye_pos_file_writer.writerow(['V_lx', 'V_ly', 'V_rx', 'V_ry', 'L_PoR', 'R_PoR', 'fixation_points'])
    vector_data = [V_lx, V_ly, V_rx, V_ry, L_PoR, R_PoR, fixation_points]
    vector_data = zip(*vector_data)
    eye_pos_file_writer.writerows(vector_data)

    return settings

# Run live gaze tracking using the gaze coefficients
def run_gaze_tracking(cap, settings, face_detector, data_folder):
    """
        1. get image
        2. run mediapipe w/ no annotations or face feedback
        3. get eye images
        4. detect glints in images (one thread each)
        5. interpolation method using glints + pupil + eye corners(?) https://sci-hub.mksa.top/10.1109/tbme.2012.2201716
            a. get vectors from glints to pupil center
            b. normalise glint-pupil vectors with respect to one of:
                i. dist between glints in eye
                ii. dist between glints in each eye (average pair location)
                iii. pupil distance
            c. put normalised vector data into polynomial expression with coeffs derived during calibration process
                - output is gaze location on the screen that it was calibrated to
    """
    #TODO: add in reset function (e.g. if user looks extreme right for 2+ secs then trigger recalibation or emergency etc)

    # gather settings
    general = settings["general"]
    live_debug = settings["live_debug"]
    gaze_tracking = settings["gaze_tracking"]
    hardware_setup = settings["hardware_setup"]
    gaze_coeffs = gaze_tracking["gaze_coeffs"]

    prev_glint_pairs = [None,None]

    # if program will record video, make object for video writing
    if gaze_tracking["record_video"]:
        vid_writer = accessCameras.create_video_writer(
                    cap, 
                    file_location = data_folder,
                    filename_tag = live_debug["video_tag"],
                    turn_angle = hardware_setup["camera_orientation"]
                    )

    # continue until quit from user
    while 1:

        # Capture frame
        success, frame = cap.read()

        # Stop if reading failed
        if success is False:
            raise Exception("Could not read image capture")
        if np.shape(frame) == [0,0,0]:
            raise Exception("Could not read image capture")

        # adjust colour and orientation of image
        frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # Specify the margins for the head positioning, and the settings for analysis     
        annotated_frame = face_detector.process_frame(
                    frame = frame, 
                    landmark_annotate = False, 
                    eye_annotate = False, 
                    eye_corner_vectors_annotate = False,
                    pose_annotate = False,
                    show_eyes = settings["live_debug"]["show_eye_imgs"],
                    save_eyes = gaze_tracking["save_eye_imgs"], #temp for development, will not save otherwise
                    time_count = None,
                    thread_events = None
                    )

        # if tried and failed to process frame, then go to next frame
        if annotated_frame is None:
            continue
        
        # TODO: check for blink- if so then go to next frame

        # if analysis was successful and using glints, then isolate them within the image
        elif (gaze_tracking["no_glints"] == False):
            glint_pairs = face_detector.get_glint_pairs(
                    glint_detect_params = gaze_tracking["glint_detection_parameters"],
                    prev_glint_pairs = prev_glint_pairs)

            # update the previous glints for next iteration, may be better to store old location for set duration 
            # (after which the old glint locations could be considered irrelevant?)
            prev_glint_pairs = glint_pairs
            
            # TODO deal with finding left but not right glints and vice versa

            # if couldn't find glints, then skip for next frame
            if glint_pairs == None:
                continue

            if live_debug["glints_annotate"]:
                annotated_frame = face_detector.annotate_glints()


        [PoR_x, PoR_y] = calc_POR(settings, face_detector)
        print("({},{})".format(PoR_x, PoR_y))

        # move cursor to location
        # pyautogui.moveTo(PoR_x,PoR_y)

        # record video if needed
        if gaze_tracking["record_video"]:
            vid_writer.write(annotated_frame)

        # ESC pressed, exit
        if cv2.waitKey(1) == 27:
            print("Escape key pressed, closing...")
            break

    # close the video and writer then  destroy all windows
    if gaze_tracking["record_video"]:
        vid_writer.release()

# calculate the estimated point of regard using either glints or eye corners and their calibrated data
def calc_POR(settings, face_detector):

    if (settings["gaze_tracking"]["no_glints"]):
        gaze_coeffs = settings["eye_corner_gaze_coeffs"]
        [l_e_x, l_e_y, r_e_x, r_e_y] = face_detector.eyes.get_normalized_corner2pupil_vector_pair()
    else:
        gaze_coeffs = settings["glint_gaze_coeffs"]
        [l_e_x, l_e_y, r_e_x, r_e_y] = face_detector.eyes.get_normalized_ave_glint2pupil_vector_pair()
    
    L_e_x_coeffs = gaze_coeffs["L_e_x_coeffs"]
    L_e_y_coeffs = gaze_coeffs["L_e_y_coeffs"]
    R_e_x_coeffs = gaze_coeffs["R_e_x_coeffs"]
    R_e_y_coeffs = gaze_coeffs["R_e_y_coeffs"]

    L_eye_PoR_x = L_e_x_coeffs[0] + L_e_x_coeffs[1]*l_e_x + L_e_x_coeffs[2]*(l_e_x**3) + L_e_x_coeffs[3]*(l_e_y**2)
    L_eye_PoR_y = L_e_y_coeffs[0] + L_e_y_coeffs[1]*l_e_x + L_e_y_coeffs[2]*l_e_y + L_e_y_coeffs[3]*(l_e_x**2)*l_e_y
    R_eye_PoR_x = R_e_x_coeffs[0] + R_e_x_coeffs[1]*r_e_x + R_e_x_coeffs[2]*(r_e_x**3) + R_e_x_coeffs[3]*(r_e_y**2)
    R_eye_PoR_y = R_e_y_coeffs[0] + R_e_y_coeffs[1]*r_e_x + R_e_y_coeffs[2]*r_e_y + R_e_y_coeffs[3]*(r_e_x**2)*r_e_y

    # average gaze
    PoR_x = int((L_eye_PoR_x + R_eye_PoR_x)/2)
    PoR_y = int((L_eye_PoR_y + R_eye_PoR_y)/2)

    # bound to inside screen
    if PoR_x < 0:
        PoR_x = 1
    elif PoR_x > pyautogui.size()[0]:
        PoR_x = pyautogui.size()[0]-1
    if PoR_y < 0:
        PoR_y = 1
    elif PoR_y > pyautogui.size()[1]:
        PoR_y = pyautogui.size()[1]-1

    return [PoR_x, PoR_y]

# Update and reload the settings.csv file
def update_settings_file(updated_settings_data, settings_filename):

#   json.dump(d, skipkeys=False, ensure_ascii=True, check_circular=True, allow_nan=True, cls=None, indent=None, separators=None)
    with open(settings_filename, 'w') as f:
        json.dump(updated_settings_data, f, indent=4, separators=[",",":"])
        f.truncate()
    with open(settings_filename) as f:
        new_settings = json.load(f)


    # load in data from updated file
    return new_settings

def main():
    # get possible options for video input
    video_options = accessCameras.gather_JSON_data("cameraOptions.json")
    video_options = accessCameras.gather_JSON_data(sys.argv[1])

    # prompt user to select video input source/option
    selected_video = accessCameras.user_camera_selection(video_options)

    # open video stream and select chosen parameters
    cap = accessCameras.open_video(selected_video)

    # to read from a file
    # cap = cv2.VideoCapture('')

    # gather relevant settings from json file
    settings_filename = "settings.json"
    with open(settings_filename) as f:
        settings = json.load(f)
    # settings["gaze_tracking"]["gaze_coeffs"]["L_e_x_coeffs"] = [1378.339393693612, -17093.656457305948, 762252.1576749284, -39545.52339060272]
    # settings["gaze_tracking"]["gaze_coeffs"]["L_e_y_coeffs"] = [254.35366726961718, 491.4498447713301, 8475.083279653383, 151185.04019600785]
    # settings["gaze_tracking"]["gaze_coeffs"]["R_e_x_coeffs"] = [1158.6825847368618, -14537.23961865338, 713435.573993977, -93492.11484048546]
    # settings["gaze_tracking"]["gaze_coeffs"]["R_e_y_coeffs"] = [413.5255913011102, 395.53030702162584, 6026.691494633872, 234120.15579100768]



    general = settings["general"]
    live_debug = settings["live_debug"]
    gaze_tracking = settings["gaze_tracking"]
    hardware_setup = settings["hardware_setup"]
    face_detector_parameters = gaze_tracking["face_detector_parameters"]
    gaze_calibration = settings["gaze_calibration"]

    # create folder to store all recorded data
    data_folder = general["saved_data_folder_name"] + ("_".join(time.ctime(time.time()).split()[1:4])).replace(':','_') + "_" + general["folder_tag"]
    os.mkdir(data_folder)
    print(data_folder)

    # make copy of settings file if saving any data
    if general["save_settings"]:
        original = "settings.json"
        target = os.path.join(data_folder, "settings.json")
        shutil.copyfile(original, target)

    # if gaze_tracking["save_eye_imgs"] or gaze_calibration["save_eye_imgs"]:


    # initialise face detector 
    face_detector = mp_mod.FaceMeshDetector(
                        data_folder = data_folder,
                        staticMode = face_detector_parameters["staticMode"], 
                        maxFaces = face_detector_parameters["maxFaces"], 
                        minDetectionCon = face_detector_parameters["minDetectionCon"], 
                        minTrackCon = face_detector_parameters["minTrackCon"]
                        )

    if general["live_debug"]:
        print("Start: Live Debug")
        run_live_debug(cap, settings, face_detector, data_folder)
        print("End: Live Debug")

    if general["run_positioning_feedback"]:
        print("Start: Positioning Feedback")
        run_camera_positioning_feedback(cap, settings, face_detector)
        print("End: Positioning Feedback")

    if general["update_focal_length"]:
        print("Start: Update Focal Length")
        updated_settings = update_focal_length(cap, settings, face_detector)
        update_settings_file(updated_settings, settings_filename)
        print("End: Update Focal Length")

    if general["calibrate_camera"]:
        print("Start: Camera Calibration")
        calibrate_camera(cap, settings, face_detector, data_folder)
        print("End: Camera Calibration")

    if general["calibrate_gaze"]:
        print("Start: Gaze Calibration")
        updated_settings = calibrate_gaze(cap, settings, face_detector, data_folder)
        update_settings_file(updated_settings, settings_filename)
        print("End: Gaze Calibration")
    
    if general["run_gaze_tracking"]:
        print("Start: Gaze Tracking")
        # TODO: debugging
            # context: when running gaze tracking after doing calibration
            # error: QObject::startTimer: Timers can only be used with threads started with QThread
            # idea: Qobject as in QTcam??? Wrong- QThread is seperate.
            # Steps to fix
                # 1. run gaze tracking in isolation first 
                    # i. make section in settings for calibration
                    # ii. save default calib data into settings
                    # iii. make calib able to save into settings json and make program reload it afterwards
                # 2. if that works, then try commenting all out and adding in lines slowly to isolate break point

            # https://stackoverflow.com/questions/27131294/error-qobjectstarttimer-qtimer-can-only-be-used-with-threads-started-with-qt
            # Starting a timer with no event loop running
            # Starting/stopping a timer from a thread other than the one that created the timer
            # Failing to set the parent property of a widget, leading to problems with the order of destruction
            # SOLUTION: was caused by the named windows displaying the eye images not being destroyed

        run_gaze_tracking(cap, settings, face_detector, data_folder)
        print("End: Gaze Tracking")

    # make copy of (possibly updated) settings file if saving any data
    if general["save_settings"]:
        original = "settings.json"
        target = os.path.join(data_folder, "updated_settings.json")
        shutil.copyfile(original, target)

    accessCameras.close_video(cap)
    cv2.destroyAllWindows()

main()


