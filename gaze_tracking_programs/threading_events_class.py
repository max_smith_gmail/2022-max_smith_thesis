import threading

class threading_events():
    def __init__(self):

        # set by the recording thread to notify the calibration program that its ready to begin
        self.record_ready_event = threading.Event()

        # set by the calibration thread to notify the recording thread to stop recording
        self.finished_calib_event = threading.Event()

        # fixation coords are updated by the calib thread and read by the recording thread for csv file
        self.global_fixation_coords = [0,0]

        # set by the calibration thread to notify the recording thread that the fixation point has reached a new destination
        self.fixation_event = threading.Event() 