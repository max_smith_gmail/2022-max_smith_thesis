import csv
from fileinput import close
from turtle import color
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import numpy as np
import os
from ast import literal_eval
from scipy.optimize import curve_fit
import json

# heading names
# """
# 'Time', 'head_orientation_xyz', 'head_dist_xyz',
# 'L_box_center', 'L_pupil_cartesian', 'L_pupil_polar', 'L_glints_cartesian', 'L_glints_polar',
# 'R_box_center', 'R_pupil_cartesian', 'R_pupil_polar', 'R_glints_cartesian', 'R_glints_polar',
# 'glint_ave_eye_dist', 'Fixation_coords'
# """
"""
'Time', 'head_orientation_xyz', 'head_dist_xyz',
'L_pupil', 'L_glints', 'L_eye_corners',
'R_pupil', 'R_glints', 'R_eye_corners',
'Fixation_coords'
"""


def gather_and_normalize_data(data_folder):
    
    settings_filename = data_folder + "/settings.json"
    with open(settings_filename) as f:
        settings = json.load(f)
    gaze_calib_settings = settings["gaze_calibration"]

    # pd.set_option('display.max_columns', None,'display.max_rows', None)
    df = pd.read_csv(os.path.join(data_folder, "eye_pos.csv"))

    # replace blank entires with not a number values.
    df.replace('',np.nan, inplace=True)

    # check to see if the glints col was empty/ has a nan value now
    no_glints = False
    if (pd.isnull(df["L_glints"][0])):
        no_glints = True
        # drop glint cols if they contain nan values (e.g. if no glints were detected col will be empty)
        df.drop(['L_glints', 'R_glints'], axis=1, inplace=True)
 
    # drop any rows whcih still have nan values (now just removing data points which are not at a fixation)
    df.dropna(inplace=True, axis=0)
    

    # convert data to correct type format
    df['Time'].index = pd.to_timedelta(df['Time'].index, unit='seconds')

    # convert all array cols from strings to arrays except for the first which is time
    for row_str in df.columns[1:]:
        df[row_str] = df[row_str].apply(literal_eval).apply(lambda x: np.array(x))

    # note the start and end of the fixations (make cols for storage)- needed later
    df[['fixation_x','fixation_y']] = pd.DataFrame(df.Fixation_coords.tolist(), index= df.index)

    # renumber the rows
    df.reset_index(inplace=True)

    # extract xy positions of the pupil and both glints for each eye, plus the fixation points (where they're looking on the screen)
    L_pupil = pd.DataFrame(df["L_pupil"].to_list(), columns=['L_eye_pupil_x', 'L_eye_pupil_y']).to_numpy()
    R_pupil = pd.DataFrame(df["R_pupil"].to_list(), columns=['R_eye_pupil_x', 'R_eye_pupil_y']).to_numpy()
    L_eye_corners = pd.DataFrame(df["L_eye_corners"].to_list(), columns=['L_eye_L_eye_corner_x', 'L_eye_L_eye_corner_y','L_eye_R_eye_corner_x', 'L_eye_R_eye_corner_y']).to_numpy()
    R_eye_corners = pd.DataFrame(df["R_eye_corners"].to_list(), columns=['R_eye_L_eye_corner_x', 'R_eye_L_eye_corner_y','R_eye_R_eye_corner_x', 'R_eye_R_eye_corner_y']).to_numpy()
    fixation_points = pd.DataFrame(df["Fixation_coords"].to_list(), columns=['fixation_x', 'fixation_y']).to_numpy()
    
    # 1. find unique values in fixation points/changeover indicies
    df['fixation_starts'] = ~(df.duplicated(subset=['fixation_x', 'fixation_y'],keep='first')) # true for all of the first duplicate occurances
    df['fixation_ends'] = ~(df.duplicated(subset=['fixation_x', 'fixation_y'],keep='last')) # true for all the last duplicate occurances

    # 2. find times of those cells
    start_times = df[df['fixation_starts'] == True]['Time']
    end_times = df[df['fixation_ends'] == True]['Time']

    # get cuttoff times for data
    fixation_trim_start_s = gaze_calib_settings["fixation_trim_start_ms"] / 1000
    fixation_trim_end_s = gaze_calib_settings["fixation_trim_end_ms"] / 1000
    print(fixation_trim_start_s)
    print(fixation_trim_end_s)

    # remove all rows which within the first or last 250ms of a fixation.
    for index, row in df.iterrows():

        # find fixation start event closest to current row, then remove row if 0-250ms AFTER the start of a fixation
        new_fix_closest_time_val = start_times.iloc[(start_times-row['Time']).abs().argmin()]
        if ((row['Time'] - new_fix_closest_time_val) < fixation_trim_start_s and (row['Time'] - new_fix_closest_time_val) > 0):
            print("drop index {} (T = {}), too close to start of fixation at t={}. diff = {}".format(index, row['Time'], new_fix_closest_time_val, row['Time'] - new_fix_closest_time_val))
            df.drop(index, inplace=True)

        # find fixation end event closest to the current row, then remove row if 0-250ms BEFORE the end of a fixation
        end_fix_closest_time_val = end_times.iloc[(end_times-row['Time']).abs().argmin()]
        if ((end_fix_closest_time_val - row['Time']) < fixation_trim_end_s and (end_fix_closest_time_val - row['Time']) > 0):
            print("drop index {} (T = {}), too close to end of fixation at t={}. diff = {}".format(index, row['Time'], end_fix_closest_time_val, end_fix_closest_time_val - row['Time']))
            df.drop(index, inplace=True)
    


    # get the average of the normalised glint2pupil vectors for each eye
    [eye_corners_V_lx, eye_corners_V_ly] = get_normalized_vector(L_pupil, L_eye_corners)
    [eye_corners_V_rx, eye_corners_V_ry] = get_normalized_vector(R_pupil, R_eye_corners)
    eye_corner_vectors = [eye_corners_V_lx, eye_corners_V_ly, eye_corners_V_rx, eye_corners_V_ry]
    vectors = [None, eye_corner_vectors]

    # if there are glints, repeat extraction and calibration with glint data
    if no_glints == False:
        L_glints = pd.DataFrame(df["L_glints"].to_list(), columns=['L_eye_L_glint_x', 'L_eye_L_glint_y','L_eye_R_glint_x', 'L_eye_R_glint_y']).to_numpy()
        R_glints = pd.DataFrame(df["R_glints"].to_list(), columns=['R_eye_L_glint_x', 'R_eye_L_glint_y','R_eye_R_glint_x', 'R_eye_R_glint_y']).to_numpy()
        [glint_V_lx, glint_V_ly] = get_normalized_vector(L_pupil, L_glints)
        [glint_V_rx, glint_V_ry] = get_normalized_vector(R_pupil, R_glints)
        glint_vectors = [glint_V_lx, glint_V_ly, glint_V_rx, glint_V_ry]
        vectors[0] = glint_vectors
    
    return vectors, fixation_points

def get_normalized_vector(pupil_coords, feature_coords):

    # TODO: debug/test
    # average the feature location
    ave_glint_x = (feature_coords[:,0] + feature_coords[:,2])/2
    ave_glint_y = (feature_coords[:,1] + feature_coords[:,3])/2

    # dist between feature points
    feat_dist_x = np.absolute((feature_coords[:,0] - feature_coords[:,2]))
    feat_dist_y = np.absolute((feature_coords[:,1] - feature_coords[:,3]))
    feat_dist = np.sqrt(np.square(feat_dist_x) + np.square(feat_dist_y))

    # determine vectors from ave feature spot to pupil in x and y 
    ave_vect_x = pupil_coords[:,0] - ave_glint_x
    ave_vect_y = pupil_coords[:,1] - ave_glint_y

    # normalised average feature vectors
    norm_vect_x = ave_vect_x/feat_dist
    norm_vect_y = ave_vect_y/feat_dist

    return [norm_vect_x, norm_vect_y]

def get_prediction_x(glint_vector_xy, C0, C1, C2, C3):

    # extract normalized glint vectors
    V_x, V_y = np.vsplit(glint_vector_xy, 2)

    # calculate estimate of screen coordinate
    PoR_x = C0 + C1*V_x + C2*(V_x**3) + C3*(V_y**2)
    PoR_x = np.array(PoR_x.flatten()).astype(float)
    return PoR_x

def get_prediction_y(glint_vector_xy, C0, C1, C2, C3):

    # extract normalized glint vectors
    V_x, V_y = np.vsplit(glint_vector_xy, 2)

    # calculate estimate of screen coordinate
    PoR_y = C0 + C1*V_x + C2*V_y + C3*(V_x**2)*V_y
    PoR_y = np.array(PoR_y.flatten()).astype(float)
    return PoR_y

def optimise_coeffs2data(V_x, V_y, fixation_coords, guess_coeffs_x=None, guess_coeffs_y=None):

    # NOTES:
    # documentation @ https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
    # x axis is the independent variable i.e. the glint vector (in x and y)
    # y axis is the dependent variable i.e. the resultant coordinate on the screen
    # Coefficients are defined in the given evaluation function (get_prediction_...). 
            # their optimised values are returned by the curve fit function after completion
    # Regression method can be changed using method=‘lm’ or ‘trf’ or ‘dogbox’ 
            # see https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html#scipy.optimize.least_squares
    # All data must be float64 (for valid results) with No Nan values
    # Loss function can be changed using loss = 
            # ‘linear’ (default): rho(z) = z. Gives a standard least-squares problem.
            # ‘soft_l1’ : rho(z) = 2 * ((1 + z)**0.5 - 1). The smooth approximation of l1 (absolute value) loss. Usually a good choice for robust least squares.
            # ‘huber’ : rho(z) = z if z <= 1 else 2*z**0.5 - 1. Works similarly to ‘soft_l1’.
            # ‘cauchy’ : rho(z) = ln(1 + z). Severely weakens outliers influence, but may cause difficulties in optimization process.
            # ‘arctan’ : rho(z) = arctan(z). Limits a maximum loss on a single residual, has properties similar to ‘cauchy’.
    # accuracy/final loss can also be returned if needed
    # end condition can be changed for closer (or further but faster) fitting
    # output must be n*1, not 1*n e.g. column not row
    
    input_data = np.vstack([V_x, V_y])
    output_data_x = fixation_coords[:,0].reshape(-1,1).flatten()
    output_data_y = fixation_coords[:,1].reshape(-1,1).flatten()
    
    optimised_coeffs_x, coeff_covariance_x = curve_fit(get_prediction_x, input_data, output_data_x, p0=guess_coeffs_x)
    optimised_coeffs_y, coeff_covariance_y = curve_fit(get_prediction_y, input_data, output_data_y, p0=guess_coeffs_y)

    return (optimised_coeffs_x, optimised_coeffs_y)

def plot2D(given_title, V_x, V_y, coeffs_x, coeffs_y, fixations):

    # calculate estimate of screen coordinate
    calc_PoR_x = (coeffs_x[0] + coeffs_x[1]*V_x + coeffs_x[2]*(V_x**3) + coeffs_x[3]*(V_y**2))
    calc_PoR_y = (coeffs_y[0] + coeffs_y[1]*V_x + coeffs_y[2]*V_y + coeffs_y[3]*(V_x**2)*V_y)

    # format data into arrays (n,) instead of columns/matricies (1,n)
    V_x = V_x.reshape(-1,1).flatten()
    V_y = V_y.reshape(-1,1).flatten()
    calc_PoR_x = calc_PoR_x.reshape(-1,1).flatten()
    calc_PoR_y = calc_PoR_y.reshape(-1,1).flatten()

    fixations_x = fixations[:,0].reshape(-1,1).flatten()
    fixations_y = fixations[:,1].reshape(-1,1).flatten()


    # find the first instances of each fixation (so we know where to seperate the data for graphing)
    unique, indices = np.unique(fixations, return_index=True, axis=0)
    fixation_slice_points = np.sort(indices)

    # zip the data so that the all data can be grouped for each fixation, then round to nearest pixel
    results_mat = np.column_stack((calc_PoR_x, calc_PoR_y, fixations_x, fixations_y))
    results_mat = [np.round(i).astype(int) for i in results_mat]

    # split the results by fixation
    results_mat = np.split(results_mat, fixation_slice_points)

    # gather list of colours
    colour_list = []
    for key, col_val in mcolors.TABLEAU_COLORS.items():
        colour_list.append(mcolors.TABLEAU_COLORS[key])


    # plot (x,y) of gaze estimations vs fixations
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    col_idx = 0
    for fixation in results_mat:
        fixation = np.transpose(fixation)
        ax1.scatter(fixation[0], fixation[1], color=colour_list[col_idx])
        ax1.scatter(fixation[2], fixation[3], color=colour_list[col_idx], marker='x')
        col_idx += 1

    plt.title(given_title)
    plt.xlabel("x axis of screen (pixels)")
    plt.ylabel("y axis of screen (pixels)")
    # plt.legend(loc='upper left')
    plt.show()

def plot3D(eye_name, V_x, V_y, coeffs_x, coeffs_y, fixations):

    # calculate estimate of screen coordinate
    calc_PoR_x = (coeffs_x[0] + coeffs_x[1]*V_x + coeffs_x[2]*(V_x**3) + coeffs_x[3]*(V_y**2))
    calc_PoR_y = (coeffs_y[0] + coeffs_y[1]*V_x + coeffs_y[2]*V_y + coeffs_y[3]*(V_x**2)*V_y)

    # format data into arrays (n,) instead of columns/matricies (1,n)
    V_x = V_x.reshape(-1,1).flatten()
    V_y = V_y.reshape(-1,1).flatten()

    calc_PoR_x = calc_PoR_x.reshape(-1,1).flatten()
    calc_PoR_y = calc_PoR_y.reshape(-1,1).flatten()

    fixations_x = fixations[:,0].reshape(-1,1).flatten()
    fixations_y = fixations[:,1].reshape(-1,1).flatten()

    error_x = calc_PoR_x - fixations_x
    error_y = calc_PoR_y - fixations_y

    # graph axes definititon
    # x axis = V_x value
    # y_axis = V_y value
    # z_axis = calc_PoR value + fixations

    fig = plt.figure()
    ax_x= fig.add_subplot(2,2,1,projection='3d')
    ax_y = fig.add_subplot(2,2,2,projection='3d')
    ax_x_err= fig.add_subplot(2,2,3,projection='3d')
    ax_y_err = fig.add_subplot(2,2,4,projection='3d')

    calc_PoR_line = ax_x.scatter(V_x, V_y, calc_PoR_x, marker='o', label="Estimated_PoR")
    fixation_line = ax_x.scatter(V_x, V_y, fixations_x, marker='^', label="Actual PoR")
    ax_x.set_title(eye_name + ": X dimension")
    ax_x.set_xlabel('Glint Vector (x)')
    ax_x.set_ylabel('Glint Vector (y)')
    ax_x.set_zlabel('PoR pixel in x screen axis')
    ax_x.legend(handles=[calc_PoR_line, fixation_line])

    calc_PoR_line = ax_y.scatter(V_x, V_y, calc_PoR_y, marker='o')
    fixation_line = ax_y.scatter(V_x, V_y, fixations_y, marker='^')
    ax_y.set_title(eye_name + ": Y dimension")
    ax_y.set_xlabel('Glint Vector (x)')
    ax_y.set_ylabel('Glint Vector (y)')
    ax_y.set_zlabel('PoR pixel in y screen axis')
    ax_x.legend(handles=[calc_PoR_line, fixation_line])

    error_line = ax_x_err.scatter(V_x, V_y, error_x, marker='o', label="PoR error in x pixels")
    ax_x_err.set_title(eye_name + ": X dimension")
    ax_x_err.set_xlabel('Glint Vector (x direction)')
    ax_x_err.set_ylabel('Glint Vector (y direction)')
    ax_x_err.set_zlabel('Estimation error (pixels)')
    ax_x_err.legend(handles=[error_line])

    error_line = ax_y_err.scatter(V_x, V_y, error_y, marker='o', label="PoR error in y pixels")
    ax_y_err.set_title(eye_name + ": X dimension")
    ax_y_err.set_xlabel('Glint Vector (x direction)')
    ax_y_err.set_ylabel('Glint Vector (y direction)')
    ax_y_err.set_zlabel('Estimation error (pixels)')
    ax_y_err.legend(handles=[error_line])

def main():
    vectors, fixation_points = gather_and_normalize_data("Recorded_data/Sep_16_13-31-41_basic_debugging/")
    
    if vectors[0] == None:
        no_glints = True
    g_vectors = vectors[0]
    ec_vectors = vectors[1]
    
    # (g_left_eye_x_coeffs, g_left_eye_y_coeffs) = optimise_coeffs2data(g_V_lx, g_V_ly, fixation_points)
    # (g_right_eye_x_coeffs, g_right_eye_y_coeffs) = optimise_coeffs2data(g_V_rx, g_V_ry, fixation_points)
    
    # print("Glint Coefficients: left_x, left_y, right_x, right_y")
    # print(g_left_eye_x_coeffs)
    # print(g_left_eye_y_coeffs)
    # print(g_right_eye_x_coeffs)
    # print(g_right_eye_y_coeffs)
    # print()

    # plot3D("GLINTS: Left Eye", g_V_lx, g_V_ly, g_left_eye_x_coeffs, g_left_eye_y_coeffs, fixation_points)
    # plot3D("GLINTS: Right Eye", g_V_rx, g_V_ry, g_right_eye_x_coeffs, g_right_eye_y_coeffs, fixation_points)

    (ec_left_eye_x_coeffs, ec_left_eye_y_coeffs) = optimise_coeffs2data(ec_vectors[0], ec_vectors[1], fixation_points)
    (ec_right_eye_x_coeffs, ec_right_eye_y_coeffs) = optimise_coeffs2data(ec_vectors[2], ec_vectors[3], fixation_points)
    
    # print("Eye Corner Coefficients: left_x, left_y, right_x, right_y")
    # print(ec_left_eye_x_coeffs)
    # print(ec_left_eye_y_coeffs)
    # print(ec_right_eye_x_coeffs)
    # print(ec_right_eye_y_coeffs)
    # print()


    # plot3D("EYE-CORNERS: Left Eye", ec_vectors[0], ec_vectors[1], ec_left_eye_x_coeffs, ec_left_eye_y_coeffs, fixation_points)
    # plot3D("EYE-CORNERS: Right Eye", ec_vectors[2], ec_vectors[3], ec_right_eye_x_coeffs, ec_right_eye_y_coeffs, fixation_points)
    plot2D("Estimated gaze point on screen versus calibration data (left eye, eye corners method)", ec_vectors[0], ec_vectors[1], ec_left_eye_x_coeffs, ec_left_eye_y_coeffs, fixation_points)
    plot2D("Estimated gaze point on screen versus calibration data (right eye, eye corners method)", ec_vectors[2], ec_vectors[3], ec_right_eye_x_coeffs, ec_right_eye_y_coeffs, fixation_points)
    plt.show()

# main()
