from cmath import pi, tan
from logging import raiseExceptions
from tkinter import S
import cv2
import threading_events_class
import mediapipe as mp
import time
import os
import numpy as np
from protobuf_to_dict import protobuf_to_dict
import math
import matplotlib.pyplot as plt
from threading_events_class import threading_events
from shutil import rmtree 
from facemesh_classes import *
from skimage import transform
from skimage.feature import  blob_log
from itertools import combinations
from math import hypot
from time import sleep
from math import atan
from statistics import mean

# TODO: clean up imports


# # CONVENTIONS
# # all sets of four coordinates are in the order left, right, top, bottom
# # all pixel coordinates are stored as integers
# # all left vs right notations are from the user's perspective (opposite of picture)

# # ROTATION
# # x looking up or down
# # y looking left or right
# # z camera orientation/turn, are they upsidedown or sideways?

# TODO remove dist function, use pixel coords thing instead
def distance(p1,p2):
    """Euclidean distance between two points."""
    x1,y1 = p1
    x2,y2 = p2
    return hypot(x2 - x1, y2 - y1)


# class to contain all face and eye detection functions
class FaceMeshDetector():
    # creates the mesh detector object
    def __init__(self, data_folder, staticMode=False, maxFaces=1, minDetectionCon=0.5, minTrackCon=0.5):

        # mediapipe tools
        self.mpDraw = mp.solutions.drawing_utils
        self.mp_face_mesh = mp.solutions.face_mesh

        # facemesh and drawing parameters
        self.faceMesh = self.mp_face_mesh.FaceMesh(
            max_num_faces = 1, 
            refine_landmarks=True, 
            min_detection_confidence=0.5, 
            min_tracking_confidence=0.5
            )
        self.draw_dot_spec = self.mpDraw.DrawingSpec(thickness=1, circle_radius=1)

        # detected head parameters and information
        self.landmarks = []
        self.im_shape = []
        self.pose = []
        self.eyes = []

        # head positioning message, ["instruction code", "message"]
        self.positioning_feedback_message = ["",""]

        # store main folder for use later
        self.data_folder = data_folder

        # create folder for eye images. If no images are saved this will be deleted in the destructor function
        self.main_eye_img_folder_name = self.data_folder + "/" + "Eye_images/"
        self.fixation_eye_img_folder_name = "misc"
        self.eye_img_save_path = os.path.join(self.main_eye_img_folder_name, self.fixation_eye_img_folder_name)

        # adjusting permissions for folder to make accessible to user
        oldmask = os.umask(000)
        os.mkdir(self.main_eye_img_folder_name, mode=0o777)
        os.mkdir(self.eye_img_save_path, mode=0o777)
        os.mkdir(os.path.join(self.eye_img_save_path, "right_eye"), mode=0o777)
        os.mkdir(os.path.join(self.eye_img_save_path, "left_eye"), mode=0o777)
        os.umask(oldmask)

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Folder and data saving functions
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    # create folder for new fixation eye images and update path 
    def new_eye_fixation_img_folder(self, fixation_coord_str):
        # update folder names
        self.fixation_eye_img_folder_name = fixation_coord_str
        self.eye_img_save_path = os.path.join(self.main_eye_img_folder_name, self.fixation_eye_img_folder_name)

        # make new folders
        oldmask = os.umask(000)
        os.mkdir(os.path.join(self.main_eye_img_folder_name, self.fixation_eye_img_folder_name), mode=0o777)
        os.mkdir(os.path.join(self.eye_img_save_path, "right_eye"), mode=0o777)
        os.mkdir(os.path.join(self.eye_img_save_path, "left_eye"), mode=0o777)
        os.umask(oldmask)

    # hardcoded header for csv file
    def get_eye_data_header(self):
        # return ['Time', 'head_orientation_xyz', 'head_dist_xyz',
        #         'L_box_center', 'L_pupil_cartesian', 'L_pupil_polar', 'L_glints_cartesian', 'L_glints_polar',
        #         'R_box_center', 'R_pupil_cartesian', 'R_pupil_polar', 'R_glints_cartesian', 'R_glints_polar',
        #         'glint_ave_eye_dist', 'Fixation_coords']
        return ['Time', 'head_orientation_xyz', 'head_dist_xyz',
                'L_pupil', 'L_glints', 'L_eye_corners',
                'R_pupil', 'R_glints', 'R_eye_corners',
                'Fixation_coords']
    
    # individual entry for set of eye data
    def get_eye_data(self):
        if self.eyes == []:
            print("No eye objects found when writing to CSV file")
            return ""

        # pupil and glint cartesian coordinates should be in absolute terms, not relative to eye-subimage
        # polar coordinates are relative to the center of the image box (L_box_center, R_box_center)
        ret_str = [
            "{:.2f}".format(self.frame_time_annotate), # time
            self.pose.get_orientation_xyz_str(dp=2), # head orientation around x,y,z
            self.pose.get_dist_xyz_str(dp=2), # head distance in x,y,z

            "[{:.2f}, {:.2f}]".format(self.eyes.left.image_box.absolute_POI_coords.x, self.eyes.left.image_box.absolute_POI_coords.y), # Left pupil coords (x,y)
            self.eyes.left.get_glint_pair_str(dp=2), # Left glint pair coords (left_x, left_y, right_x, right_y)
            self.eyes.left.get_eye_corner_pair_str(dp=2), # Left eye corner pair (left_x, left_y, right_x, right_y)

            "[{:.2f}, {:.2f}]".format(self.eyes.right.image_box.absolute_POI_coords.x, self.eyes.right.image_box.absolute_POI_coords.y), # Left pupil coords (x,y)
            self.eyes.right.get_glint_pair_str(dp=2), # Right glint pair coords (left_x, left_y, right_x, right_y)
            self.eyes.right.get_eye_corner_pair_str(dp=2), # Left eye corner pair (left_x, left_y, right_x, right_y)

            ]
            # "[{:.2f}, {:.2f}]".format(self.eyes.left.centre.x, self.eyes.left.centre.y), # Left box center
            # "[{:.2f}, {:.2f}]".format(self.eyes.right.centre.x, self.eyes.right.centre.y), # Right box center
            # "[{:.2f}, {:.2f}]".format(self.eyes.left.image_box.relative_angle2POI * 180/pi, self.eyes.left.image_box.dist2POI), # Left pupil polar
            # "[{:.2f}, {:.2f}]".format(self.eyes.right.image_box.relative_angle2POI * 180/pi, self.eyes.right.image_box.dist2POI), # Right pupil polar

            # self.eyes.calc_glint_ave_eye_dist(dp=2) #glint average distance in pixels
        return ret_str

    def __del__(self):
        # if the eye images folder is present but has no fixation folders other than misc, delete it
        if "Eye_images" in os.listdir(self.data_folder):
            if (len(os.listdir(self.main_eye_img_folder_name)) <= 1 and 
                len(os.listdir(os.path.join(self.main_eye_img_folder_name,"misc", "left_eye"))) == 0):
                rmtree(self.main_eye_img_folder_name)
    
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Mathematical and image processing functions
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    # runs all analysis for the current frame then return the annotated version
    def process_frame(self, frame, landmark_annotate, eye_annotate, eye_corner_vectors_annotate, pose_annotate, show_eyes, save_eyes, time_count, thread_events):
        self.frame_time_start = time.time()
        self.frame_time_annotate = time_count
        self.show_eyes = show_eyes
        self.thread_events = thread_events
        # improves performance: https://www.youtube.com/watch?v=-toNMaS4SeQ
        frame.flags.writeable = True 

        # run mediapipe on frame
        self.results = self.faceMesh.process(frame)

        # Check if success and save landmaks
        if self.results.multi_face_landmarks is None:
            print("Could not identify face in frame. skipping")
            return None
        else:
            self.face = self.results.multi_face_landmarks[0]
            self.landmarks = protobuf_to_dict(self.results.multi_face_landmarks[0])['landmark']

        # store shape of frame
        self.im_height = np.shape(frame)[0]
        self.im_width = np.shape(frame)[1]
        # make a copy of image for annotations #TODO: may be redundant! #NOTE performance
        self.annotated_image = frame.copy()

        # find and isolate eyes, determine head border and pose
        self.define_eyes()
        self.get_head_border()
        self.calc_head_pose()
        (self.left_eye_img, self.right_eye_img)= self.get_eye_imgs(frame, transform=False)

        if eye_annotate:
            self.annotate_eyes()

        if eye_corner_vectors_annotate:
            self.annotate_eye_corner_vectors()

        if pose_annotate:
            self.annotate_pose()
        # show eye images if needed
        if show_eyes:
            self.show_eye_imgs()

        # save eye images if needed
        if save_eyes:
            self.save_eye_imgs()

        # basic annotations if needed
        if landmark_annotate:
            self.mpDraw.draw_landmarks( image=self.annotated_image,
                                        landmark_list=self.face,
                                        connections=self.mp_face_mesh.FACEMESH_CONTOURS,
                                        landmark_drawing_spec=self.draw_dot_spec,
                                        connection_drawing_spec=self.draw_dot_spec)

        if self.frame_time_annotate is not None:
            cv2.putText(self.annotated_image, "T={:.2f} (s)".format(self.frame_time_annotate), (self.im_width- 200, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

        return self.annotated_image 
    
    def define_head_margins(self):

        self.inner_margin_box = angled_box(
            int(self.im_width/2 + self.inner_margin[0]/2),
            int(self.im_width/2 - self.inner_margin[0]/2),
            int(self.im_height/2 - self.inner_margin[1]/2),
            int(self.im_height/2 + self.inner_margin[1]/2),
            )

        self.outer_margin_box = angled_box(
            int(self.im_width/2 + self.outer_margin[0]/2),
            int(self.im_width/2 - self.outer_margin[0]/2),
            int(self.im_height/2 - self.outer_margin[1]/2),
            int(self.im_height/2 + self.outer_margin[1]/2),
            )

    def get_head_border(self):
        # initially define a set of landmarks as the face border (estimates), then search through and replace them as larger borders are found
        face_border_landmarks = landmark_set(454,234,10,152, self.landmarks, self.im_width, self.im_height,)
        
        landmark_id = 0
        for landmark in self.landmarks:
            (x_pix_cur, y_pix_cur) = (int(landmark['x'] * self.im_width), int(landmark['y'] * self.im_height))
            label = ''

            # NOTE: left and right is always treated from the user's POV (so left = right side of screen)
            if x_pix_cur > face_border_landmarks.left_pixel_coords.x:
                label = 'l'
            elif x_pix_cur < face_border_landmarks.right_pixel_coords.x:
                label = 'r'

            # NOTE: landmark could be a both left/right border and a top/bottom e.g. top corner landmark
            if y_pix_cur > face_border_landmarks.top_pixel_coords.y:
                label += 't'
            elif y_pix_cur < face_border_landmarks.bottom_pixel_coords.y:
                label += 'b'

            if label != '':
                face_border_landmarks.replace_landmark(label, landmark_id, landmark)
            landmark_id += 1
        
        # Store landmarks that make up face border
        self.face_border_landmarks = face_border_landmarks

        # create a box to bound the face
        self.face_border_box = angled_box(
            face_border_landmarks.left_pixel_coords,
            face_border_landmarks.right_pixel_coords,
            face_border_landmarks.top_pixel_coords,
            face_border_landmarks.bottom_pixel_coords)

    def define_eyes(self):
        #                           left, right, top, bottom, landmarks, im_width, im_height):
        left_eye_bounds = landmark_set(359, 463, 257, 253, self.landmarks, self.im_width, self.im_height)
        left_eye_iris = landmark_set(474, 476, 475, 477, self.landmarks, self.im_width, self.im_height)
        left_eye_pupil = pixel_coords(self.landmarks[473]['x'] * self.im_width, self.landmarks[473]['y'] * self.im_height, self.landmarks[473]['z'] * self.im_width)
        left_eye_corner_l = pixel_coords(self.landmarks[263]['x'] * self.im_width, self.landmarks[263]['y'] * self.im_height)
        left_eye_corner_r = pixel_coords(self.landmarks[362]['x'] * self.im_width, self.landmarks[362]['y'] * self.im_height)
        left_eye_corners = [left_eye_corner_l, left_eye_corner_r]
        left_eye = eye(left_eye_bounds, left_eye_iris, left_eye_pupil, left_eye_corners)

        right_eye_bounds = landmark_set(243, 130, 27, 23, self.landmarks, self.im_width, self.im_height)
        right_eye_iris= landmark_set(469, 471, 470, 472, self.landmarks, self.im_width, self.im_height)
        right_eye_pupil = pixel_coords(self.landmarks[468]['x'] * self.im_width, self.landmarks[468]['y'] * self.im_height, self.landmarks[468]['z'] * self.im_width)
        right_eye_corner_l = pixel_coords(self.landmarks[133]['x'] * self.im_width, self.landmarks[133]['y'] * self.im_height)
        right_eye_corner_r = pixel_coords(self.landmarks[33]['x'] * self.im_width, self.landmarks[33]['y'] * self.im_height)
        right_eye_corners = [right_eye_corner_l, right_eye_corner_r]
        right_eye = eye(right_eye_bounds, right_eye_iris, right_eye_pupil, right_eye_corners)
        
        self.eyes = eye_pair(left_eye, right_eye)      

    def calc_head_pose(self):

        # following vid tutorial https://www.youtube.com/watch?v=-toNMaS4SeQ
        # https://github.com/niconielsen32/ComputerVision/blob/master/headPoseEstimation.py#L42
        
        face_2D = []
        face_3D = []
        important_pose_landmark_ids = [1, 33, 61, 199, 263, 291]

        # collect required landmark data
        for id in important_pose_landmark_ids:
            lm = self.landmarks[id]

            if id == 1:
                nose_2D = (lm['x'] * self.im_width, lm['y'] * self.im_height)
                nose_3D = (lm['x'] * self.im_width, lm['y'] * self.im_height, lm['z'] * 3000)
                # TODO: figure out why 3000 then #DEFINE

            x, y = int(lm['x'] * self.im_width), int(lm['y'] * self.im_height)

            # gather required landmark coordinates
            face_2D.append([x, y])
            face_3D.append([x, y, lm['z']])      

        # convert data to np arrays
        face_2D = np.array(face_2D, dtype = np.float64)
        face_3D = np.array(face_3D, dtype = np.float64)
            
        # TODO: get camera focal length (when camera is in focus with better lens)
        focal_len = 1 * self.im_width
        
        # TODO: when calibrating matrix check all values
        cam_matrix = np.array([ [focal_len, 0,          self.im_height/2  ],
                                [0,         focal_len,  self.im_width/2   ],
                                [0,         0,          1                 ]])
        
        # TODO: when calibrating matrix add values
        dist_matrix = np.zeros((4, 1), dtype=np.float64)
        
        # solve PnP. Gives rotation and translation matricies from camera to nose.
        # will need to also create transformation from the nose to each eye
        success, rot_vec, trans_vec = cv2.solvePnP(face_3D, face_2D, cam_matrix, dist_matrix)

        # Get rotation vector from rotation matrix
        rmat, jac = cv2.Rodrigues(rot_vec)
        
        # Get angles
        angles, mtxR, mtxQ, Qx, Qy, Qz = cv2.RQDecomp3x3(rmat)

        # currently redundant return values
        nose_3D_projection, jacobian = cv2.projectPoints(nose_3D, rot_vec, trans_vec, cam_matrix, dist_matrix)

        # store the beginnning and end of the nose vector for the image plane
        p1 = (int(nose_2D[0]), int(nose_2D[1])) #just int of nose2D
        p2 = (int(nose_2D[0] + angles[1] * 360 * 10) , int(nose_2D[1] - angles[0] * 360 * 10)) # nose2D + angle extended


        # calculate the head tilt since the method above does not account for z axis #TODO use pixel coord class for this
        run = abs(self.eyes.left.bounds.left_pixel_coords.x - self.eyes.right.bounds.right_pixel_coords.x)
        rise =  self.eyes.right.bounds.right_pixel_coords.y - self.eyes.left.bounds.left_pixel_coords.y
        head_tilt_rad = math.atan(rise/run)

        # (angle * 360) = degrees, angle * 360 * pi/180 = angle * 2 * pi = radians
        head_orientation = orientation(angles[0]* 2 * pi, angles[1] * 2 * pi, head_tilt_rad)
        
        # store results in head pose class
        self.pose = head_pose(rot_vec, trans_vec, head_orientation, (p1,p2))

        # just middle of face image
        self.pose.add_centroid(pixel_coords(
            (self.face_border_landmarks.left_pixel_coords.x + self.face_border_landmarks.right_pixel_coords.x)/2,
            (self.face_border_landmarks.top_pixel_coords.y + self.face_border_landmarks.bottom_pixel_coords.y)/2
            ))

        # construct boxes for images of the eyes
        self.eyes.make_image_boxes(self.pose.orientation.z)

    def check_head_positioning(self, frame_head_margins):

        # if the frame has not been processed then head position cannot be checked
        if self.frame == None:
            print("frame not yet processed, invalid")
            return None

        # determine the head positioning
        self.inner_margin = [min(frame_head_margins)[0] * self.im_width/100, min(frame_head_margins)[1] * self.im_height/100] 
        self.outer_margin = [max(frame_head_margins)[0] * self.im_width/100, max(frame_head_margins)[1] * self.im_height/100] 
        self.define_head_margins()
        self.get_head_border()
        
        # shorthand variables
        face = self.face_border_box
        inner_m = self.inner_margin_box
        outer_m = self.outer_margin_box
        
        # reset message, ["instruction code", "message"]
        self.positioning_feedback_message = ["",""]
        
        # if the face is too large or to small, advise user/carer to adjust distance
        if face.width_px < inner_m.width_px:
            self.positioning_feedback_message = ["closer","Move camera closer to user"]

        elif face.width_px > outer_m.width_px:
            self.positioning_feedback_message = ["further","Move camera away from user"]

        else:
            # if scale is correct, then check positioning
            
            if face.img_leftmost_val < outer_m.img_leftmost_val or face.img_rightmost_val < inner_m.img_rightmost_val:
                # if they are too far to the left of the screen (their right) and need need to move to their left
                self.positioning_feedback_message = ["right","Move head right or camera left"]
                
            elif face.img_rightmost_val > outer_m.img_rightmost_val or face.img_leftmost_val > inner_m.img_leftmost_val:
                # if they are too far to the left of the screen (their right) and need need to move to their left (right of the screen). 
                self.positioning_feedback_message = ["left","Move head left or camera right"]

            elif face.img_highest_val > inner_m.img_highest_val or face.img_lowest_val > outer_m.img_lowest_val:
                # if they are too low and need to move up
                # NOTE: positionally high pixels have low values
                self.positioning_feedback_message = ["up","Move head up or camera down"]

            elif face.img_highest_val < outer_m.img_highest_val or face.img_lowest_val < inner_m.img_lowest_val:
                #if they are too high and need to move down
                # NOTE: positionally high pixels have low values
                self.positioning_feedback_message = ["down","Move head down or camera up"]

        # annotate the image for visual feedback
        cv2.drawContours(self.annotated_image, [self.face_border_box.box_points], 0, (0, 100, 255), 2)
        if self.positioning_feedback_message[0] == "":
            margin_colour = (0,255,0)
        else:
            margin_colour = (0,0,255)
            cv2.putText(self.annotated_image, self.positioning_feedback_message[1], (500, 300), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 3)
        cv2.drawContours(self.annotated_image, [self.inner_margin_box.box_points], 0, margin_colour, 2)
        cv2.drawContours(self.annotated_image, [self.outer_margin_box.box_points], 0, margin_colour, 2)

        return self.annotated_image

    def get_glint_pairs(self, glint_detect_params, prev_glint_pairs):

        head_tilt_rad = self.pose.orientation.z

        if prev_glint_pairs == None:
            L_eye_glints = self.find_glints(self.left_eye_img, 'l', glint_detect_params, head_tilt_rad)
            R_eye_glints = self.find_glints(self.right_eye_img, 'r', glint_detect_params, head_tilt_rad)
        else:
            L_eye_glints = self.find_glints(self.left_eye_img, 'l', glint_detect_params, head_tilt_rad, prev_glint_pairs[0])
            R_eye_glints = self.find_glints(self.right_eye_img, 'r', glint_detect_params, head_tilt_rad, prev_glint_pairs[1])

        # if no glints were found, tell the gaze tracker so that the frame can be skipped
        if L_eye_glints is None or R_eye_glints is None:
            return None

        # otherwise, store the glints and return them
        self.eyes.left.add_glint_pair(L_eye_glints)
        self.eyes.right.add_glint_pair(R_eye_glints)


        # TODO: if no glint is found, record NA or something so that the entry is still record.
        return [self.eyes.left.glint_pair, self.eyes.right.glint_pair]
    
    def find_glints(self, img, which_eye, glint_detect_params, head_tilt_rad, prev_glint_pair=None):
        # TODO: maybe use mediapipe pupil location as bounding condition (radius around pupil), test if reliable
        """
            0. crop as corneal glints must be central
            1. threshold image to isolate bright points
            2. filter out any bright objects larger than glint (experiment with this) or smaller than spec
            3. isolate two closest to image center
        """
        
        # crop border area of image to focus in on pupil #TODO: may need to focus in on this or broaden, make part of gaze settings
        [og_h, og_w, og_d] = np.shape(img)
        crop_perc = 0.2
        cropped = img[int(crop_perc*og_h):int((1-crop_perc)*og_h), int(crop_perc*og_w):int((1-crop_perc)*og_w)]
        
        # convert to grayscale and blur to help with noise
        gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)  
        blurred = cv2.GaussianBlur(gray, (5,5),0)

        # gather settings
        threshold_min = glint_detect_params["threshold_min"]
        threshold_max = glint_detect_params["threshold_max"]
        min_sigma = glint_detect_params["min_sigma"]
        max_sigma = glint_detect_params["max_sigma"]
        threshold = glint_detect_params["threshold"]
        exclude_border = glint_detect_params["exclude_border"]

        # detect blob locations using laplace of guassian technique on thresholded image
        # https://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.blob_log
        thresh_img = cv2.threshold(blurred,threshold_min,threshold_max,cv2.THRESH_BINARY)[1]
        blobs = blob_log(thresh_img,min_sigma=min_sigma, max_sigma=max_sigma, threshold=threshold, exclude_border=exclude_border)
        
        # transfer data into np array and swap first two cols so that format is x,y,area
        np_blobs = (np.asarray(blobs)).astype(float)
        np_blobs[:, [1, 0]] = np_blobs[:, [0, 1]]

        # translate to original, non-cropped coordinates
        np_blobs[:,0] += int(crop_perc*og_w)
        np_blobs[:,1] += int(crop_perc*og_h)
        
        # create vectors of booleans for keep track of blobs to keep
        glint_blob_bool_v = np.ones((len(np_blobs)), dtype=bool)

        # gather border cuttoff from settings
        cutoff_border_frac = glint_detect_params["cutoff_border_perc"]/100

        # isolate glint blobs using vector, elimate incorrect one by one
        for i in range(len(np_blobs)):
            x, y, area = np_blobs[i]

            # must have area less than XXX #TODO print out areas of glint blobs for exp
            if area > 5: 
                glint_blob_bool_v[i] = False

            # must be close to the center of image (not in the cutoff border)
            elif (abs(x - og_w/2) > (cutoff_border_frac * og_w/2)) or (abs(y - og_h/2) > (cutoff_border_frac * og_h/2)):
                glint_blob_bool_v[i] = False

        # draw rect to show area cutoff
        if glint_detect_params["display_glints_verbose"]:
            top_left = (int(cutoff_border_frac * og_w/2), int(cutoff_border_frac * og_h/2))
            bottom_right = (int(og_w) - int(cutoff_border_frac * og_w/2), int(og_h) - int(cutoff_border_frac * og_h/2))
            color = (0, 0, 255)
            thickness = 1
            img = cv2.rectangle(img, top_left, bottom_right, color, thickness)

        # isolate successful blobs (potential glints)
        glint_blobs = np_blobs[glint_blob_bool_v]
        elim_blobs = np_blobs[np.logical_not(glint_blob_bool_v)]

        # checking all remaining blob combinations to isolate the glint pair
        indicies = np.arange(0,len(glint_blobs))
        combos = np.array(list(combinations(indicies,2)))
        good_combos_bool_vector = np.ones((len(combos)), dtype=bool)

        tilt_tolerance_deg = glint_detect_params["glint_tilt_error_tolerance_deg"]
        glint_dist_min_px = glint_detect_params["glint_dist_min_px"]
        glint_dist_max_px = glint_detect_params["glint_dist_max_px"]

        for i in range(len(combos)):

            # make blob 1 on the user's left (right on image)
            if glint_blobs[combos[i][0]][0] < glint_blobs[combos[i][1]][0]:
                temp_blob = combos[i][0]
                combos[i][0] = combos[i][1] 
                combos[i][1] = temp_blob

            blob_left = glint_blobs[combos[i][0]]
            blob_right = glint_blobs[combos[i][1]]

            # make angle measurement from user's left to right
            if abs(blob_left[0] - blob_right[0]) == 0:
                glint_tilt_rad = pi/2
            else:
                glint_tilt_rad = atan((blob_left[1] - blob_right[1])/(abs(blob_left[0] - blob_right[0])))

            # if distance is not within range, exclude combo
            dist = distance(blob_left[:2], blob_right[:2])
            if dist < glint_dist_min_px or dist > glint_dist_max_px:
                good_combos_bool_vector[i] = False

            # if angle is incorrect by more than 10 degrees, exclude combo
            elif abs(head_tilt_rad - glint_tilt_rad) > (tilt_tolerance_deg * pi/180):
                good_combos_bool_vector[i] = False

        # isolate valid combinations, hopefully there is only one combination left
        valid_combos = combos[good_combos_bool_vector]

        # return coordinates of pupils in images
        if (len(valid_combos) == 0):
            glints = None

        elif (len(valid_combos) == 1):
            glints = glint_pair(glint_blobs[valid_combos[0][0]], glint_blobs[valid_combos[0][1]])

        else:
            # if there are multiple valid glint options, find the closest to the previous glint. if not provided then closest to img middle
            combo_displacements = np.zeros(len(valid_combos))
            for i in range(len(valid_combos)):

                blob_left = glint_blobs[combos[i][0]]
                blob_right = glint_blobs[combos[i][1]]
                midpoint = (mean([blob_left[0], blob_right[0]]), mean([blob_left[1], blob_right[1]]))

                if prev_glint_pair is None:
                    combo_displacements[i] = distance((0,0),(abs(midpoint[0]- og_w/2),abs(midpoint[1]-og_h/2)))
                    # print("Couldn't isolate glints, and no previous glint provided. Returning closest glint pair to image center.")

                else:
                    prev_midpoint = (   mean([prev_glint_pair.left.abs_x, prev_glint_pair.right.abs_x]), 
                                        mean([prev_glint_pair.left.abs_y, prev_glint_pair.right.abs_y]))
                    combo_displacements[i] = distance((0,0),(abs(midpoint[0]- prev_midpoint[0]), abs(midpoint[1] - prev_midpoint[1])))
                    # print("Couldn't isolate glints, returning closest glint pair to previous glint")
            
            best_combo_idx = np.argmin(combo_displacements)
            glints = glint_pair(glint_blobs[valid_combos[best_combo_idx][0]], glint_blobs[valid_combos[best_combo_idx][1]])


        # annotate image if needed
        if glint_detect_params["display_glints_verbose"] and glint_detect_params["display_glint_imgs"]:
            for blob in glint_blobs:
                x,y = int(blob[0]), int(blob[1])

                # if the final pair then print green, otherwise yellow
                if glints is not None:
                    if x == glints.left.rel_x and y == glints.left.rel_y or x == glints.right.rel_x or y == glints.left.rel_y:
                        cv2.circle(img, (x, y), 2, [0,255,0], -1)
                    else:
                        cv2.circle(img, (x, y), 2, [0,255,255], -1)
                else:
                    cv2.circle(img, (x, y), 2, [0,255,255], -1)

            for blob in elim_blobs:
                x,y = int(blob[0]), int(blob[1])
                cv2.circle(img, (x, y), 2, [0,0,255], -1)


        # if image is being shown (settings) 
        # TODO use which_eye param to select imshow location
        # TODO this runs for either gaze calib or gaze tracking but only checkes the gaze tracking setting
        if glint_detect_params["display_glint_imgs"]:

            # NOTE: Not deleting these windows is the cause of the Q timer bug!! (I think)
            # NOTE: Update- error v likely caused by not commenting out main in regression.py and running face_analysis.py
            # if the left eye, display window on right to make mirror image
            # window_name = which_eye + "glint detection"
            # cv2.namedWindow(window_name)
            # img_window_x = 30
            # if which_eye == 'l':
            #     img_window_x += 2*og_w
            # cv2.moveWindow(window_name, (img_window_x), 20)
            # cv2.imshow(window_name, img)
            cv2.imshow(str(which_eye + "glint detection"), img)

        return glints

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Image Functions
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    # create imgs for eyes and return them
    def get_eye_imgs(self, frame, transform=False):
        if not transform:
            eye_imgs = (self.crop_rect(frame, self.eyes.left.image_box.rect), self.crop_rect(frame, self.eyes.right.image_box.rect))
        else:
            eye_imgs = self.rectify_eye_imgs(eye_imgs)
        return eye_imgs

    # display the eye images
    def show_eye_imgs(self):
        if self.show_eyes:
            cv2.imshow("right eye cropped", self.right_eye_img)
            cv2.waitKey(1)
            cv2.imshow("left eye cropped", self.right_eye_img)
            cv2.waitKey(1)

    def save_eye_imgs(self):

        # # create sub-image of each eye
        # right_img = self.crop_rect(frame, self.eyes.right.image_box.rect)
        # left_img = self.crop_rect(frame, self.eyes.left.image_box.rect)

        # dst = cv2.perspectiveTransform(pts, matrix)

        # get transform to make image "fit" the right eye
        # right_img_trans = self.eyes.right.image_box.get_homography_matrix()
        # tf_img_warp = transform.warp(frame, right_img_trans.inverse, mode = 'symmetric')

        # # cv2.imshow("image warped for right eye", tf_img_warp)
        # fig, ax = plt.subplots(1,2)

        # ax[0].set_title(f'Original', fontsize = 15)
        # ax[0].imshow(frame)
        # ax[0].set_axis_off()

        # ax[1].set_title(f'Transformed', fontsize = 15)
        # ax[1].imshow(tf_img_warp)
        # ax[1].set_axis_off()
        # plt.show()
        # cv2.waitKey(1)
        
        # dst = cv2.perspectiveTransform(self.eyes.,right_img_trans)

        # self.left_img = self.crop_rect(frame, self.eyes.left.image_box.rect)
        # self.right_img = self.crop_rect(frame, self.eyes.right.image_box.rect)
        # get titles for eye images
        if self.frame_time_annotate == None:
            r_title = ((str(time.ctime(time.time()).split()[-2])).replace('.','_') + "_r.jpg").replace(':','-')
            l_title = ((str(time.ctime(time.time()).split()[-2])).replace('.','_') + "_l.jpg").replace(':','-')
        else:
            r_title = ("{:.2f}".format(round(self.frame_time_annotate,3)) + "_r.jpg").replace(':','_')
            l_title = ("{:.2f}".format(round(self.frame_time_annotate,3)) + "_l.jpg").replace(':','_')

        # save eye images in relevant spot- if calibrating and recording data save in fixation-subfolder
        eye_folder = self.main_eye_img_folder_name
        if self.thread_events is not None:
            if self.thread_events.fixation_event.is_set():
                eye_folder = self.eye_img_save_path
            else:
                eye_folder = os.path.join(self.main_eye_img_folder_name, "misc")
        else:
            eye_folder = os.path.join(self.main_eye_img_folder_name, "misc")

        r_path = os.path.join(eye_folder, "right_eye", r_title)
        l_path = os.path.join(eye_folder, "left_eye", l_title)

        # save the images + small delay to ensure save processes complete
        if not cv2.imwrite(r_path, self.right_eye_img):
            raise Exception("Could not write right-eye image")
        cv2.waitKey(1)

        if not cv2.imwrite(l_path, self.left_eye_img):
            raise Exception("Could not write left-eye image")
        cv2.waitKey(1)

    def crop_rect(self, img, rect):
        # Source: https://jdhao.github.io/2019/02/23/crop_rotated_rectangle_opencv/
        # get the parameter of the small rectangle
        center, size, angle = rect[0], rect[1], rect[2]
        center, size = tuple(map(int, center)), tuple(map(int, size))

        # get row and col num in img
        height, width = img.shape[0], img.shape[1]

        # calculate the rotation matrix
        M = cv2.getRotationMatrix2D(center, angle, 1)

        # rotate the original image
        img_rot = cv2.warpAffine(img, M, (width, height))

        # now rotated rectangle becomes vertical, and we crop it
        img_crop = cv2.getRectSubPix(img_rot, size, center)

        # rotate the image so that it is in landscape and not upside down
        crop_height, crop_width = img_crop.shape[0], img_crop.shape[1]
        if crop_height > crop_width:

            # if the user is tilting their head to their right
            if self.pose.orientation.z < 0:
                img_crop = cv2.rotate(img_crop, cv2.ROTATE_90_COUNTERCLOCKWISE)
            else:
                img_crop = cv2.rotate(img_crop, cv2.ROTATE_90_CLOCKWISE)

        return img_crop
        
    def rectify_eye_imgs(self, eye_imgs):
        transformed_eye_imgs = []
        for eye in eye_imgs:
            transform = eye.image_box.get_homography_matrix(eye.image_box.width_px, eye.image_box.height_px)
            # apply transform and save img
            transformed_eye_imgs.append(eye)
            
        return transformed_eye_imgs

    # def rectify_eye_boxes(self, eye_imgs):
    #     transformed_eye_imgs = []
    #     for eye in [self.eyes.left, self.eyes.right]:
    #         transform = eye.image_box.get_homography_matrix(eye.image_box.width_px, eye.image_box.height_px)
    #         transformed_eye_imgs.append(eye)
    #         # print(transform)
    #         # print(frame_temp[500][500])
    #         # TODO finish transform calc
    #     return transformed_eye_imgs

    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    # Image Annotation functions
    # %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    def annotate_eye_corner_vectors(self):
        for cur_eye in [self.eyes.left, self.eyes.right]:

            # arrow from left eye corner to pupil
            self.annotated_image = cv2.arrowedLine(self.annotated_image,(cur_eye.eye_corner_left.x, cur_eye.eye_corner_left.y),
                (cur_eye.pupil.x, cur_eye.pupil.y), (255,255,0), 2)

            # arrow from right eye corner to pupil
            self.annotated_image = cv2.arrowedLine(self.annotated_image,(cur_eye.eye_corner_right.x, cur_eye.eye_corner_right.y),
                (cur_eye.pupil.x, cur_eye.pupil.y), (255,255,0), 2)

    def annotate_eyes(self):
        # annotate pupil
        for cur_eye in [self.eyes.left, self.eyes.right]:

            # pupil center
            # cv2.circle(self.annotated_image, (cur_eye.pupil.x, cur_eye.pupil.y), 4, [255,255,0], -1)

            # arrow from center eye box to pupil
            # self.annotated_image = cv2.arrowedLine(self.annotated_image,(cur_eye.centre.x, cur_eye.centre.y),
            #     (cur_eye.pupil.x, cur_eye.pupil.y), (255,255,0), 2)

            # write angle and length
            angle = cur_eye.image_box.relative_angle2POI * 180/pi
            cv2.putText(self.annotated_image, 
                "{:.2f} deg".format(angle), 
                (int(cur_eye.image_box.centre.x + 15), int(cur_eye.image_box.img_lowest_val - 10)), 
                cv2.FONT_HERSHEY_SIMPLEX, 1, (255,255,0), 2)
    
            # Draw the eye boxes
            # display centers of boxes
            cv2.circle(self.annotated_image, ((cur_eye.image_box.centre.x, cur_eye.image_box.centre.y)), 3, (0,0,255), -1)
            
            # boxes around eye images
            cv2.drawContours(self.annotated_image, [cur_eye.image_box.box_points], 0, (0, 0, 255), 2)
                   
    def annotate_pose(self):
        cv2.line(self.annotated_image, self.pose.nose_line_points[0], self.pose.nose_line_points[1], (255, 0, 0), 3)
        cv2.putText(self.annotated_image, "x: " + str(np.round((180/pi) * self.pose.orientation.x,2)), (500, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.putText(self.annotated_image, "y: " + str(np.round((180/pi) * self.pose.orientation.y,2)), (500, 100), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        cv2.putText(self.annotated_image, "z: " + str(np.round((180/pi) * self.pose.orientation.z,2)), (500, 150), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

        # draw angled line to show head tilt. starts from the bottom left of the face box
        if self.face_border_box == None:
            self.get_head_border()
        start_point = pixel_coords(self.face_border_box.img_leftmost_val, self.face_border_box.img_lowest_val)
        len = 0.2 * self.face_border_box.width_px
        end_point = start_point.calc_vector_end_coords(len, self.pose.orientation.z)
        self.annotated_image = cv2.arrowedLine(self.annotated_image,(start_point.x, start_point.y),
                                                (end_point.x, end_point.y), (0,100,255), 3)
        cv2.putText(self.annotated_image, "{:.2f} degrees".format(self.pose.orientation.z * 180/pi), (int(start_point.x + len), int(start_point.y - 5)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 100, 200), 2)

    # annotate the pupil and glints in each eye
    def annotate_glints(self):

        for eye in [self.eyes.left, self.eyes.right]:

            # pupil
            cv2.circle(self.annotated_image, (eye.pupil.x, eye.pupil.y), 2, [255,0,0], -1)

            # add circles around the glints
            for glint in [eye.glint_pair.left,eye.glint_pair.right]:
                cv2.circle(self.annotated_image, (int(glint.abs_x), int(glint.abs_y)), 2, [0,255,0], -1)

        return self.annotated_image



