import pyautogui

# Cheatsheet: https://pyautogui.readthedocs.io/en/latest/quickstart.html


# set up a failsave 2.5 second pause between autogui commands 
# (so your mouse can be used in between if program is stuck)
pyautogui.PAUSE = 2.5
pyautogui.FAILSAFE = True

#current mouse position in pixels
current_mouse_pos = pyautogui.position()

#screen resolution/size in pixels
screen_res = pyautogui.size()

# check if coordinates are on/fit on the screen
coords_fit = pyautogui.onScreen(100,300)

print(current_mouse_pos)
print(screen_res)
print(coords_fit)


# moving the mouse in absolute coords. Duration is the time taken to move there.
# if duration is zero then movement is instant
pyautogui.moveTo(300,300,duration=3)

# moving the mouse in relative coords. Be sure to check that the new position is on the screen
(x_offset, y_offset) = (50, 50)
if pyautogui.onScreen(pyautogui.position()[0] + x_offset, pyautogui.position()[1] + y_offset):
    pyautogui.moveRel(50, 50, duration=1)

# draging (and hold!) the mouse
# pyautogui.dragTo(200, 400, duration=3)  # drag mouse to XY
# pyautogui.dragRel(100, -150, duration=2)  # drag mouse relative to its current position

# click the mouse. button can be 'left', 'right' or 'middle'
pyautogui.click(x=400, y=200, clicks=2, interval=0.1, button='left')

# # alternate click functions
# pyautogui.rightClick(x=400, y=200)
# pyautogui.middleClick(x=400, y=200)
# pyautogui.doubleClick(x=400, y=200)
# pyautogui.tripleClick(x=400, y=200)

# #for clicking and dragging, click selection and deselection can be done manually
# pyautogui.mouseDown(x=400, y=400, button='left')
# pyautogui.mouseUp(x=400, y=400, button='left')

# scrolling. negative values scroll down
pyautogui.scroll(-8, x=500, y=200)

# # Messaging functions
# pyautogui.alert('This displays some text with an OK button.')
# button_choice = pyautogui.confirm('This displays text and has an OK and Cancel button.')

# input = pyautogui.prompt('This lets the user type in a string and press OK.')
