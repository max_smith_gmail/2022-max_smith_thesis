from cmath import pi
import cv2
import numpy as np
import os
from facemesh_classes import pixel_coords
from skimage.feature import blob_dog, blob_log, blob_doh
from itertools import combinations
from math import hypot
from time import sleep
from math import atan
from statistics import mean

class glint():
    def __init__(self, x, y,area):
        self.x = x
        self.y = y
        self.area = area

class glint_pair():
    def __init__(self, blob_1, blob_2):
        glints = [glint(blob_1[0], blob_1[1], blob_1[2]), 
                  glint(blob_2[0], blob_2[1], blob_2[2])]

        # check left and right (from the user's perspective)
        if glints[0].x > glints[0].y:
            self.left = glints[0]
            self.right = glints[1]
        else:
            self.left = glints[1]
            self.right = glints[0]

    def get_csv_str(self):
        ret_str = [ 'L({},{})'.format(self.left.x, self.left.y),
                    'R({},{})'.format(self.right.x, self.right.y)]
        return ret_str

def distance(p1,p2):
    """Euclidean distance between two points."""
    x1,y1 = p1
    x2,y2 = p2
    return hypot(x2 - x1, y2 - y1)

def get_glint_locations(img, glint_detect_params, head_tilt_rad, prev_glint_pair):
    # TODO: maybe use mediapipe pupil location as bounding condition (radius around pupil), test if reliable
    """
        0. crop as corneal glints must be central
        1. threshold image to isolate bright points
        2. filter out any bright objects larger than glint (experiment with this) or smaller than spec
        3. isolate two closest to image center
    """
    

    # crop border area of image to focus in on pupil #TODO: may need to focus in on this or broaden, make part of gaze settings
    [og_h, og_w, og_d] =np.shape(img)
    crop_perc = 0.2
    cropped = img[int(crop_perc*og_h):int((1-crop_perc)*og_h), int(crop_perc*og_w):int((1-crop_perc)*og_w)]
    
    # convert to grayscale and blur to help with noise
    gray = cv2.cvtColor(cropped, cv2.COLOR_BGR2GRAY)  
    blurred = cv2.GaussianBlur(gray, (5,5),0)

    # gather settings
    threshold_min = glint_detect_params["threshold_min"]
    threshold_max = glint_detect_params["threshold_max"]
    min_sigma = glint_detect_params["min_sigma"]
    max_sigma = glint_detect_params["max_sigma"]
    threshold = glint_detect_params["threshold"]
    exclude_border = glint_detect_params["exclude_border"]

    # detect blob locations using laplace of guassian technique on thresholded image
    # https://scikit-image.org/docs/stable/api/skimage.feature.html#skimage.feature.blob_log
    thresh_img = cv2.threshold(blurred,threshold_min,threshold_max,cv2.THRESH_BINARY)[1]
    blobs = blob_log(thresh_img,min_sigma=min_sigma, max_sigma=max_sigma, threshold=threshold, exclude_border=exclude_border)
    
    # transfer data into np array and swap first two cols so that format is x,y,area
    np_blobs = (np.asarray(blobs)).astype(float)
    np_blobs[:, [1, 0]] = np_blobs[:, [0, 1]]

    # translate to original, non-cropped coordinates
    np_blobs[:,0] += int(crop_perc*og_w)
    np_blobs[:,1] += int(crop_perc*og_h)
    
    # create vectors of booleans for keep track of blobs to keep
    glint_blob_bool_v = np.ones((len(np_blobs)), dtype=bool)

    # gather border cuttoff from settings
    cutoff_border_frac = glint_detect_params["cutoff_border_perc"]/100

    # isolate glint blobs using vector, elimate incorrect one by one
    for i in range(len(np_blobs)):
        x, y, area = np_blobs[i]

        # must have area less than XXX #TODO print out areas of glint blobs for exp
        if area > 5: 
            glint_blob_bool_v[i] = False

        # must be close to the center of image (not in the cutoff border)
        elif (abs(x - og_w/2) > (cutoff_border_frac * og_w/2)) or (abs(y - og_h/2) > (cutoff_border_frac * og_h/2)):
            glint_blob_bool_v[i] = False

    # draw rect to show area cutoff
    if glint_detect_params["display_glints_verbose"]:
        top_left = (int(cutoff_border_frac * og_w/2), int(cutoff_border_frac * og_h/2))
        bottom_right = (int(og_w) - int(cutoff_border_frac * og_w/2), int(og_h) - int(cutoff_border_frac * og_h/2))
        color = (0, 0, 255)
        thickness = 1
        img = cv2.rectangle(img, top_left, bottom_right, color, thickness)

    # isolate successful blobs (potential glints)
    glint_blobs = np_blobs[glint_blob_bool_v]
    elim_blobs = np_blobs[np.logical_not(glint_blob_bool_v)]

    # checking all remaining blob combinations to isolate the glint pair
    indicies = np.arange(0,len(glint_blobs))
    combos = np.array(list(combinations(indicies,2)))
    good_combos_bool_vector = np.ones((len(combos)), dtype=bool)

    tilt_tolerance_deg = glint_detect_params["glint_tilt_error_tolerance_deg"]
    glint_dist_min_px = glint_detect_params["glint_dist_min_px"]
    glint_dist_max_px = glint_detect_params["glint_dist_max_px"]

    for i in range(len(combos)):

        # make blob 1 on the user's left (right on image)
        if glint_blobs[combos[i][0]][0] < glint_blobs[combos[i][1]][0]:
            temp_blob = combos[i][0]
            combos[i][0] = combos[i][1] 
            combos[i][1] = temp_blob

        blob_left = glint_blobs[combos[i][0]]
        blob_right = glint_blobs[combos[i][1]]

        # make angle measurement from user's left to right
        glint_tilt_rad = atan((blob_left[1] - blob_right[1])/(abs(blob_left[0] - blob_right[0])))

        # if distance is not within range, exclude combo
        dist = distance(blob_left[:2], blob_right[:2])
        if dist < glint_dist_min_px or dist > glint_dist_max_px:
            good_combos_bool_vector[i] = False

        # if angle is incorrect by more than 10 degrees, exclude combo
        elif abs(head_tilt_rad - glint_tilt_rad) > (tilt_tolerance_deg * pi/180):
            good_combos_bool_vector[i] = False

    # isolate valid combinations, hopefully there is only one combination left
    valid_combos = combos[good_combos_bool_vector]

    # return coordinates of pupils in images
    if (len(valid_combos) == 0):
        glints = None

    elif (len(valid_combos) == 1):
        glints = glint_pair(glint_blobs[valid_combos[0][0]], glint_blobs[valid_combos[0][1]])

    else:
        # if there are multiple valid glint options, find the closest to the previous glint. if not provided then closest to img middle
        combo_displacements = np.zeros(len(valid_combos))
        for i in range(len(valid_combos)):

            blob_left = glint_blobs[combos[i][0]]
            blob_right = glint_blobs[combos[i][1]]
            midpoint = (mean([blob_left[0], blob_right[0]]), mean([blob_left[1], blob_right[1]]))

            if prev_glint_pair is None:
                combo_displacements[i] = distance((0,0),(abs(midpoint[0]- og_w/2),abs(midpoint[1]-og_h/2)))
                print("Couldn't isolate glints, and no previous glint provided. Returning closest glint pair to image center.")

            else:
                prev_midpoint = (   mean([prev_glint_pair.left.x, prev_glint_pair.right.x]), 
                                    mean([prev_glint_pair.left.y, prev_glint_pair.right.y]))
                combo_displacements[i] = distance((0,0),(abs(midpoint[0]- prev_midpoint[0]), abs(midpoint[1] - prev_midpoint[1])))
                print("Couldn't isolate glints, returning closest glint pair to previous glint")
        
        best_combo_idx = np.argmin(combo_displacements)
        glints = glint_pair(glint_blobs[valid_combos[best_combo_idx][0]], glint_blobs[valid_combos[best_combo_idx][1]])


    # annotate image if needed
    if glint_detect_params["display_glints_verbose"] and glint_detect_params["display_glint_imgs"]:
        for blob in glint_blobs:
            x,y = int(blob[0]), int(blob[1])

            # if the final pair then print green, otherwise yellow
            if glints is not None:
                if x == glints.left.x and y == glints.left.y or x == glints.right.x or y == glints.left.y:
                    cv2.circle(img, (x, y), 2, [0,255,0], -1)
                else:
                    cv2.circle(img, (x, y), 2, [0,255,255], -1)
            else:
                cv2.circle(img, (x, y), 2, [0,255,255], -1)

        for blob in elim_blobs:
            x,y = int(blob[0]), int(blob[1])
            cv2.circle(img, (x, y), 2, [0,0,255], -1)


    # if image is being shown (settings)
    if glint_detect_params["display_glint_imgs"]:
        cv2.imshow("Glint Detection Display", img)

    # TODO: TEMP, remove sleep
    if cv2.waitKey(1) == 32:
        sleep(10)
    return glints


def main():
    img_folder = "Recorded_data/Aug_22_16:24:46_pnr_calib_rec/Eye_images/misc/left_eye"
    for img_name in os.listdir(img_folder):
        img = cv2.imread(os.path.join(img_folder,img_name))
        get_glint_locations(img)

# main()





# https://towardsdatascience.com/image-processing-blob-detection-204dc6428dd
# https://medium.com/swlh/image-processing-with-python-connected-components-and-region-labeling-3eef1864b951

# def method_1_simple_blob(img):
   
#     # https://learnopencv.com/blob-detection-using-opencv-python-c/
#     # Setup SimpleBlobDetector parameters.
#     params = cv2.SimpleBlobDetector_Params()

#     # Change thresholds
#     params.minThreshold = 120
#     params.maxThreshold = 255

#     # Filter by Area.
#     params.filterByArea = True
#     params.minArea = 1
#     params.maxArea = 100

#     # Filter by Circularity
#     params.filterByCircularity = False
#     params.minCircularity = 0.8

#     # Filter by Convexity
#     params.filterByConvexity = False
#     params.minConvexity = 0.8

#     # Filter by Inertia
#     params.filterByInertia = False
#     params.minInertiaRatio = 0.01

#     # Create a detector with the parameters
#     ver = (cv2.__version__).split('.')
#     if int(ver[0]) < 3 :
#         detector = cv2.SimpleBlobDetector(params)
#     else : 
#         detector = cv2.SimpleBlobDetector_create(params)

#     detected_glints = detector.detect(img)
    

#     blank = np.zeros((1, 1))
#     return cv2.drawKeypoints(img, detected_glints, blank, (0, 0, 255),cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

# def method_2_log(img, overlay_img):
#     blobs = blob_log(img, max_sigma=30, threshold=0.01)
#     """
#     In the Chapter 3, Convolution and Frequency Domain Filtering, we saw that the cross correlation of an 
#     image with a filter can be viewed as pattern matching; that is, comparing a (small) template image 
#     (of what we want to find) against all local regions in the image. The key idea in blob detection comes 
#     from this fact. We have already seen how an LoG filter with zero crossing can be used for edge detection
#     in the last chapter. LoG can also be used to find scale invariant regions by searching 3D (location + scale) 
#     extrema of the LoG with the concept of Scale Space. If the scale of the Laplacian (σ of the LoG filter) 
#     gets matched with the scale of the blob, the magnitude of the Laplacian response attains a maximum at 
#     the center of the blob. With this approach, the LoG-convolved images are computed with gradually increasing 
#     σ and they are stacked up in a cube. The blobs correspond to the local maximums in this cube. This approach 
#     only detects the bright blobs on the dark backgrounds. It is accurate, but slow (particularly for detecting 
#     larger blobs).
#     https://subscription.packtpub.com/book/big-data-and-business-intelligence/9781789343731/7/ch07lvl1sec52/blob-detectors-with-log-dog-and-doh
#     """
#     for blob in blobs:
#         y, x, area = blob
#         cv2.circle(overlay_img, (int(x),int(y)), 2, [255,255,0], -1)

# def method_3_dog(img, overlay_img):
#     blobs = blob_dog(img, max_sigma=30, threshold=0.01)
#     """
#     The LoG approach is approximated by the DoG approach, and hence it is faster. 
#     The image is smoothed (using Gaussians) with increasing σ values, and 
#     the difference between two consecutive smoothed images is stacked up in a cube. 
#     This approach again detects the bright blobs on the dark backgrounds. 
#     It is faster than LoG but less accurate, although the larger blobs detection is still expensive.
#     https://subscription.packtpub.com/book/big-data-and-business-intelligence/9781789343731/7/ch07lvl1sec52/blob-detectors-with-log-dog-and-doh
#     """
#     for blob in blobs:
#         y, x, area = blob
#         cv2.circle(overlay_img, (int(x),int(y)), 2, [255,255,0], -1)

# def method_4_doh(img, overlay_img):
#     blobs = blob_doh(img, max_sigma=30, threshold=0.01)
#     """
#     The DoH approach is the fastest of all these approaches. 
#     It detects the blobs by computing maximums in the matrix of the Determinant of 
#     Hessian of the image. The size of blobs does not have any impact on the speed 
#     of detection. Both the bright blobs on the dark background and the dark blobs 
#     on the bright backgrounds are detected by this approach, but the small blobs 
#     are not detected accurately.
#     https://subscription.packtpub.com/book/big-data-and-business-intelligence/9781789343731/7/ch07lvl1sec52/blob-detectors-with-log-dog-and-doh
#     """
#     for blob in blobs:
#         y, x, area = blob
#         cv2.circle(overlay_img, (int(x),int(y)), 2, [255,255,0], -1)
