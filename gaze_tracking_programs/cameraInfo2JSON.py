from asyncore import write
import sys

def makeTabs(num):
    tabs = ""
    for i in range(num):
        tabs += "\t"
    return tabs

def addQuotes(str_in):
    ret_str = "\""
    if type(str_in) is list:
        for i in range(len(str_in)):
            if i == len(str_in) -1:
                ret_str += str_in[i] + "\""
            else:
                ret_str += str_in[i] + " "
        return ret_str
    return "\"" + str_in + "\""

def convert2JSON(lines):

    with open(sys.argv[2],"w") as f_out:

        # first line of json
        f_out.write("{\n")
        tab_count = 1
        indexX = 0
        videoX = 0
        listing_resolutions = False
        resolution_count = 0

        for line in lines:

            # remove new line character + preceeding whitespace from old line
            line = line.strip()

            # if the entry is irrelevant ignore
            if "ioctl" in line or line == "":
                continue

            # if the entry is a new camera feed/ video index
            elif "/dev" in line:
                if videoX != 0:
                    
                    # finish newline after previous interval
                    f_out.write("\n")
                    # finish previous resolutions brackets
                    tab_count -= 1
                    f_out.write(makeTabs(tab_count) + "]\n")
                     
                    # finish previous index brackets
                    tab_count -= 1
                    f_out.write(makeTabs(tab_count) + "}\n")

                    # finish previous video brackets
                    tab_count -= 1
                    f_out.write(makeTabs(tab_count) + "],\n\n")
                f_out.write(makeTabs(tab_count) + "\"" + line + "\": [\n")
                tab_count +=1
                videoX += 1
                indexX = 0
                continue

            # if the entry is a different entry from the camera add brackets
            elif "Index" in line:
                if listing_resolutions:
                    f_out.write("\n")
                    tab_count -= 1
                    f_out.write(makeTabs(tab_count) + "]\n")
                    tab_count -= 1
                    # last index not accounted for yet
                    f_out.write(makeTabs(tab_count) + "},\n")
                    listing_resolutions = False
                # if indexX != 0:
                f_out.write(makeTabs(tab_count) + "{\n")
                tab_count += 1
                indexX += 1
                
            elif "Size" in line:
                if listing_resolutions:
                    f_out.write(",")
                f_out.write(makeTabs(tab_count) + "\n")
                f_out.write(makeTabs(tab_count) + "{\n")
                resolution_count += 1
                tab_count += 1
                listing_resolutions = True
            

            
            # add quotations for tag and info, remove other whitespace
            # += tag + : + value
            split_idx = line.index(":")
            newline = makeTabs(tab_count) + addQuotes(line[:split_idx].strip()) + ":" + addQuotes(line[split_idx+1:].strip())
            if "Interval" not in line:
                newline += ","
            f_out.write(newline + "\n")

            # if the entry is a name then we need to add the resolutions tag before the next line
            if "Name" in line:
                f_out.write(makeTabs(tab_count) + "\"Resolutions\": [")

                tab_count += 1
                # listing_resolutions = True

            elif "Interval" in line:
                tab_count -= 1
                f_out.write(makeTabs(tab_count) + "}")
                
                # f_out.write("\n")
                listing_resolutions = True

        # close final bracket then last line of json and close
        f_out.write(makeTabs(tab_count) + "]\n")
        f_out.write("}")
    f_out.close
    
def main():
    # try:
    with open(sys.argv[1],"r") as f_in:
        lines = f_in.readlines()
        convert2JSON(lines)
    f_in.close
    # except:
    #     print("Unable to load filename")

main()