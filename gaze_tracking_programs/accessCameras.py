import sys
import json
from unittest.mock import CallableMixin
import cv2
import os
import time
import numpy as np

class video_input:

    def __init__(self, stream, index, name, pixel_format):
        self.stream = stream                # /dev/videoX
        self.index = index                  #  0, 1 etc
        self.name = name                    # 'MJPG'
        self.pixel_format = pixel_format    # Motion-JPEG
        self.resolutions = []               #[[640,480, 30], [1920,1080, 30]] etc
        self.selected_resolution = 0        # 1 

    def add_resolution(self, w, h, fps):
        self.resolutions.append([w,h,fps])

    def select_resolution(self,index):
        if index < len(self.resolutions):
            self.select_resolution = index
            print("You have selected {} at ".format(self.name), end = '')
            self.print_resolution(self.select_resolution)
        else:
            print("Invalid selection index. Options range from 0:{}".format(len(self.resolutions)))
    
    def get_resolution(self):
        return self.resolutions[self.selected_resolution]

    def get_fourcc_code(self):
        pixel_code = self.pixel_format.split("\'")[1]
        # return cv2.VideoWriter_fourcc(pixel_code[0], pixel_code[1], pixel_code[2], pixel_code[3])
        return cv2.VideoWriter_fourcc(*pixel_code[0:4])
         
    def disp(self):
        print("{}[{}]".format(self.stream, self.index))
        print("Name: {}".format(self.name))
        print("Pixel Format: {}".format(self.pixel_format))
        print("Available resolutions:")
        i = 0
        for res in self.resolutions:
            print("\t{}. ".format(i), end='')
            self.print_resolution(i)
            i += 1
        print("\n")

    def print_resolution(self, index):
        if index < len(self.resolutions):
            res = self.resolutions[index]
            print("{}x{} @ {}fps".format(res[0], res[1], res[2]))

# get fourcc character code from encodedfourcc num
def decode_fourcc(cc):
    return "".join([chr((int(cc) >> 8 * i) & 0xFF) for i in range(4)])

# Opening JSON file and return as dictionary
def get_JSON(filename):
    f = open(filename)
    return json.load(f)

# use JSON file and use data to fill in video_input object (which is more accessible)
def gather_JSON_data(filename):

    camera_info = get_JSON(filename)

    video_options = []
    for video_stream in camera_info:
        for vid_format_index in camera_info[video_stream]:
            print(vid_format_index)
            # gather data from JSON
            index =  int(vid_format_index["Index"])
            pixel_format = vid_format_index["Pixel Format"]
            name = vid_format_index["Name"]
            resolutions = vid_format_index["Resolutions"]

            # create new video_input class then add the available resolutions
            current_option = video_input(video_stream, index, name, pixel_format)
            for r in resolutions:
                w = int(r["Size"].split(" ")[1].split("x")[0])
                h = int(r["Size"].split(" ")[1].split("x")[0])
                fps = float(r["Interval"].split("(")[1].split(" ")[0])
                current_option.add_resolution(h,w,fps)

            video_options.append(current_option)
            
    return video_options

# prompt the user to select a camera from displayed options
def user_camera_selection(video_options):
    print("Available video stream options:\n")

    for op in video_options:
        op.disp()

    # ask for input until correct format given
    input_success = False
    while input_success == False:
        try:
            user_selection = input("Enter the video stream index followed by the resolution option index e.g. \"0 0\"\n").split(" ")
            stream_idx = int(user_selection[0])
            res_option_idx = int(user_selection[1])
            input_success = True
        except:
            print("Incorrectlty formatted input, enter each index seperated by a space")
            pass
    
    # select and return video
    video_options[stream_idx].select_resolution(res_option_idx)
    return video_options[stream_idx]

def print_video_parameters(cap):

    # useful info about opencv on this computer
    # print(cv2.getBuildInformation())

    for i in range(47):
        print("Parameter No.={} value={}".format(i,cap.get(i)))

    # print("Saturation: {}".format(cap.get(cv2.CAP_PROP_SATURATION)))
    # print("Autofocus: {}".format(cap.get(cv2.CAP_PROP_AUTOFOCUS)))
    # print("Exposure: {}".format(cap.get(cv2.CAP_PROP_EXPOSURE)))
    # print("ISO Speed: {}".format(cap.get(cv2.CAP_PROP_ISO_SPEED)))
    # print("Hue: {}".format(cap.get(cv2.CAP_PROP_HUE)))
    # print("Contrast: {}".format(cap.get(cv2.CAP_PROP_CONTRAST)))
    # print("Brightness: {}".format(cap.get(cv2.CAP_PROP_BRIGHTNESS)))
    # print("Gamma: {}".format(cap.get(cv2.CAP_PROP_GAMMA)))


def set_video_parameters(cap, selected_video):
    # Set parameters for capture, parameter documentation 
    # here https://github.com/opencv/opencv/blob/master/modules/videoio/include/opencv2/videoio.hpp
    # and here https://docs.opencv.org/4.x/d4/d15/group__videoio__flags__base.html#gaeb8dd9c89c10a5c63c139bf7c4f5704d
    
    # cap.set(cv2.CV_CAP_PROP_POS_MSEC, 0)
    cap.set(cv2.CAP_PROP_FOURCC, selected_video.get_fourcc_code())
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, selected_video.get_resolution()[0])
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, selected_video.get_resolution()[1])
    cap.set(cv2.CAP_PROP_FPS, selected_video.get_resolution()[2])

    # # auto_expose_check(cap) (off)
    # ret = cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 2.5)
    # print(ret)     
    # print("check exposure options")
    # print("exposure before", cap.get(14))
    # print(cap.set(cv2.CAP_PROP_EXPOSURE, -0.5))
    # print("Set exposure!")
    # print("exposure after", cap.get(14))
    # check_expose(cap)
    # check_param(cap, cv2.CV_CAP_PROP_MODE)
    # cap.set(cv2.CV_CAP_PROP_FORMAT, 0)
    # cap.set(cv2.CV_CAP_PROP_MODE, 0)
    # cap.set(cv2.CAP_PROP_BRIGHTNESS, 50)
    # cap.set(cv2.CAP_PROP_CONTRAST, 0)
    # cap.set(cv2.CAP_PROP_SATURATION, 0)
    # cap.set(cv2.CV_CAP_PROP_GAIN, 0)
    # cap.set(cv2.CV_CAP_PROP_CONVERT_RGB, 0)
    # cap.set(cv2.CAP_PROP_RECTIFICATION, 0)
    # cap.set(cv2.CAP_PROP_MONOCHROME, 0)
    # cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)
    # cap.set(cv2.CAP_PROP_SETTINGS, 0)
    # cap.set(cv2.CAP_PROP_SAR_DEN, 0)
    # cap.set(cv2.CAP_PROP_CHANNEL, 0)
    # cap.set(cv2.CAP_PROP_AUTO_WB, 0)
    # print_video_parameters(cap)
    return cap


def check_param(cap, param):
    for i in range(-1000, 9000):
        for f in range(0,3):
            ret = cap.set(param, i + f*0.25)
            if ret is not False:
                print(ret)
                print("value change at i+ f*0.25= {}", i+ f*0.25)

def check_expose(cap):
    for i in range(-1000, 9000):
        for f in range(0,3):
            ret = cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, i + f*0.25)
            if ret is not False:
                print(ret)
                print("value change at i+ f*0.25= {}", i+ f*0.25)
            

def auto_expose_check(cap):
    print("check values of autoexposure")
    prev = cap.get(23)
    for i in range(-1000, 9000):
        for f in range(0,3):
            ret = cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, i + f*0.25)
            if ret is not False:

                print(ret)
                print("value change at i+ f*0.25= {}", i+ f*0.25)
            if cap.get(23) != prev:
                print("value change at i+ f*0.25= {}", i+ f*0.25)
                prev = cap.get(23)

        if (i%500 == 0):
            time.sleep(0.1)

    print("done checking")


def open_video(selected_video):

    # Capture video stream
    cap = cv2.VideoCapture(int(selected_video.stream[-1]))
    # cap = cv2.VideoCapture(int(selected_video.stream[-1]) + cv2.CAP_QT) #alternate video API
    # cap = cv2.VideoCapture(int(selected_video.stream[-1]) + cv2.CAP_V4L2) #alternate video API

    # adjust camera parameters (resolution etc)
    cap = set_video_parameters(cap, selected_video)

    #Check whether user selected camera is opened successfully.
    if not (cap.isOpened()):
        print("Could not open video device")

    return cap

def display_video(cap, time_ms, fps_bool, greyscale_bool):

    # time reference variable used to calculate FPS
    frame_start_time = time.time()
    time_total = 0

    # continue until set time has elapsed
    while time_total < time_ms:

        # Capture frame-by-frame
        success, frame = cap.read()

        # Stop if reading failed
        if success is False:
            raise Exception("Could not read image capture")

        # Convert to greyscale: direct grayscale input not supported on cam
        if greyscale_bool:
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

        # calcuate FPS and update time_total
        current_time = time.time()
        fps = 1 / (current_time - frame_start_time)
        time_total += current_time - frame_start_time
        frame_start_time = current_time

        # annotate FPS onto current frame
        if fps_bool:
            cv2.putText(frame, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)       

        # Display the resulting frame
        cv2.imshow('Video Input',frame)

        # Waits for a user input to quit the application
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

def create_video_writer(cap, file_location, filename_tag, turn_angle = 0):
    # default height and width
    vid_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    vid_height= int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # rotate the frame for height and width dimensions if needed
    if turn_angle%180 == 90:
        swap_temp = vid_width
        vid_width = vid_height
        vid_height = swap_temp

    frame_start_time = time.time()
    vid_filename = file_location +  "/" + ("_".join(time.ctime(frame_start_time).split()[1:4])).replace(':','_') + filename_tag + ".avi" 
    vid_fps = int(cap.get(cv2.CAP_PROP_FPS))
    vid_fourcc_format = int(cap.get(cv2.CAP_PROP_FOURCC))

    vid_writer = cv2.VideoWriter(vid_filename, vid_fourcc_format, vid_fps, (vid_height, vid_width))
    return vid_writer

# redundant atm
def write_frame(vid_writer, frame, turn_angle = 0):

    vid_height = np.shape(frame)[0]
    vid_width = np.shape(frame)[1]

    # if flipped by 90 deg then image dimensions are flipped
    if turn_angle%180 == 90:
        vid_height = np.shape(frame)[1]
        vid_width = np.shape(frame)[0]

def record_video(cap, time_ms, fps_bool, greyscale_bool, file_location, filename_add):

    # time reference variable used to calculate FPS
    frame_start_time = time.time()
    time_total = 0

    # setup video recording
    vid_filename = file_location + filename_add + "_" + "_".join(time.ctime(frame_start_time).split()[1:4]) + ".avi"
    vid_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    vid_height= int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    vid_fps= int(cap.get(cv2.CAP_PROP_FPS))
    vid_fourcc_format= int(cap.get(cv2.CAP_PROP_FOURCC))

    vid_writer= cv2.VideoWriter(vid_filename, vid_fourcc_format, vid_fps, (vid_width, vid_height))

    
    # continue until set time has elapsed
    while time_total < time_ms:

        # Capture frame-by-frame
        success, frame = cap.read()

        # Stop if reading failed
        if success is False:
            raise Exception("Could not read image capture")

        # Convert to greyscale: direct grayscale input not supported on cam
        if greyscale_bool:
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

        # add the frame to the video
        vid_writer.write(frame)

        # calcuate FPS and update time_total
        current_time = time.time()
        fps = 1 / (current_time - frame_start_time)
        time_total += current_time - frame_start_time
        frame_start_time = current_time

        # annotate FPS onto current frame
        if fps_bool:
            disp_frame = frame
            cv2.putText(disp_frame, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)       
            cv2.putText(disp_frame, "Recording...", (20, 110), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)       

        # Display the resulting frame
        cv2.imshow('Video Recording Preview',disp_frame)

        # Waits for a user input to quit the application
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    print("saved {} seconds of video to {}".format(time_total, vid_filename))
    vid_writer.release()


def close_video(cap):
    cap.release()
    cv2.destroyAllWindows()



# example use of module
def main():

    # get possible options for video input
    video_options = gather_JSON_data(sys.argv[1])

    # promt user to select video input source/option
    selected_video = user_camera_selection(video_options)

    # open video stream and select chosen parameters
    cap = open_video(selected_video)

    # show the video for X second with a fps overlay in RGB (not greyscale)
    greyscale = False
    fps_overlay = True
    # display_video(cap, 5, fps_overlay, greyscale)

    # show the video for X second with a fps overlay in RGB (not greyscale)
    record_video(cap, 3, fps_overlay, greyscale, "video_recordings/", "initial_testing")

    # close the video and destroy all windows
    close_video(cap)
    

# main()