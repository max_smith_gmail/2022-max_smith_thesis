# Following tut here: https://pyimagesearch.com/2017/04/03/facial-landmarks-dlib-opencv-python/

# import the necessary packages
from imutils import face_utils
import numpy as np
# import argparse
import imutils
import dlib
import cv2
import time

# construct the argument parser and parse the arguments
# ap = argparse.ArgumentParser()
# ap.add_argument("-p", "--shape-predictor", required=True, help="path to facial landmark predictor")
# ap.add_argument("-i", "--image", required=True, help="path to input image")
# args = vars(ap.parse_args())



# initialize dlib's face detector (HOG-based) and then create
# the facial landmark predictor
detector = dlib.get_frontal_face_detector()
# predictor = dlib.shape_predictor(args["shape_predictor"])
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")



# # get input from webcam
cap = cv2.VideoCapture(0)
# cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M','J','P','G'))
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
# cap.set(cv2.CAP_PROP_FPS, 30)

cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
cap.set(cv2.CAP_PROP_FPS, 30)

# time reference variable used to calculate FPS
pTime = 0

# while number_of_saved_eyes > 0:
while 1:

    # process the video frame in to an numpy array (img). raise error if failed
    success, img = cap.read()

    if success == False:
        raise Exception("Could not read image capture")
    # img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    # img = imutils.resize(img, width=500)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # detect faces in the grayscale i
    rects = detector(gray, 1)

    # loop over the face detections
    for (i, rect) in enumerate(rects):
        # determine the facial landmarks for the face region, then
        # convert the facial landmark (x, y)-coordinates to a NumPy
        # array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)
        # convert dlib's rectangle to a OpenCV-style bounding box
        # [i.e., (x, y, w, h)], then draw the face bounding box
        (x, y, w, h) = face_utils.rect_to_bb(rect)
        cv2.rectangle(i, (x, y), (x + w, y + h), (0, 255, 0), 2)
        # show the face number
        cv2.putText(i, "Face #{}".format(i + 1), (x - 10, y - 10),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        # loop over the (x, y)-coordinates for the facial landmarks
        # and draw them on the img
        for (x, y) in shape:
            cv2.circle(img, (x, y), 1, (0, 0, 255), -1)

    # calcuate FPS
    cTime = time.time()
    fps = 1 / (cTime - pTime)
    pTime = cTime

    img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    # annotate FPS onto current frame
    cv2.putText(img, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)
    
    # display the current frame
    cv2.imshow("img", img)
    cv2.waitKey(1)



# download must be unzipped and stored in directory then used in the command line
# python facial_landmarks.py --shape-predictor shape_predictor_68_face_landmarks.dat --image images/example_01.jpg
