#! /bin/bash

# helpful bash symbol meanings: https://linuxhint.com/bash-scripting-symbols/



# COMMAND LINE ARGUMENTS
# number of args in command line = $#
# access command line args $1, $2 etc
# access whole command line $@ or $*

# multi line comment
: '

This is the comments section

This is the first line

This is the second line

'


# VARIABLES
# to access use $var
    # a="cat"
    # b=”lynx”
    # c=”DRAGON”

# STRINGS
# string ength comarison use \< and \>
# to uppercase ^^ e.g. echo ${a^^}
# first letter uppercase ^
# to lowercase ,, e.g.  a = a,,

# FILE IO
# replace file: echo "hello world" > file.txt
# append to file: echo "hello world" >> file.txt

# RUN PYTHON SCRIPTS (just like terminal)
# python3 basics.py

# LOGIC
# -le = less than or equal to
# -lt = less than


# WHILE LOOP: https://ryanstutorials.net/bash-scripting-tutorial/bash-loops.php
counter=1
while [ $counter -le 10 ]
do
    echo $counter
    ((counter++))
done


# FOR LOOP: https://ryanstutorials.net/bash-scripting-tutorial/bash-loops.php
names='Stan Kyle Cartman'
for name in $names
do
    echo $name
done