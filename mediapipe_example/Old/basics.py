import cv2
import pandas as pd

# list camera resolutions and types (for index 0)
# v4l2-ctl -d /dev/video0 --list-formats-ext

def decode_fourcc(cc):
    return "".join([chr((int(cc) >> 8 * i) & 0xFF) for i in range(4)])

# determine what camera feeds are available
def returnCameraIndexes():
    # checks the first 10 indexes.
    index = 0
    arr = []
    while index < 10:
        cap = cv2.VideoCapture(index)
        if cap.read()[0]:
            arr.append(index)
            cap.release()
        index += 1
    return arr

def checkCameraResolutions(cam_index):
    # url = "https://en.wikipedia.org/wiki/List_of_common_resolutions"
    # table = pd.read_html(url)[0]
    # table.columns = table.columns.droplevel()
    # table= table[['W', 'H']]
    # table.to_csv("cameraResolutions.csv")
    video_types = [cv2.VideoWriter_fourcc('U', 'Y', 'V', 'Y'),cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')]
    table = pd.read_csv("cameraResolutions.csv")
    # table = pd.read_csv("knownCameraResolutions.csv")
    cap = cv2.VideoCapture(cam_index)
    resolutions = {}
    
    print("\nDefault: {}: {}x{}".format(decode_fourcc(cap.get(cv2.CAP_PROP_FOURCC)), cap.get(cv2.CAP_PROP_FRAME_WIDTH), cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    for video_type in video_types:
        prev = [cap.get(cv2.CAP_PROP_FOURCC), cap.get(cv2.CAP_PROP_FRAME_WIDTH), cap.get(cv2.CAP_PROP_FRAME_HEIGHT)]

        # print("\n\nLooking through video type: {}".format(decode_fourcc(video_type)))
        # update the video type and store the current settings
        cap.set(cv2.CAP_PROP_FOURCC, video_type)
        
        for index, row in table[["W", "H"]].iterrows():
            # print("{}x{}".format(row["W"], row["H"]), end = ', ')
            # attempt new resolution
            cap.set(cv2.CAP_PROP_FRAME_WIDTH, row["W"])
            cap.set(cv2.CAP_PROP_FRAME_HEIGHT, row["H"])

            # if change was successful, add entry to stored successes
            changed = [cap.get(cv2.CAP_PROP_FOURCC), cap.get(cv2.CAP_PROP_FRAME_WIDTH), cap.get(cv2.CAP_PROP_FRAME_HEIGHT)]
            if prev != changed:            
                # resolutions[str(video_type[0]) + ": " + str(changed[1])+"x"+str(changed[2])] = "OK"
                print("\n\nFOUND {}: {}x{}\n".format(decode_fourcc(video_type), changed[1], changed[2]))

            # update prev for next iteration
            prev = changed

    # print(resolutions)


def main():
    cam_index_array= returnCameraIndexes()
    print(" Available camera streams at indicies: " + str(cam_index_array))
    
    for cam_index in cam_index_array:
        print("Index {}".format(cam_index))
        checkCameraResolutions(cam_index)
    
    # Open the device at the ID 0
    cap = cv2.VideoCapture(cam_index_array[0])

    #Check whether user selected camera is opened successfully.
    if not (cap.isOpened()):
        print("Could not open video device")

    # To set the parameters of the camera
    # Search for " VideoCaptureProperties" on
    # https://docs.opencv.org/4.x/d4/d15/group__videoio__flags__base.html#gaeb8dd9c89c10a5c63c139bf7c4f5704d
    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    cap.set(cv2.CAP_PROP_FPS, 30)


    # cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('U', 'Y', 'V', 'Y'))
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 640)
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)

    # print(decode_fourcc(cap.get(cv2.CAP_PROP_FOURCC)))
    # print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    # print(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

    # you can't change one parameter unless it works as a whole settings 
    # i.e. if you change to UYVY from 1920*1080 then it won't change.

    while(True):

        # Capture frame-by-frame
        ret, frame = cap.read()

        # Display the resulting frame
        cv2.imshow('preview',frame)

        #Waits for a user input to quit the application
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()

    cv2.destroyAllWindows()

main()