# TODO
"""
1. incorporate distance measurement and use it to calculate the size of the eye boxes
2. automatically detect the current location of file, create NEW file with timestamp or custom name and save recorded images there
3. convert series of jpgs to images
4. run images through teachable machines as experiment
5. try using DLB (ali slack messages) instead of mediapipe-compare
"""

# TODO 2
"""
determine head pose
make rectangles slant to match head pose 
make boxes 2*1 but based on eye width, then scale to specific size (based on neural net)
"""

import cv2
import mediapipe as mp
import time
import os
import numpy as np

# class to store coordinates of eyes
class eye_landmark_coords():
    def __init__(self, left=0, right=0, bottom=0, top=0):
        self.left = left
        self.right = right
        self.bottom = bottom
        self.top = top


# class to store coordinates of eyes
class eye_pixel_coords():
    def __init__(self, ave_x=0, ave_y=0, width=0, height=0):
        self.centre = (ave_x,ave_y)
        self.width = width
        self.height = height


        # resolution of camera + focal length + distance of face = size of eyes
        # each eye should be twice as wide as tall 

        self.left = int(self.centre[0] - (width/2))
        self.right = int(self.centre[0] + (width/2))
        self.bottom = int(self.centre[1] - (height/2))
        self.top = int(self.centre[1] + (height/2))
        # print("left={}, right={}".format(self.left, self.right))
        # print("top={}, bottom={}".format(self.top, self.bottom))


# class to contain all face and eye detection functions
class FaceMeshDetector():

    def __init__(self, staticMode=False, maxFaces=2, minDetectionCon=0.5, minTrackCon=0.5):

        self.staticMode = staticMode
        self.maxFaces = maxFaces
        self.minDetectionCon = minDetectionCon
        self.minTrackCon = minTrackCon
        self.refined_landmark = True
        # refined_landmark=True, enables 4 points per iris!
        # https://solutions.mediapipe.dev/face_mesh#refine_landmarks

        self.mpDraw = mp.solutions.drawing_utils
        self.mpFaceMesh = mp.solutions.face_mesh
        self.faceMesh = self.mpFaceMesh.FaceMesh(self.staticMode, self.maxFaces,
                                                 self.minDetectionCon, self.minTrackCon)
        # self.faceMesh = self.mpFaceMesh.FaceMesh(self.staticMode, self.maxFaces, self.refined_landmark,
                                                #  self.minDetectionCon, self.minTrackCon)
        self.drawSpec = self.mpDraw.DrawingSpec(thickness=1, circle_radius=1)
        self.eyes = []

    # currently redundant
    def findFaceMesh(self, img, draw=True):
        # self.imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.results = self.faceMesh.process(img)
        if self.results.multi_face_landmarks:
            faces = []
            for faceLms in self.results.multi_face_landmarks:
                if draw:
                    self.mpDraw.draw_landmarks(img, faceLms, self.mpFaceMesh.FACE_CONNECTIONS, self.drawSpec, self.drawSpec)
                face = []
                for id,lm in enumerate(faceLms.landmark):
                    ih, iw, ic = img.shape
                    x,y = int(lm.x*iw), int(lm.y*ih)
                    # if id in self.left_eye or id in self.right_eye:
                    #     print(id,x,y)
                    #     cv2.putText(img, str(id), (x, y), cv2.FONT_HERSHEY_PLAIN, 0.7, (0, 255, 0), 1)
                    face.append([x,y])
                faces.append(face)
        return img, faces

    def findEyes(self, img, path, save):

        # create objects to store landmark IDs of eyes: left, right, bottom, top
        left_landmarks = eye_landmark_coords(130, 243, 27, 23)
        right_landmarks = eye_landmark_coords(463, 359, 257, 253)

        # convert to RGB and use facemesh to identify landmarks
        self.imgRGB = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        self.results = self.faceMesh.process(self.imgRGB)
        # im_height, im_width, ic = img.shape
        im_height, im_width, ic = img.shape

        # if a face has been found, loop through all found faces
        if self.results.multi_face_landmarks is not None:
            for face in self.results.multi_face_landmarks:

                # # get facial distance
                # left_eye_middle = face[145]
                # right_eye_middle = face[374]
                
                # # note- the ,_ just takes the first value
                # width_pixels,_ = self.faceMeshface_detector.findDistance(left_eye_middle, right_eye_middle)
                # width_cm = 6.3 # average space between the eyes in cm
                # print("Left eye horisonal= {}:{}".format((int(face.landmark[left_landmarks.left].x*im_width),int(face.landmark[left_landmarks.right].x*im_width))))
                # print("Left eye horisonal= {}:{}".format((int(face.landmark[130]),int(face.landmark[243]))))

                # print(int(face.landmark[130].x * im_width))
                # print(int(face.landmark[243].x * im_width))
                # width is approx 90 pixels
                # print(face.landmark[243])
                
                left_eye_width = int(abs((face.landmark[left_landmarks.left].x - face.landmark[left_landmarks.right].x)) *im_width)
                right_eye_width = int(abs((face.landmark[left_landmarks.left].x - face.landmark[left_landmarks.right].x)) *im_width)
                ave_eye_width = left_eye_width + right_eye_width/2

                # get average landmark location and create an eye pixel coordinate object to store box dimensions
                left_eye_average_x = (int(face.landmark[left_landmarks.left].x*im_width)+int(face.landmark[left_landmarks.right].x*im_width))/2
                left_eye_average_y = (int(face.landmark[left_landmarks.bottom].y * im_height) + int(face.landmark[left_landmarks.top].y * im_height))/2
                left_eye_pixel_coords = eye_pixel_coords(left_eye_average_x, left_eye_average_y, ave_eye_width, ave_eye_width/2)
                
                # create sub-image containing left eye and display it
                left_eye_img = img[left_eye_pixel_coords.bottom:left_eye_pixel_coords.top, left_eye_pixel_coords.left:left_eye_pixel_coords.right]
                cv2.imshow("left-eye", left_eye_img)
                # print("Left eye size: height={}, width={}".format(len(left_eye_img), len(left_eye_img[0])))

                # get average landmark location and create an eye pixel coordinate object to store box dimensions
                right_eye_average_x = (int(face.landmark[right_landmarks.left].x*im_width) + int(face.landmark[right_landmarks.right].x*im_width))/2
                right_eye_average_y = (int(face.landmark[right_landmarks.bottom].y * im_height) + int(face.landmark[right_landmarks.top].y * im_height))/2
                right_eye_pixel_coords = eye_pixel_coords(right_eye_average_x, right_eye_average_y, ave_eye_width, ave_eye_width/2)

                # create sub-image containing left eye and display it
                right_eye_img = img[right_eye_pixel_coords.bottom:right_eye_pixel_coords.top, right_eye_pixel_coords.left:right_eye_pixel_coords.right]
                cv2.imshow("right-eye", right_eye_img)

                # save the image  
                if save==True:
                        
                    #to save as image
                    title = "left-eye" + str(time.time()) + ".jpg"
                    if not cv2.imwrite(os.path.join(path, title), left_eye_img):
                        raise Exception("Could not write image")
                        
                    #to save as image
                    title = "right-eye" + str(time.time()) + ".jpg"
                    if not cv2.imwrite(os.path.join(path, title), right_eye_img):
                        raise Exception("Could not write image")
                    
                    # small delay to ensure save process completes
                    cv2.waitKey(1)

                # annotate eyes on image
                cv2.rectangle(img, [left_eye_pixel_coords.left, left_eye_pixel_coords.bottom], [left_eye_pixel_coords.right, left_eye_pixel_coords.top], [255,0,0], 1)            
                cv2.rectangle(img, [right_eye_pixel_coords.left, right_eye_pixel_coords.bottom], [right_eye_pixel_coords.right, right_eye_pixel_coords.top], [0,255,0], 1)
                

        return img



def main():
    # get input from webcam
    cap = cv2.VideoCapture(0)

    cap.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter_fourcc('M','J','P','G'))
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    cap.set(cv2.CAP_PROP_FPS, 30)

    # time reference variable used to calculate FPS
    pTime = 0

    # create a faceMeshDetector object to capute only 1 face
    detector = FaceMeshDetector(maxFaces=1)

    # how many eye images should be saved
    number_of_saved_eyes = 500

    # where should the images be saved?
    path="/home/max-smith/Documents/2022-max_smith_thesis/mediapipe_example/eyes/IR_test_1"
   
    # # array of eye image dimensions for average 
    # img_dimension_h_ws = []

    # while number_of_saved_eyes > 0:
    while 1:

        # process the video frame in to an numpy array (img). raise error if failed
        success, img = cap.read()

        if success == False:
            raise Exception("Could not read image capture")
        # img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        # img = cv2.rotate(img, cv2.cv2.ROTATE_90_CLOCKWISE)
        # faces = []
        # img, faces = detector.findFaceMesh(img, True)

        # run the find eyes program while we need to save more eye images
        if number_of_saved_eyes > 0:
            number_of_saved_eyes-=1
            img = detector.findEyes(img, path, True)
            # img_dimension_h_ws.append(left_h_w)
            # img_dimension_h_ws.append(right_h_w)


        # calcuate FPS
        cTime = time.time()
        fps = 1 / (cTime - pTime)
        pTime = cTime

        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
        # annotate FPS onto current frame
        cv2.putText(img, f'FPS: {int(fps)}', (20, 70), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 3)
        
        # display the current frame
        cv2.imshow("Image", img)
        cv2.waitKey(1)

    # average eye dimensions
    # total_w = 0
    # total_h = 0
    # for img_h_w in img_dimension_h_ws:
    #     total_h+=img_h_w[0]
    #     total_w+=img_h_w[1]
    # ave_height=total_h/len(img_dimension_h_ws)
    # ave_width=total_w/len(img_dimension_h_ws)
    # print("average dimensions of eye images h={} w={}".format(ave_height, ave_width))
    # average is 20*40 images

if __name__ == "__main__":
    main()