# CMake generated Testfile for 
# Source directory: /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws/src
# Build directory: /home/max-smith/Documents/2022-max_smith_thesis/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("beginner_tutorials")
