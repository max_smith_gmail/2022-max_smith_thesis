//
//  untitled_dt.h
//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  Code generation for model "untitled".
//
//  Model version              : 1.0
//  Simulink Coder version : 9.6 (R2021b) 14-May-2021
//  C++ source code generated on : Tue May  3 20:13:45 2022
//
//  Target selection: ert.tlc
//  Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
//  Code generation objectives: Unspecified
//  Validation result: Not run


#include "ext_types.h"

// data type size table
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T),
  sizeof(int32_T),
  sizeof(SL_Bus_untitled_geometry_msgs_Vector3),
  sizeof(SL_Bus_untitled_geometry_msgs_Twist),
  sizeof(ros_slroscpp_internal_block_S_T),
  sizeof(char_T),
  sizeof(uchar_T),
  sizeof(time_T)
};

// data type name table
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T",
  "physical_connection",
  "SL_Bus_untitled_geometry_msgs_Vector3",
  "SL_Bus_untitled_geometry_msgs_Twist",
  "ros_slroscpp_internal_block_S_T",
  "char_T",
  "uchar_T",
  "time_T"
};

// data type transitions for block I/O structure
static DataTypeTransition rtBTransitions[] = {
  { (char_T *)(&untitled_B.In1), 16, 0, 1 },

  { (char_T *)(&untitled_B.X), 0, 0, 3 },

  { (char_T *)(&untitled_B.SourceBlock_o1), 8, 0, 1 }
  ,

  { (char_T *)(&untitled_DW.obj), 17, 0, 1 },

  { (char_T *)(&untitled_DW.EnabledSubsystem_SubsysRanBC), 2, 0, 1 }
};

// data type transition table for block I/O structure
static DataTypeTransitionTable rtBTransTable = {
  5U,
  rtBTransitions
};

// data type transitions for Parameters structure
static DataTypeTransition rtPTransitions[] = {
  { (char_T *)(&untitled_P.Out1_Y0), 16, 0, 1 },

  { (char_T *)(&untitled_P.Constant_Value), 16, 0, 1 }
};

// data type transition table for Parameters structure
static DataTypeTransitionTable rtPTransTable = {
  2U,
  rtPTransitions
};

// [EOF] untitled_dt.h
