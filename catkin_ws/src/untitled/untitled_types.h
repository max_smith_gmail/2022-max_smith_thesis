//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: untitled_types.h
//
// Code generated for Simulink model 'untitled'.
//
// Model version                  : 1.0
// Simulink Coder version         : 9.6 (R2021b) 14-May-2021
// C/C++ source code generated on : Tue May  3 20:13:45 2022
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_untitled_types_h_
#define RTW_HEADER_untitled_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"

// Model Code Variants
#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_untitled_geometry_msgs_Vector3_
#define DEFINED_TYPEDEF_FOR_SL_Bus_untitled_geometry_msgs_Vector3_

// MsgType=geometry_msgs/Vector3
struct SL_Bus_untitled_geometry_msgs_Vector3
{
  real_T X;
  real_T Y;
  real_T Z;
};

#endif

#ifndef DEFINED_TYPEDEF_FOR_SL_Bus_untitled_geometry_msgs_Twist_
#define DEFINED_TYPEDEF_FOR_SL_Bus_untitled_geometry_msgs_Twist_

// MsgType=geometry_msgs/Twist
struct SL_Bus_untitled_geometry_msgs_Twist
{
  // MsgType=geometry_msgs/Vector3
  SL_Bus_untitled_geometry_msgs_Vector3 Linear;

  // MsgType=geometry_msgs/Vector3
  SL_Bus_untitled_geometry_msgs_Vector3 Angular;
};

#endif

#ifndef struct_ros_slroscpp_internal_block_S_T
#define struct_ros_slroscpp_internal_block_S_T

struct ros_slroscpp_internal_block_S_T
{
  boolean_T matlabCodegenIsDeleted;
  int32_T isInitialized;
  boolean_T isSetupComplete;
};

#endif                                // struct_ros_slroscpp_internal_block_S_T

// Parameters (default storage)
typedef struct P_untitled_T_ P_untitled_T;

// Forward declaration for rtModel
typedef struct tag_RTM_untitled_T RT_MODEL_untitled_T;

#endif                                 // RTW_HEADER_untitled_types_h_

//
// File trailer for generated code.
//
// [EOF]
//
