#include "slros_initialize.h"

ros::NodeHandle * SLROSNodePtr;
const std::string SLROSNodeName = "test1";

// For Block test1/Subscribe
SimulinkSubscriber<geometry_msgs::Twist, SL_Bus_test1_geometry_msgs_Twist> Sub_test1_1;

// For Block test1/Publish
SimulinkPublisher<geometry_msgs::Twist, SL_Bus_test1_geometry_msgs_Twist> Pub_test1_16;

void slros_node_init(int argc, char** argv)
{
  ros::init(argc, argv, SLROSNodeName);
  SLROSNodePtr = new ros::NodeHandle();
}

