//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: test1.h
//
// Code generated for Simulink model 'test1'.
//
// Model version                  : 1.3
// Simulink Coder version         : 9.6 (R2021b) 14-May-2021
// C/C++ source code generated on : Tue May  3 20:42:02 2022
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#ifndef RTW_HEADER_test1_h_
#define RTW_HEADER_test1_h_
#include <float.h>
#include <stddef.h>
#include "rtwtypes.h"
#include "rtw_extmode.h"
#include "sysran_types.h"
#include "dt_info.h"
#include "ext_work.h"
#include "slros_initialize.h"
#include "test1_types.h"

// Shared type includes
#include "multiword_types.h"

// Macros for accessing real-time model data structure
#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWExtModeInfo
#define rtmGetRTWExtModeInfo(rtm)      ((rtm)->extModeInfo)
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                (&(rtm)->Timing.taskTime0)
#endif

// Block signals (default storage)
struct B_test1_T {
  SL_Bus_test1_geometry_msgs_Twist In1;// '<S4>/In1'
  real_T X;
  real_T Y;
  boolean_T SourceBlock_o1;            // '<S3>/SourceBlock'
};

// Block states (default storage) for system '<Root>'
struct DW_test1_T {
  ros_slroscpp_internal_block_P_T obj; // '<S2>/SinkBlock'
  ros_slroscpp_internal_block_S_T obj_n;// '<S3>/SourceBlock'
  int8_T EnabledSubsystem_SubsysRanBC; // '<S3>/Enabled Subsystem'
};

// Parameters (default storage)
struct P_test1_T_ {
  SL_Bus_test1_geometry_msgs_Twist Constant_Value;// Computed Parameter: Constant_Value
                                                     //  Referenced by: '<S1>/Constant'

  SL_Bus_test1_geometry_msgs_Twist Out1_Y0;// Computed Parameter: Out1_Y0
                                              //  Referenced by: '<S4>/Out1'

  SL_Bus_test1_geometry_msgs_Twist Constant_Value_d;// Computed Parameter: Constant_Value_d
                                                       //  Referenced by: '<S3>/Constant'

  real_T Vel_X_Value;                  // Expression: 0.9903680419921876
                                          //  Referenced by: '<Root>/Vel_X'

  real_T Vel_Y_Value;                  // Expression: 0
                                          //  Referenced by: '<Root>/Vel_Y'

  real_T Vel_Z_Value;                  // Expression: 0
                                          //  Referenced by: '<Root>/Vel_Z'

  real_T Ang_X_Value;                  // Expression: 0
                                          //  Referenced by: '<Root>/Ang_X'

  real_T Ang_Y_Value;                  // Expression: 0
                                          //  Referenced by: '<Root>/Ang_Y'

  real_T Ang_Z_Value;                  // Expression: 0.9310170491536458
                                          //  Referenced by: '<Root>/Ang_Z'

};

// Real-time Model Data Structure
struct tag_RTM_test1_T {
  const char_T *errorStatus;
  RTWExtModeInfo *extModeInfo;

  //
  //  Sizes:
  //  The following substructure contains sizes information
  //  for many of the model attributes such as inputs, outputs,
  //  dwork, sample times, etc.

  struct {
    uint32_T checksums[4];
  } Sizes;

  //
  //  SpecialInfo:
  //  The following substructure contains special information
  //  related to other components that are dependent on RTW.

  struct {
    const void *mappingInfo;
  } SpecialInfo;

  //
  //  Timing:
  //  The following substructure contains information regarding
  //  the timing information for the model.

  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    time_T stepSize0;
    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

// Block parameters (default storage)
#ifdef __cplusplus

extern "C" {

#endif

  extern P_test1_T test1_P;

#ifdef __cplusplus

}
#endif

// Block signals (default storage)
#ifdef __cplusplus

extern "C" {

#endif

  extern struct B_test1_T test1_B;

#ifdef __cplusplus

}
#endif

// Block states (default storage)
extern struct DW_test1_T test1_DW;

#ifdef __cplusplus

extern "C" {

#endif

  // Model entry point functions
  extern void test1_initialize(void);
  extern void test1_step(void);
  extern void test1_terminate(void);

#ifdef __cplusplus

}
#endif

// Real-time Model object
#ifdef __cplusplus

extern "C" {

#endif

  extern RT_MODEL_test1_T *const test1_M;

#ifdef __cplusplus

}
#endif

//-
//  The generated code includes comments that allow you to trace directly
//  back to the appropriate location in the model.  The basic format
//  is <system>/block_name, where system is the system number (uniquely
//  assigned by Simulink) and block_name is the name of the block.
//
//  Use the MATLAB hilite_system command to trace the generated code back
//  to the model.  For example,
//
//  hilite_system('<S3>')    - opens system 3
//  hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
//
//  Here is the system hierarchy for this model
//
//  '<Root>' : 'test1'
//  '<S1>'   : 'test1/Blank Message'
//  '<S2>'   : 'test1/Publish'
//  '<S3>'   : 'test1/Subscribe'
//  '<S4>'   : 'test1/Subscribe/Enabled Subsystem'

#endif                                 // RTW_HEADER_test1_h_

//
// File trailer for generated code.
//
// [EOF]
//
