#ifndef _SLROS_INITIALIZE_H_
#define _SLROS_INITIALIZE_H_

#include "slros_busmsg_conversion.h"
#include "slros_generic.h"
#include "test1_types.h"

extern ros::NodeHandle * SLROSNodePtr;
extern const std::string SLROSNodeName;

// For Block test1/Subscribe
extern SimulinkSubscriber<geometry_msgs::Twist, SL_Bus_test1_geometry_msgs_Twist> Sub_test1_1;

// For Block test1/Publish
extern SimulinkPublisher<geometry_msgs::Twist, SL_Bus_test1_geometry_msgs_Twist> Pub_test1_16;

void slros_node_init(int argc, char** argv);

#endif
