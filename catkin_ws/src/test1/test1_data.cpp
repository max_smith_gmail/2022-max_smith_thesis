//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: test1_data.cpp
//
// Code generated for Simulink model 'test1'.
//
// Model version                  : 1.3
// Simulink Coder version         : 9.6 (R2021b) 14-May-2021
// C/C++ source code generated on : Tue May  3 20:42:02 2022
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "test1.h"
#include "test1_private.h"

// Block parameters (default storage)
P_test1_T test1_P = {
  // Computed Parameter: Constant_Value
  //  Referenced by: '<S1>/Constant'

  {
    {
      0.0,                             // X
      0.0,                             // Y
      0.0                              // Z
    },                                 // Linear

    {
      0.0,                             // X
      0.0,                             // Y
      0.0                              // Z
    }                                  // Angular
  },

  // Computed Parameter: Out1_Y0
  //  Referenced by: '<S4>/Out1'

  {
    {
      0.0,                             // X
      0.0,                             // Y
      0.0                              // Z
    },                                 // Linear

    {
      0.0,                             // X
      0.0,                             // Y
      0.0                              // Z
    }                                  // Angular
  },

  // Computed Parameter: Constant_Value_d
  //  Referenced by: '<S3>/Constant'

  {
    {
      0.0,                             // X
      0.0,                             // Y
      0.0                              // Z
    },                                 // Linear

    {
      0.0,                             // X
      0.0,                             // Y
      0.0                              // Z
    }                                  // Angular
  },

  // Expression: 0.9903680419921876
  //  Referenced by: '<Root>/Vel_X'

  0.99036804199218764,

  // Expression: 0
  //  Referenced by: '<Root>/Vel_Y'

  0.0,

  // Expression: 0
  //  Referenced by: '<Root>/Vel_Z'

  0.0,

  // Expression: 0
  //  Referenced by: '<Root>/Ang_X'

  0.0,

  // Expression: 0
  //  Referenced by: '<Root>/Ang_Y'

  0.0,

  // Expression: 0.9310170491536458
  //  Referenced by: '<Root>/Ang_Z'

  0.9310170491536458
};

//
// File trailer for generated code.
//
// [EOF]
//
