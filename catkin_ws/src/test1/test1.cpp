//
// Academic License - for use in teaching, academic research, and meeting
// course requirements at degree granting institutions only.  Not for
// government, commercial, or other organizational use.
//
// File: test1.cpp
//
// Code generated for Simulink model 'test1'.
//
// Model version                  : 1.3
// Simulink Coder version         : 9.6 (R2021b) 14-May-2021
// C/C++ source code generated on : Tue May  3 20:42:02 2022
//
// Target selection: ert.tlc
// Embedded hardware selection: Generic->Unspecified (assume 32-bit Generic)
// Code generation objectives: Unspecified
// Validation result: Not run
//
#include "test1.h"
#include "test1_private.h"
#include "test1_dt.h"

// Block signals (default storage)
B_test1_T test1_B;

// Block states (default storage)
DW_test1_T test1_DW;

// Real-time model
RT_MODEL_test1_T test1_M_ = RT_MODEL_test1_T();
RT_MODEL_test1_T *const test1_M = &test1_M_;

// Model step function
void test1_step(void)
{
  SL_Bus_test1_geometry_msgs_Twist rtb_BusAssignment;

  // Reset subsysRan breadcrumbs
  srClearBC(test1_DW.EnabledSubsystem_SubsysRanBC);

  // BusAssignment: '<Root>/Bus Assignment' incorporates:
  //   Constant: '<Root>/Ang_X'
  //   Constant: '<Root>/Ang_Y'
  //   Constant: '<Root>/Ang_Z'
  //   Constant: '<Root>/Vel_X'
  //   Constant: '<Root>/Vel_Y'
  //   Constant: '<Root>/Vel_Z'

  rtb_BusAssignment.Linear.X = test1_P.Vel_X_Value;
  rtb_BusAssignment.Linear.Y = test1_P.Vel_Y_Value;
  rtb_BusAssignment.Linear.Z = test1_P.Vel_Z_Value;
  rtb_BusAssignment.Angular.X = test1_P.Ang_X_Value;
  rtb_BusAssignment.Angular.Y = test1_P.Ang_Y_Value;
  rtb_BusAssignment.Angular.Z = test1_P.Ang_Z_Value;

  // Outputs for Atomic SubSystem: '<Root>/Publish'
  // MATLABSystem: '<S2>/SinkBlock'
  Pub_test1_16.publish(&rtb_BusAssignment);

  // End of Outputs for SubSystem: '<Root>/Publish'

  // Outputs for Atomic SubSystem: '<Root>/Subscribe'
  // MATLABSystem: '<S3>/SourceBlock'
  test1_B.SourceBlock_o1 = Sub_test1_1.getLatestMessage(&rtb_BusAssignment);

  // Outputs for Enabled SubSystem: '<S3>/Enabled Subsystem' incorporates:
  //   EnablePort: '<S4>/Enable'

  if (test1_B.SourceBlock_o1) {
    // Inport: '<S4>/In1' incorporates:
    //   MATLABSystem: '<S3>/SourceBlock'

    test1_B.In1 = rtb_BusAssignment;
    srUpdateBC(test1_DW.EnabledSubsystem_SubsysRanBC);
  }

  // End of Outputs for SubSystem: '<S3>/Enabled Subsystem'
  // End of Outputs for SubSystem: '<Root>/Subscribe'

  // SignalConversion generated from: '<Root>/Bus Selector'
  test1_B.X = test1_B.In1.Linear.X;

  // SignalConversion generated from: '<Root>/Bus Selector'
  test1_B.Y = test1_B.In1.Linear.Y;

  // External mode
  rtExtModeUploadCheckTrigger(1);

  {                                    // Sample time: [0.2s, 0.0s]
    rtExtModeUpload(0, (real_T)test1_M->Timing.taskTime0);
  }

  // signal main to stop simulation
  {                                    // Sample time: [0.2s, 0.0s]
    if ((rtmGetTFinal(test1_M)!=-1) &&
        !((rtmGetTFinal(test1_M)-test1_M->Timing.taskTime0) >
          test1_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(test1_M, "Simulation finished");
    }

    if (rtmGetStopRequested(test1_M)) {
      rtmSetErrorStatus(test1_M, "Simulation finished");
    }
  }

  // Update absolute time for base rate
  // The "clockTick0" counts the number of times the code of this task has
  //  been executed. The absolute time is the multiplication of "clockTick0"
  //  and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
  //  overflow during the application lifespan selected.

  test1_M->Timing.taskTime0 =
    ((time_T)(++test1_M->Timing.clockTick0)) * test1_M->Timing.stepSize0;
}

// Model initialize function
void test1_initialize(void)
{
  // Registration code
  rtmSetTFinal(test1_M, -1);
  test1_M->Timing.stepSize0 = 0.2;

  // External mode info
  test1_M->Sizes.checksums[0] = (1116932359U);
  test1_M->Sizes.checksums[1] = (3166086631U);
  test1_M->Sizes.checksums[2] = (3655552705U);
  test1_M->Sizes.checksums[3] = (3071001867U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[7];
    test1_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    systemRan[3] = &rtAlwaysEnabled;
    systemRan[4] = (sysRanDType *)&test1_DW.EnabledSubsystem_SubsysRanBC;
    systemRan[5] = &rtAlwaysEnabled;
    systemRan[6] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(test1_M->extModeInfo,
      &test1_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(test1_M->extModeInfo, test1_M->Sizes.checksums);
    rteiSetTPtr(test1_M->extModeInfo, rtmGetTPtr(test1_M));
  }

  // data type transition information
  {
    static DataTypeTransInfo dtInfo;
    test1_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 22;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    // Block I/O transition table
    dtInfo.BTransTable = &rtBTransTable;

    // Parameters transition table
    dtInfo.PTransTable = &rtPTransTable;
  }

  {
    int32_T i;
    char_T b_zeroDelimTopic[17];
    char_T b_zeroDelimTopic_tmp[17];
    static const char_T tmp[16] = { '/', 't', 'u', 'r', 't', 'l', 'e', '1', '/',
      'c', 'm', 'd', '_', 'v', 'e', 'l' };

    // SystemInitialize for Atomic SubSystem: '<Root>/Publish'
    // Start for MATLABSystem: '<S2>/SinkBlock' incorporates:
    //   MATLABSystem: '<S3>/SourceBlock'

    test1_DW.obj.matlabCodegenIsDeleted = false;
    test1_DW.obj.isInitialized = 1;

    // SystemInitialize for Atomic SubSystem: '<Root>/Subscribe'
    for (i = 0; i < 16; i++) {
      b_zeroDelimTopic_tmp[i] = tmp[i];
    }

    b_zeroDelimTopic_tmp[16] = '\x00';

    // End of SystemInitialize for SubSystem: '<Root>/Subscribe'
    for (i = 0; i < 17; i++) {
      b_zeroDelimTopic[i] = b_zeroDelimTopic_tmp[i];
    }

    Pub_test1_16.createPublisher(&b_zeroDelimTopic[0], 1);
    test1_DW.obj.isSetupComplete = true;

    // End of Start for MATLABSystem: '<S2>/SinkBlock'
    // End of SystemInitialize for SubSystem: '<Root>/Publish'

    // SystemInitialize for Atomic SubSystem: '<Root>/Subscribe'
    // SystemInitialize for Enabled SubSystem: '<S3>/Enabled Subsystem'
    // SystemInitialize for Outport: '<S4>/Out1' incorporates:
    //   Inport: '<S4>/In1'

    test1_B.In1 = test1_P.Out1_Y0;

    // End of SystemInitialize for SubSystem: '<S3>/Enabled Subsystem'

    // Start for MATLABSystem: '<S3>/SourceBlock'
    test1_DW.obj_n.matlabCodegenIsDeleted = false;
    test1_DW.obj_n.isInitialized = 1;
    Sub_test1_1.createSubscriber(&b_zeroDelimTopic_tmp[0], 1);
    test1_DW.obj_n.isSetupComplete = true;

    // End of SystemInitialize for SubSystem: '<Root>/Subscribe'
  }
}

// Model terminate function
void test1_terminate(void)
{
  // Terminate for Atomic SubSystem: '<Root>/Publish'
  // Terminate for MATLABSystem: '<S2>/SinkBlock'
  if (!test1_DW.obj.matlabCodegenIsDeleted) {
    test1_DW.obj.matlabCodegenIsDeleted = true;
  }

  // End of Terminate for MATLABSystem: '<S2>/SinkBlock'
  // End of Terminate for SubSystem: '<Root>/Publish'

  // Terminate for Atomic SubSystem: '<Root>/Subscribe'
  // Terminate for MATLABSystem: '<S3>/SourceBlock'
  if (!test1_DW.obj_n.matlabCodegenIsDeleted) {
    test1_DW.obj_n.matlabCodegenIsDeleted = true;
  }

  // End of Terminate for MATLABSystem: '<S3>/SourceBlock'
  // End of Terminate for SubSystem: '<Root>/Subscribe'
}

//
// File trailer for generated code.
//
// [EOF]
//
