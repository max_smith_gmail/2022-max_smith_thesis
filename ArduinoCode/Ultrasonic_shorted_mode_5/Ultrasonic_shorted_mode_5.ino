#include <SoftwareSerial.h>
#define rxPin 9
#define txPin 8

// sudo chmod a+rw /dev/ttyACM0
 
SoftwareSerial jsnSerial(rxPin, txPin);
 
void setup() {
  
  jsnSerial.begin(9600);
  Serial.begin(9600);
}
 
void loop() {
  jsnSerial.write(0x01);
  delay(10);
  Serial.println("looping");
  if(jsnSerial.available()){
    Serial.println("in the if");
    Serial.println(jsnSerial.readString());
  }
}