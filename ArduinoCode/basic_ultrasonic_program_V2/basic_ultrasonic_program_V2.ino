// https://forum.core-electronics.com.au/t/need-help-with-ultrasonic-sensor/9286

#define ECHOPIN 8// Pin to receive echo pulse
#define TRIGPIN 9// Pin to send trigger pulse
void setup(){
  Serial.begin(9600);
  pinMode(ECHOPIN, INPUT);
  pinMode(TRIGPIN, OUTPUT);
  digitalWrite(ECHOPIN, HIGH);
}
void loop(){
  digitalWrite(TRIGPIN, LOW); // Set the trigger pin to low for 2uS
  delayMicroseconds(2);
  digitalWrite(TRIGPIN, HIGH); // Send a 10uS high to trigger ranging
  delayMicroseconds(10);
  digitalWrite(TRIGPIN, LOW); // Send pin low again
  unsigned int distance = pulseIn(ECHOPIN, HIGH,26000); // Read in times pulse, max wait 26ms
  distance= distance/58; //Spd of Snd = 343 m/s = 0.0343cm/us /2 for round trip = 0.01715cm/s => 1/0.01715 ~= 58us/cm
  Serial.print(distance);
  Serial.println("   cm");
  delay(50);// Wait 50mS before next ranging
}